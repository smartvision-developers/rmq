@extends('admin.index')
@push('custom-css')
<style type="text/css">
	.select2{
		width: 100% !important;
	}
</style>
@endpush
@section('content')
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>{{!empty($title)?$title:''}}</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('productcoupons')}}" class="dropdown-item" style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}} </a>
				<a href="{{aurl('productcoupons/'.$productcoupons->id)}}" class="dropdown-item" style="color:#343a40">
				<i class="fa fa-eye"></i> {{trans('admin.show')}} </a>
				<a class="dropdown-item" style="color:#343a40" href="{{aurl('productcoupons/create')}}">
					<i class="fa fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$productcoupons->id}}" class="dropdown-item" style="color:#343a40" href="#">
					<i class="fa fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$productcoupons->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>   {{trans('admin.ask_del')}} {{trans('admin.id')}}  ({{$productcoupons->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
						'method' => 'DELETE',
						'route' => ['productcoupons.destroy', $productcoupons->id]
						]) !!}
						{!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						<a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('admin.cancel')}}</a>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
										
{!! Form::open(['url'=>aurl('/productcoupons/'.$productcoupons->id),'method'=>'put','id'=>'productcoupons','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        <div>
            @php $products= App\Models\Product_Coupon::where('coupon_id', $productcoupons->id )->get();
  
             @endphp
            @foreach($products as $product)
                <div>{{$product->product_id()->first()->product_name}}</div>
            @endforeach
        </div>
        <p class="btn btn-info" id="show"> تعديل خصم المنتجات </p>
    </div>
    
</div>

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 hide" style="display:none;">
		<div class="form-group">
				{!! Form::label('product_id',trans('admin.product_id'),['class'=>'control-label']) !!}
				{!! Form::select('product_id[]',App\Models\Product::pluck('product_name','id'),old('product_id'),['class'=>'form-control select2','multiple']) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('coupon_name',trans('admin.coupon_name'),['class'=>'control-label']) !!}
        {!! Form::text('coupon_name', $productcoupons->coupon_name ,['class'=>'form-control','placeholder'=>trans('admin.coupon_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('coupon_code',trans('admin.coupon_code'),['class'=>'control-label']) !!}
        {!! Form::text('coupon_code', $productcoupons->coupon_code ,['class'=>'form-control','placeholder'=>trans('admin.coupon_code')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <!-- Date range -->
    <div class="form-group">
        {!! Form::label('start_date',trans('admin.start_date')) !!}
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                </span>
            </div>
            {!! Form::text('start_date', $productcoupons->start_date ,['class'=>'form-control float-right datepicker','placeholder'=>trans('admin.start_date'),'readonly'=>'readonly']) !!}
        </div>
        <!-- /.input group -->
    </div>
    <!-- /.form group -->
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <!-- Date range -->
    <div class="form-group">
        {!! Form::label('end_date',trans('admin.end_date')) !!}
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                </span>
            </div>
            {!! Form::text('end_date', $productcoupons->end_date ,['class'=>'form-control float-right datepicker','placeholder'=>trans('admin.end_date'),'readonly'=>'readonly']) !!}
        </div>
        <!-- /.input group -->
    </div>
    <!-- /.form group -->
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('coupon_discount',trans('admin.coupon_discount'),['class'=>'control-label']) !!}
        {!! Form::text('coupon_discount', $productcoupons->coupon_discount ,['class'=>'form-control','placeholder'=>trans('admin.coupon_discount')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('discount_type',trans('admin.discount_type'),['class'=>'control-label']) !!}
{!! Form::select('discount_type',['percent'=>trans('admin.percent'),'sar'=>trans('admin.sar'),], $productcoupons->discount_type ,['class'=>'form-control select2','placeholder'=>trans('admin.discount_type')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('status',trans('admin.status'),['class'=>'control-label']) !!}
{!! Form::select('status',['show'=>trans('admin.show'),'hide'=>trans('admin.hide'),], $productcoupons->status ,['class'=>'form-control select2','placeholder'=>trans('admin.status')]) !!}
		</div>
</div>

</div>
		<!-- /.row -->
		</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="save" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save') }}</button>
<button type="submit" name="save_back" class="btn btn-success btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save_back') }}</button>
{!! Form::close() !!}
</div>
</div>
@endsection

@push('custom-scripts')

<script type="text/javascript">
    $(document).ready(function(){
    // console.log('dfgh');
   
       $("#show").click(function(){
    $(".hide").show();
  });

   
     });
</script>
@endpush