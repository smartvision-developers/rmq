@extends('admin.index')
@section('content')
@include('admin.ajax',[
	'typeForm'=>'create',
	'selectID'=>'city_id',
	'outputClass'=>'region_id',
	'url'=>aurl('users/get/region/id'),
])


<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>
			{{ !empty($title)?$title:'' }}
			</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{ aurl('users') }}"  style="color:#343a40"  class="dropdown-item">
				<i class="fas fa-list"></i> {{ trans('admin.show_all') }}</a>
			</div>
		</div>
		</h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
								
{!! Form::open(['url'=>aurl('/users'),'id'=>'users','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('first_name',trans('admin.first_name'),['class'=>' control-label']) !!}
            {!! Form::text('first_name',old('first_name'),['class'=>'form-control','placeholder'=>trans('admin.first_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('last_name',trans('admin.last_name'),['class'=>' control-label']) !!}
            {!! Form::text('last_name',old('last_name'),['class'=>'form-control','placeholder'=>trans('admin.last_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('email',trans('admin.email'),['class'=>'control-label']) !!}
            {!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>trans('admin.email')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('mobile',trans('admin.mobile'),['class'=>' control-label']) !!}
            {!! Form::text('mobile',old('mobile'),['class'=>'form-control','placeholder'=>trans('admin.mobile')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('password',trans('admin.password'),['class'=>' control-label']) !!}
            {!! Form::password('password',['class'=>'form-control','placeholder'=>trans('admin.password')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	<div class="form-group">
		{!! Form::label('city_id',trans('admin.city_id')) !!}
		{!! Form::select('city_id',App\Models\City::pluck('city_name','id'),old('city_id'),['class'=>'form-control select2','placeholder'=>trans('admin.choose')]) !!}
	</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	<div class="form-group">
		{!! Form::label('region_id',trans('admin.region_id')) !!}
		<span class="region_id"></span>
	</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('country_code',trans('admin.country_code'),['class'=>' control-label']) !!}
            {!! Form::text('country_code',old('country_code'),['class'=>'form-control','placeholder'=>trans('admin.country_code')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	<div class="form-group">
		{!! Form::label('status',trans('admin.status')) !!}
		{!! Form::select('status',['pending'=>trans('admin.pending'),'accepted'=>trans('admin.accepted'),'rejected'=>trans('admin.rejected'),],old('status'),['class'=>'form-control select2','placeholder'=>trans('admin.choose')]) !!}
	</div>
	
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	<div id="ifReject" style="display: none;" class="form-group">
       		{!! Form::label('status',trans('admin.decline_reason')) !!}
        <textarea class="form-control" type="text" id="decline_reason" name="decline_reason" placeholder="{{trans('admin.decline_reason')}}" /></textarea>
    </div>
</div>
</div>
		<!-- /.row -->
	</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="add" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add') }}</button>
<button type="submit" name="add_back" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add_back') }}</button>
{!! Form::close() !!}	</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  $(function() {
  	$('#status').change(function() {
   // console.log($('#status').val());
    if ($(this).val() == "rejected") {
        $('#ifReject').css('display','block');
    } else {
         $('#ifReject').css('display','none');
    }
});
});
</script>
@endpush