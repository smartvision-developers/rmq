@extends('admin.index')
@section('content')
@include('admin.ajax',[
	'typeForm'=>'edit',
	'selectID'=>'city_id',
	'parentValue'=>$users->city_id,
	'outputClass'=>'region_id',
	'selectedvalue'=>$users->region_id,
	'url'=>aurl('users/get/region/id'),
])


<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>{{!empty($title)?$title:''}}</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('users')}}" class="dropdown-item" style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}} </a>
				<a href="{{aurl('users/'.$users->id)}}" class="dropdown-item" style="color:#343a40">
				<i class="fa fa-eye"></i> {{trans('admin.show')}} </a>
				<a class="dropdown-item" style="color:#343a40" href="{{aurl('users/create')}}">
					<i class="fa fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$users->id}}" class="dropdown-item" style="color:#343a40" href="#">
					<i class="fa fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$users->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>   {{trans('admin.ask_del')}} {{trans('admin.id')}}  ({{$users->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
						'method' => 'DELETE',
						'route' => ['users.destroy', $users->id]
						]) !!}
						{!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						<a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('admin.cancel')}}</a>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
										
{!! Form::open(['url'=>aurl('/users/'.$users->id),'method'=>'put','id'=>'users','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('first_name',trans('admin.first_name'),['class'=>'control-label']) !!}
        {!! Form::text('first_name', $users->first_name ,['class'=>'form-control','placeholder'=>trans('admin.first_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('last_name',trans('admin.last_name'),['class'=>'control-label']) !!}
        {!! Form::text('last_name', $users->last_name ,['class'=>'form-control','placeholder'=>trans('admin.last_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('email',trans('admin.email'),['class'=>'control-label']) !!}
            {!! Form::email('email', $users->email ,['class'=>'form-control','placeholder'=>trans('admin.email')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('mobile',trans('admin.mobile'),['class'=>'control-label']) !!}
        {!! Form::text('mobile', $users->mobile ,['class'=>'form-control','placeholder'=>trans('admin.mobile')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('password',trans('admin.password'),['class'=>'control-label']) !!}
        {!! Form::password('password' ,['class'=>'form-control','placeholder'=>trans('admin.password')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('city_id',trans('admin.city_id'),['class'=>'control-label']) !!}
{!! Form::select('city_id',App\Models\City::pluck('city_name','id'), $users->city_id ,['class'=>'form-control select2','placeholder'=>trans('admin.city_id')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('region_id',trans('admin.region_id'),['class'=>'control-label']) !!}
		<span class="region_id"></span>
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('country_code',trans('admin.country_code'),['class'=>'control-label']) !!}
        {!! Form::text('country_code', $users->country_code ,['class'=>'form-control','placeholder'=>trans('admin.country_code')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('status',trans('admin.status'),['class'=>'control-label']) !!}
{!! Form::select('status',['pending'=>trans('admin.pending'),'accepted'=>trans('admin.accepted'),'rejected'=>trans('admin.rejected'),], $users->status ,['class'=>'form-control select2','placeholder'=>trans('admin.status')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
	<div id="ifReject" @if($users->status != 'rejected') style="display: none;"  @endif class="form-group">
       		{!! Form::label('status',trans('admin.decline_reason')) !!}
        <textarea class="form-control" type="text" id="decline_reason" name="decline_reason" placeholder="{{trans('admin.decline_reason')}}" />{{$users->decline_reason}}</textarea>
    </div>
</div>
</div>
		<!-- /.row -->
		</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="save" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save') }}</button>
<button type="submit" name="save_back" class="btn btn-success btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save_back') }}</button>
{!! Form::close() !!}
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">
  $(function() {
  	$('#status').change(function() {
   // console.log($('#status').val());
    if ($(this).val() == "rejected") {
        $('#ifReject').css('display','block');
    } else {
         $('#ifReject').css('display','none');
    }
});
});
</script>
@endpush