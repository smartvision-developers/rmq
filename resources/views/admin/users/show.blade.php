@extends('admin.index')
@push('css')
<style type="text/css">
	.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    background-color: #343a40;
}
</style>
@endpush
@section('content')
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<a>{{!empty($title)?$title:''}}</a>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<span class="caret"></span>
				<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('users')}}" class="dropdown-item"  style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}}</a>
				<a class="dropdown-item"  style="color:#343a40" href="{{aurl('users/'.$users->id.'/edit')}}">
					<i class="fas fa-edit"></i> {{trans('admin.edit')}}
				</a>
				<a class="dropdown-item"  style="color:#343a40" href="{{aurl('users/create')}}">
					<i class="fas fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$users->id}}" class="dropdown-item"  style="color:#343a40" href="#">
					<i class="fas fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$users->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>  {{trans('admin.ask_del')}} {{trans('admin.id')}} ({{$users->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
               'method' => 'DELETE',
               'route' => ['users.destroy', $users->id]
               ]) !!}
                {!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						 <a class="btn btn-default" data-dismiss="modal">{{trans('admin.cancel')}}</a>
                {!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xs-12">
				<b>{{trans('admin.id')}} :</b> {{$users->id}}
			</div>
			<div class="clearfix"></div>
			<hr />
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.first_name')}} :</b>
				{!! $users->first_name !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.last_name')}} :</b>
				{!! $users->last_name !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.email')}} :</b>
				{!! $users->email !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.mobile')}} :</b>
				{!! $users->mobile !!}
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.country_code')}} :</b>
				{!! $users->country_code !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.city_id')}} :</b>
				@if(!empty($users->city_id()->first()))
			{{ $users->city_id()->first()->city_name }}
			@endif
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.region_id')}} :</b>
				@if(!empty($users->region_id()->first()))
			{{ $users->region_id()->first()->region_name }}
			@endif
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.status')}} :</b>
				{{ trans("admin.".$users->status) }}
			</div>
			@if($users->status == 'rejected')
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.decline_reason')}} :</b>
				{!! $users->decline_reason !!}
			</div>
			@endif
			<!-- /.row -->
		</div>

			<div class="row">
            <ul class="nav nav-pills mb-3 mt-5" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"> @lang('admin.favorite')</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">@lang('admin.cart')</a>
              </li>
             
            </ul>
        
            <div class="tab-content col-md-12" id="pills-tabContent">
                 
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            	@if(!empty($users->favourite_id()->first()))
              	<table class="table">
              	  <thead>
				    <tr>
				      <th>#</th>
				      <th>@lang('admin.product-img') </th>
				      <th>@lang('admin.product-name') </th>
				      <th>@lang('admin.created_at') </th>
				    </tr>
				  </thead>
				  <tbody>
				  	@php $count=1; @endphp
				  	@foreach($users->favourite_id as $data)
				  		<tr>
				  			<td>{{$count}}</td>
				  			<td><img width="100px" src="{{it()->url($data->product_id()->first()->product_image)}}"></td>
				  			<td><a href="{{url('admin/products/'.$data->product_id)}}">{{$data->product_id()->first()->product_name}}</a></td>
				  			<td>{{$data->created_at}}</td>
				  		</tr>
				  		@php $count++; @endphp
				  	@endforeach
				  </tbody>
              	</table>
              		{!! $users->favourite_id->render() !!}
              	@else
              	<h4>@lang('admin.product-nothing') </h4>
              	@endif
              </div>
                         
            
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
									@if(!empty($users->cart_id()->first()))
									{{count($users->cart_id)}}
								@foreach($users->cart_id as $key => $order)
						<table class="table">
		            <thead>
						    <tr>
						      <th>#</th>
						      <th>@lang('admin.product-name') </th>
						      <th>@lang('admin.product-price') </th>
						      <th>@lang('admin.total-price') </th>
						      <th>@lang('admin.created_at') </th>
						    </tr>
						  </thead>
						  <tbody>
						  		<tr>
						  			<td>{{$loop->iteration}}</td>
						  			<td><a href="{{url('admin/products/'.$order->product_id)}}">{{$order->product_name}} </a></td>
						  			<td>{{$order->product_price}} @lang('main.sar')</td>
						  			<td>{{$order->total_price}} @lang('main.sar')</td>
						  			<td>{{$order->created_at}}</td>
						  		</tr>
						  </tbody>
		              	</table>
  					@endforeach
  					              		{!! $users->cart_id->render() !!}
              	@else
              	<h4>@lang('admin.product-nothing')  </h4>
              	@endif
              </div>
                      
            </div>


		</div>
	</div>
	<!-- /.card-body -->
	<div class="card-footer">
	</div>
</div>
@endsection