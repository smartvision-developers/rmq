<!-- Add icons to the links using the .nav-icon class
with font-awesome or any other icon font library -->
<li class="nav-header"></li>
<li class="nav-item">
  <a href="{{ aurl('') }}" class="nav-link {{ active_link(null,'active') }}">
    <i class="nav-icon fas fa-home"></i>
    <p>
      {{ trans('admin.dashboard') }}
    </p>
  </a>
</li>
@if(admin()->user()->role('settings_show'))
<li class="nav-item">
  <a href="{{ aurl('settings') }}" class="nav-link  {{ active_link('settings','active') }}">
    <i class="nav-icon fas fa-cogs"></i>
    <p>
      {{ trans('admin.settings') }}
    </p>
  </a>
</li>
@endif
@if(admin()->user()->role("admins_show"))
<li class="nav-item {{ active_link('admins','menu-open') }}">
  <a href="#" class="nav-link  {{ active_link('admins','active') }}">
    <i class="nav-icon fas fa-users"></i>
    <p>
      {{trans('admin.admins')}}
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('admins')}}" class="nav-link {{ active_link('admins','active') }}">
        <i class="fas fa-users nav-icon"></i>
        <p>{{trans('admin.admins')}}</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('admins/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}}</p>
      </a>
    </li>
  </ul>
</li>
@endif
@if(admin()->user()->role("admingroups_show"))
<li class="nav-item {{ active_link('admingroups','menu-open') }}">
  <a href="#" class="nav-link  {{ active_link('admingroups','active') }}">
    <i class="nav-icon fas fa-users"></i>
    <p>
      {{trans('admin.admingroups')}}
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('admingroups')}}" class="nav-link {{ active_link('admingroups','active') }}">
        <i class="fas fa-users nav-icon"></i>
        <p>{{trans('admin.admingroups')}}</p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('admingroups/create') }}" class="nav-link ">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}}</p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--users_start_route-->
@if(admin()->user()->role("users_show"))
<li class="nav-item {{active_link('users','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('users','active')}}">
    <i class="nav-icon fa fa-user"></i>
    <p>
      {{trans('admin.users')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('users')}}" class="nav-link  {{active_link('users','active')}}">
        <i class="fa fa-user nav-icon"></i>
        <p>{{trans('admin.users')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('users/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--users_end_route-->
<!--countrys_start_route-->
<!-- @if(admin()->user()->role("countrys_show"))
<li class="nav-item {{active_link('countrys','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('countrys','active')}}">
    <i class="nav-icon fa fa-flag"></i>
    <p>
      {{trans('admin.countrys')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('countrys')}}" class="nav-link  {{active_link('countrys','active')}}">
        <i class="fa fa-flag nav-icon"></i>
        <p>{{trans('admin.countrys')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('countrys/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif -->
<!--countrys_end_route-->

<!--citys_start_route-->
@if(admin()->user()->role("citys_show"))
<li class="nav-item {{active_link('citys','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('citys','active')}}">
    <i class="nav-icon fa fa-map-marker-alt"></i>
    <p>
      {{trans('admin.citys')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('citys')}}" class="nav-link  {{active_link('citys','active')}}">
        <i class="fa fa-map-marker-alt nav-icon"></i>
        <p>{{trans('admin.citys')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('citys/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--citys_end_route-->

<!--regions_start_route-->
@if(admin()->user()->role("regions_show"))
<li class="nav-item {{active_link('regions','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('regions','active')}}">
    <i class="nav-icon fa fa-map-marker"></i>
    <p>
      {{trans('admin.regions')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('regions')}}" class="nav-link  {{active_link('regions','active')}}">
        <i class="fa fa-map-marker nav-icon"></i>
        <p>{{trans('admin.regions')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('regions/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--regions_end_route-->

<!--sliders_start_route-->
@if(admin()->user()->role("sliders_show"))
<li class="nav-item {{active_link('sliders','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('sliders','active')}}">
    <i class="nav-icon fa fa-sliders-h"></i>
    <p>
      {{trans('admin.sliders')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('sliders')}}" class="nav-link  {{active_link('sliders','active')}}">
        <i class="fa fa-sliders-h nav-icon"></i>
        <p>{{trans('admin.sliders')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('sliders/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--sliders_end_route-->

<!--contacts_start_route-->
@if(admin()->user()->role("contacts_show"))
<li class="nav-item {{active_link('contacts','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('contacts','active')}}">
    <i class="nav-icon fa fa-file-contract"></i>
    <p>
      {{trans('admin.contacts')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('contacts')}}" class="nav-link  {{active_link('contacts','active')}}">
        <i class="fa fa-file-contract nav-icon"></i>
        <p>{{trans('admin.contacts')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('contacts/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--contacts_end_route-->

<!--abouts_start_route-->
@if(admin()->user()->role("abouts_show"))
<li class="nav-item {{active_link('abouts','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('abouts','active')}}">
    <i class="nav-icon fa fa-feather"></i>
    <p>
      {{trans('admin.abouts')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('abouts')}}" class="nav-link  {{active_link('abouts','active')}}">
        <i class="fa fa-feather nav-icon"></i>
        <p>{{trans('admin.abouts')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('abouts/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--abouts_end_route-->

<!--websitelinks_start_route-->
@if(admin()->user()->role("websitelinks_show"))
<li class="nav-item {{active_link('websitelinks','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('websitelinks','active')}}">
    <i class="nav-icon fa fa-link"></i>
    <p>
      {{trans('admin.websitelinks')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('websitelinks')}}" class="nav-link  {{active_link('websitelinks','active')}}">
        <i class="fa fa-link nav-icon"></i>
        <p>{{trans('admin.websitelinks')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('websitelinks/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--websitelinks_end_route-->

<!--products_start_route-->
@if(admin()->user()->role("products_show"))
<li class="nav-item {{active_link('products','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('products','active')}}">
    <i class="nav-icon fa fa-paint-brush"></i>
    <p>
      {{trans('admin.products')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('products')}}" class="nav-link  {{active_link('products','active')}}">
        <i class="fa fa-paint-brush nav-icon"></i>
        <p>{{trans('admin.products')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('products/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
     <li class="nav-item">
      <a href="{{ aurl('productdimensions/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.productdimensions')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--products_end_route-->

<!--productfavourites_start_route-->
@if(admin()->user()->role("productfavourites_show"))
<li class="nav-item {{active_link('productfavourites','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('productfavourites','active')}}">
    <i class="nav-icon fa fa-heart"></i>
    <p>
      {{trans('admin.productfavourites')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('productfavourites')}}" class="nav-link  {{active_link('productfavourites','active')}}">
        <i class="fa fa-heart nav-icon"></i>
        <p>{{trans('admin.productfavourites')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('productfavourites/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--productfavourites_end_route-->

<!--mostpopulars_start_route-->
@if(admin()->user()->role("mostpopulars_show"))
<li class="nav-item {{active_link('mostpopulars','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('mostpopulars','active')}}">
    <i class="nav-icon fa fa-brush"></i>
    <p>
      {{trans('admin.mostpopulars')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('mostpopulars')}}" class="nav-link  {{active_link('mostpopulars','active')}}">
        <i class="fa fa-brush nav-icon"></i>
        <p>{{trans('admin.mostpopulars')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('mostpopulars/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--mostpopulars_end_route-->

<!--statistics_start_route-->
@if(admin()->user()->role("statistics_show"))
<li class="nav-item {{active_link('statistics','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('statistics','active')}}">
    <i class="nav-icon fa fa-chart-line"></i>
    <p>
      {{trans('admin.statistics')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('statistics')}}" class="nav-link  {{active_link('statistics','active')}}">
        <i class="fa fa-chart-line nav-icon"></i>
        <p>{{trans('admin.statistics')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('statistics/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--statistics_end_route-->

<!--sociallinks_start_route-->
@if(admin()->user()->role("sociallinks_show"))
<li class="nav-item {{active_link('sociallinks','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('sociallinks','active')}}">
    <i class="nav-icon fa fa-link"></i>
    <p>
      {{trans('admin.sociallinks')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('sociallinks')}}" class="nav-link  {{active_link('sociallinks','active')}}">
        <i class="fa fa-link nav-icon"></i>
        <p>{{trans('admin.sociallinks')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('sociallinks/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--sociallinks_end_route-->

<!--tradmarks_start_route-->
@if(admin()->user()->role("tradmarks_show"))
<li class="nav-item {{active_link('tradmarks','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('tradmarks','active')}}">
    <i class="nav-icon fa fa-trademark"></i>
    <p>
      {{trans('admin.tradmarks')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('tradmarks')}}" class="nav-link  {{active_link('tradmarks','active')}}">
        <i class="fa fa-trademark nav-icon"></i>
        <p>{{trans('admin.tradmarks')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('tradmarks/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--tradmarks_end_route-->

<!--productbooks_start_route-->
@if(admin()->user()->role("productbooks_show"))
<li class="nav-item {{active_link('productbooks','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('productbooks','active')}}">
    <i class="nav-icon fa fa-book"></i>
    <p>
      {{trans('admin.productbooks')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('productbooks')}}" class="nav-link  {{active_link('productbooks','active')}}">
        <i class="fa fa-book nav-icon"></i>
        <p>{{trans('admin.productbooks')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('productbooks/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--productbooks_end_route-->

<!--carts_start_route-->
@if(admin()->user()->role("carts_show"))
<li class="nav-item {{active_link('carts','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('carts','active')}}">
    <i class="nav-icon fa fa-shopping-cart"></i>
    <p>
      {{trans('admin.carts')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('carts')}}" class="nav-link  {{active_link('carts','active')}}">
        <i class="fa fa-shopping-cart nav-icon"></i>
        <p>{{trans('admin.carts')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('carts/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--carts_end_route-->

<!--productdimensions_start_route-->
<!-- @if(admin()->user()->role("productdimensions_show"))
<li class="nav-item {{active_link('productdimensions','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('productdimensions','active')}}">
    <i class="nav-icon fa fa-icons"></i>
    <p>
      {{trans('admin.productdimensions')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('productdimensions')}}" class="nav-link  {{active_link('productdimensions','active')}}">
        <i class="fa fa-icons nav-icon"></i>
        <p>{{trans('admin.productdimensions')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('productdimensions/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif -->
<!--productdimensions_end_route-->

<!--productcoupons_start_route-->
@if(admin()->user()->role("productcoupons_show"))
<li class="nav-item {{active_link('productcoupons','menu-open')}} ">
  <a href="#" class="nav-link {{active_link('productcoupons','active')}}">
    <i class="nav-icon fa fa-icons"></i>
    <p>
      {{trans('admin.productcoupons')}} 
      <i class="right fas fa-angle-left"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{aurl('productcoupons')}}" class="nav-link  {{active_link('productcoupons','active')}}">
        <i class="fa fa-icons nav-icon"></i>
        <p>{{trans('admin.productcoupons')}} </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ aurl('productcoupons/create') }}" class="nav-link">
        <i class="fas fa-plus nav-icon"></i>
        <p>{{trans('admin.create')}} </p>
      </a>
    </li>
  </ul>
</li>
@endif
<!--productcoupons_end_route-->
