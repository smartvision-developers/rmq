<!--admingroups_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ App\Models\AdminGroup::count() }}</h3>
        <p>{{ trans('admin.admingroups') }}</p>
      </div>
      <div class="icon">
        <i class="fas fa-users"></i>
      </div>
      <a href="{{ aurl('admingroups') }}" class="small-box-footer">{{ trans('admin.admingroups') }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
<!--admingroups_end-->
<!--admins_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ App\Models\Admin::count() }}</h3>
        <p>{{ trans('admin.admins') }}</p>
      </div>
      <div class="icon">
        <i class="fas fa-users"></i>
      </div>
      <a href="{{ aurl('admins') }}" class="small-box-footer">{{ trans('admin.admins') }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
<!--admins_end-->

<!--countrys_start-->
<div class="col-lg-3 col-6" style="display: none;">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Country::count()) }}</h3>
        <p>{{ trans("admin.countrys") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-flag"></i>
      </div>
      <a href="{{ aurl("countrys") }}" class="small-box-footer">{{ trans("admin.countrys") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--countrys_end-->
<!--citys_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\City::count()) }}</h3>
        <p>{{ trans("admin.citys") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("citys") }}" class="small-box-footer">{{ trans("admin.citys") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--citys_end-->
<!--regions_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Region::count()) }}</h3>
        <p>{{ trans("admin.regions") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("regions") }}" class="small-box-footer">{{ trans("admin.regions") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--regions_end-->
<!--sliders_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Slider::count()) }}</h3>
        <p>{{ trans("admin.sliders") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("sliders") }}" class="small-box-footer">{{ trans("admin.sliders") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--sliders_end-->
<!--contacts_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Contact::count()) }}</h3>
        <p>{{ trans("admin.contacts") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-file-contract"></i>
      </div>
      <a href="{{ aurl("contacts") }}" class="small-box-footer">{{ trans("admin.contacts") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--contacts_end-->

<!--abouts_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\About::count()) }}</h3>
        <p>{{ trans("admin.abouts") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-feather"></i>
      </div>
      <a href="{{ aurl("abouts") }}" class="small-box-footer">{{ trans("admin.abouts") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--abouts_end-->
<!--websitelinks_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\WebsiteLink::count()) }}</h3>
        <p>{{ trans("admin.websitelinks") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-link"></i>
      </div>
      <a href="{{ aurl("websitelinks") }}" class="small-box-footer">{{ trans("admin.websitelinks") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--websitelinks_end-->
<!--products_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Product::count()) }}</h3>
        <p>{{ trans("admin.products") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("products") }}" class="small-box-footer">{{ trans("admin.products") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--products_end-->
<!--productfavourites_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\ProductFavourite::count()) }}</h3>
        <p>{{ trans("admin.productfavourites") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-heart"></i>
      </div>
      <a href="{{ aurl("productfavourites") }}" class="small-box-footer">{{ trans("admin.productfavourites") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--productfavourites_end-->
<!--mostpopulars_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\MostPopular::count()) }}</h3>
        <p>{{ trans("admin.mostpopulars") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("mostpopulars") }}" class="small-box-footer">{{ trans("admin.mostpopulars") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--mostpopulars_end-->
<!--statistics_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Statistic::count()) }}</h3>
        <p>{{ trans("admin.statistics") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-chart-line"></i>
      </div>
      <a href="{{ aurl("statistics") }}" class="small-box-footer">{{ trans("admin.statistics") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--statistics_end-->
<!--sociallinks_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\SocialLink::count()) }}</h3>
        <p>{{ trans("admin.sociallinks") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-link"></i>
      </div>
      <a href="{{ aurl("sociallinks") }}" class="small-box-footer">{{ trans("admin.sociallinks") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--sociallinks_end-->
<!--tradmarks_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Tradmark::count()) }}</h3>
        <p>{{ trans("admin.tradmarks") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-trademark"></i>
      </div>
      <a href="{{ aurl("tradmarks") }}" class="small-box-footer">{{ trans("admin.tradmarks") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--tradmarks_end-->
<!--productbooks_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\ProductBook::count()) }}</h3>
        <p>{{ trans("admin.productbooks") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-book"></i>
      </div>
      <a href="{{ aurl("productbooks") }}" class="small-box-footer">{{ trans("admin.productbooks") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--productbooks_end-->
<!--carts_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\Cart::count()) }}</h3>
        <p>{{ trans("admin.carts") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-shopping-cart"></i>
      </div>
      <a href="{{ aurl("carts") }}" class="small-box-footer">{{ trans("admin.carts") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--carts_end-->
<!--productdimensions_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\ProductDimension::count()) }}</h3>
        <p>{{ trans("admin.productdimensions") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("productdimensions") }}" class="small-box-footer">{{ trans("admin.productdimensions") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--productdimensions_end-->
<!--productcoupons_start-->
<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-primary">
      <div class="inner">
        <h3>{{ mK(App\Models\ProductCoupon::count()) }}</h3>
        <p>{{ trans("admin.productcoupons") }}</p>
      </div>
      <div class="icon">
        <i class="fa fa-icons"></i>
      </div>
      <a href="{{ aurl("productcoupons") }}" class="small-box-footer">{{ trans("admin.productcoupons") }} <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>
<!--productcoupons_end-->