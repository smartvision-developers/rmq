@extends('admin.index')
@section('content')
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<a>{{!empty($title)?$title:''}}</a>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<span class="caret"></span>
				<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('products')}}" class="dropdown-item"  style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}}</a>
				<a class="dropdown-item"  style="color:#343a40" href="{{aurl('products/'.$products->id.'/edit')}}">
					<i class="fas fa-edit"></i> {{trans('admin.edit')}}
				</a>
				<a class="dropdown-item"  style="color:#343a40" href="{{aurl('products/create')}}">
					<i class="fas fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$products->id}}" class="dropdown-item"  style="color:#343a40" href="#">
					<i class="fas fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$products->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>  {{trans('admin.ask_del')}} {{trans('admin.id')}} ({{$products->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
               'method' => 'DELETE',
               'route' => ['products.destroy', $products->id]
               ]) !!}
                {!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						 <a class="btn btn-default" data-dismiss="modal">{{trans('admin.cancel')}}</a>
                {!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="col-md-12 col-lg-12 col-xs-12">
				<b>{{trans('admin.id')}} :</b> {{$products->id}}
			</div>
			<div class="clearfix"></div>
			<hr />
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.product_name')}} :</b>
				{!! $products->product_name !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.number_floor')}} :</b>
				{!! $products->number_floor !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.number_room')}} :</b>
				{!! $products->number_room !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.land_area')}} :</b>
				{!! $products->land_area !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.design_by')}} :</b>
				{!! $products->design_by !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.price')}} :</b>
				{!! $products->price !!} @lang('main.sar')
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.stock')}} :</b>
				{!! $products->stock !!}
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
				<b>{{trans('admin.sold')}} :</b>
				{!! $products->sold !!} 
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<b>{{trans('admin.product_description')}} :</b>
				{!! $products->product_description !!}
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<b>{{trans('admin.product_content')}} :</b>
				{!! $products->product_content !!}
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<b>{{trans('admin.chart_information')}} :</b>
				{!! $products->chart_information !!}
			</div>
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<b>{{trans('admin.notes')}} :</b>
				{!! $products->notes !!}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.product_image')}} :</b>
				@include("admin.show_image",["image"=>$products->product_image])
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-md-8 col-lg-4 col-xs-12">
					  <b>{{trans('admin.product_video')}} :</b>
					</div>
					<div class="col-md-2 col-lg-2 col-xs-12">
						@include("admin.show_video",["video"=>$products->product_video])
					</div>
					<div class="col-md-2 col-lg-2 col-xs-12">
						<a href="{{ it()->url($products->product_video) }}" target="_blank"><i class="fa fa-download fa-2x"></i></a>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-md-8 col-lg-4 col-xs-12">
					  <b>{{trans('admin.design_book')}} :</b>
					</div>
					<div class="col-md-2 col-lg-2 col-xs-12">
						
					</div>
					<div class="col-md-2 col-lg-2 col-xs-12">
						<a href="{{ it()->url($products->design_book) }}" target="_blank"><i class="fa fa-download fa-2x"></i></a>
					</div>
				</div>
			</div>
			<!-- /.row -->

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.status')}} :</b>
				{{ trans("admin.".$products->status) }}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<b>{{trans('admin.view_count')}} :</b>
				{!! $products->view_count !!}
			</div>
		</div>
	</div>
	<!-- /.card-body -->
	<div class="card-footer">
	</div>
</div>
@endsection