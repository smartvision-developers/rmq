@extends('admin.index')
@push('css')
<style type="text/css">
    .land_area-style{
        font-weight: bolder;
    position: absolute;
    top: 35px;
    left: 33px;
    font-size: 16px;
    }
</style>
@endpush
@section('content')
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>
			{{ !empty($title)?$title:'' }}
			</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{ aurl('products') }}"  style="color:#343a40"  class="dropdown-item">
				<i class="fas fa-list"></i> {{ trans('admin.show_all') }}</a>
			</div>
		</div>
		</h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
								
{!! Form::open(['url'=>aurl('/products'),'id'=>'products','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('product_name',trans('admin.product_name'),['class'=>' control-label']) !!}
            {!! Form::text('product_name',old('product_name'),['class'=>'form-control','placeholder'=>trans('admin.product_name')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('number_floor',trans('admin.number_floor'),['class'=>' control-label']) !!}
            {!! Form::text('number_floor',old('number_floor'),['class'=>'form-control','placeholder'=>trans('admin.number_floor')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('number_room',trans('admin.number_room'),['class'=>' control-label']) !!}
            {!! Form::text('number_room',old('number_room'),['class'=>'form-control','placeholder'=>trans('admin.number_room')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('land_area',trans('admin.land_area'),['class'=>' control-label']) !!}
            {!! Form::text('land_area',old('land_area'),['class'=>'form-control','placeholder'=>trans('admin.land_area')]) !!}
             <span class="land_area-style">{{trans('admin.meter-square')}}</span>
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('design_by',trans('admin.design_by'),['class'=>' control-label']) !!}
            {!! Form::text('design_by',old('design_by'),['class'=>'form-control','placeholder'=>trans('admin.design_by')]) !!}
    </div>
</div>

<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('price',trans('admin.price'),['class'=>' control-label']) !!}
            {!! Form::text('price',old('price'),['class'=>'form-control','placeholder'=>trans('admin.price')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-3 col-xs-6">
    <div class="form-group">
        {!! Form::label('stock',trans('admin.stock'),['class'=>' control-label']) !!}
            {!! Form::number('stock',old('stock'),['min' => '1','class'=>'form-control','placeholder'=>trans('admin.stock')]) !!}
    </div>
</div>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div class="form-group">
        {!! Form::label('product_description',trans('admin.product_description'),['class'=>'control-label']) !!}
            {!! Form::textarea('product_description',old('product_description'),['class'=>'form-control','placeholder'=>trans('admin.product_description')]) !!}
    </div>
</div>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
     @include("admin.dropzone",[
    "thumbnailWidth"=>"80",
    "thumbnailHeight"=>"80",
    "parallelUploads"=>"20",
    "maxFiles"=>"30",
    "maxFileSize"=>"",
    "acceptedMimeTypes"=>it()->acceptedMimeTypes("image"),
    "autoQueue"=>true,
    "dz_param"=>"product_photos",
    "type"=>"create",
    "route"=>"products",
    "path"=>"products",
    ])
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 product_video">
    <div class="form-group">
        <label for="'product_video'">{{ trans('admin.product_video') }}</label>
        <div class="input-group">
            <div class="custom-file">
                {!! Form::file('product_video',['class'=>'custom-file-input','placeholder'=>trans('admin.product_video'),"accept"=>it()->acceptedMimeTypes("mp4"),"id"=>"product_video"]) !!}
                {!! Form::label('product_video',trans('admin.product_video'),['class'=>'custom-file-label']) !!}
            </div>
            <div class="input-group-append">
                <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 product_image">
    <div class="form-group">
        <label for="'product_image'">{{ trans('admin.product_image') }}</label>
        <div class="input-group">
            <div class="custom-file">
                {!! Form::file('product_image',['class'=>'custom-file-input','placeholder'=>trans('admin.product_image'),"accept"=>it()->acceptedMimeTypes("image"),"id"=>"product_image"]) !!}
                {!! Form::label('product_image',trans('admin.product_image'),['class'=>'custom-file-label']) !!}
            </div>
            <div class="input-group-append">
                <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div class="form-group">
        {!! Form::label('product_content',trans('admin.product_content'),['class'=>'control-label']) !!}
            {!! Form::textarea('product_content',old('product_content'),['class'=>'form-control ckeditor','placeholder'=>trans('admin.product_content')]) !!}
    </div>
</div>
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div class="form-group">
        {!! Form::label('chart_information',trans('admin.chart_information'),['class'=>'control-label']) !!}
            {!! Form::textarea('chart_information',old('chart_information'),['class'=>'form-control ckeditor','placeholder'=>trans('admin.chart_information')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 design_book">
    <div class="form-group">
        <label for="'design_book'">{{ trans('admin.design_book') }}</label>
        <div class="input-group">
            <div class="custom-file">
                {!! Form::file('design_book',['class'=>'custom-file-input','placeholder'=>trans('admin.design_book'),"accept"=>it()->acceptedMimeTypes(""),"id"=>"design_book"]) !!}
                {!! Form::label('design_book',trans('admin.design_book'),['class'=>'custom-file-label']) !!}
            </div>
            <div class="input-group-append">
                <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div class="form-group">
        {!! Form::label('notes',trans('admin.notes'),['class'=>'control-label']) !!}
            {!! Form::textarea('notes',old('notes'),['class'=>'form-control ckeditor','placeholder'=>trans('admin.notes')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('status',trans('admin.status')) !!}
        {!! Form::select('status',['show'=>trans('admin.show'),'hide'=>trans('admin.hide'),],old('status'),['class'=>'form-control select2','placeholder'=>trans('admin.choose')]) !!}
    </div>
</div>
</div>
		<!-- /.row -->
	</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="add" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add') }}</button>
<button type="submit" name="add_back" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add_back') }}</button>
{!! Form::close() !!}	</div>
</div>
@endsection