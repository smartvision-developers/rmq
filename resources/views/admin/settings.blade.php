@extends('admin.index')
@push('css')
<style type="text/css">
    .vat-style{
        font-weight: bolder;
    position: absolute;
    top: 33px;
    left: 33px;
    font-size: 23px;
    }
</style>
@endpush
@section('content')
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">{{!empty($title)?$title:''}}</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        {!! Form::open(['url'=>aurl('/settings'),'id'=>'settings','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('sitename_ar',trans('admin.sitename_ar'),['class'=>'control-label']) !!}
                {!! Form::text('sitename_ar',setting()->sitename_ar,['class'=>'form-control','placeholder'=>trans('admin.sitename_ar')]) !!}
            </div>
           <!--  <div class="form-group col-md-6">
                {!! Form::label('sitename_en',trans('admin.sitename_en'),['class'=>'control-label']) !!}
                {!! Form::text('sitename_en',setting()->sitename_en,['class'=>'form-control','placeholder'=>trans('admin.sitename_en')]) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('sitename_fr',trans('admin.sitename_fr'),['class'=>'control-label']) !!}
                {!! Form::text('sitename_fr',setting()->sitename_fr,['class'=>'form-control','placeholder'=>trans('admin.sitename_fr')]) !!}
            </div> -->
            <div class="form-group col-md-6">
                {!! Form::label('email',trans('admin.email'),['class'=>'control-label']) !!}
                {!! Form::text('email',setting()->email,['class'=>'form-control','placeholder'=>trans('admin.email')]) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('mobile',trans('admin.mobile'),['class'=>'control-label']) !!}
                {!! Form::text('mobile',setting()->mobile,['class'=>'form-control','placeholder'=>trans('admin.mobile')]) !!}
            </div>

            <div class="form-group col-md-6">
                {!! Form::label('address',trans('admin.address'),['class'=>'control-label']) !!}
                {!! Form::text('address',setting()->address,['class'=>'form-control','placeholder'=>trans('admin.address')]) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('commercial_registration_no',trans('admin.commercial_registration_no'),['class'=>'control-label']) !!}
                {!! Form::text('commercial_registration_no',setting()->commercial_registration_no,['class'=>'form-control','placeholder'=>trans('admin.commercial_registration_no')]) !!}
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('tax_no',trans('admin.tax_no'),['class'=>'control-label']) !!}
                {!! Form::text('tax_no',setting()->tax_no,['class'=>'form-control','placeholder'=>trans('admin.tax_no')]) !!} 
            </div>
            <div class="form-group col-md-6">
                {!! Form::label('vat',trans('admin.vat'),['class'=>'control-label']) !!}
                {!! Form::text('vat',setting()->vat,['class'=>'form-control','placeholder'=>trans('admin.vat')]) !!} 
                <span class="vat-style">%</span>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="exampleInputFile">{{ trans('admin.logo') }}</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    {!! Form::file('logo',['class'=>'custom-file-input','placeholder'=>trans('admin.logo')]) !!}
                                    {!! Form::label('logo',trans('admin.logo'),['class'=>'custom-file-label']) !!}
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <br />
                        @include('admin.show_image',['image'=>setting()->logo])
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="exampleInputFile">{{ trans('admin.icon') }}</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    {!! Form::file('icon',['class'=>'custom-file-input','placeholder'=>trans('admin.icon')]) !!}
                                    {!! Form::label('icon',trans('admin.icon'),['class'=>'custom-file-label']) !!}
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <br />
                        @include('admin.show_image',['image'=>setting()->icon])
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                {!! Form::label('about_us',trans('admin.about_us'),['class'=>'control-label']) !!}
                {!! Form::textarea('about_us',setting()->about_us,['class'=>'form-control','placeholder'=>trans('admin.about_us')]) !!}
            </div>
       
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                 @include("admin.dropzone",[
                "thumbnailWidth"=>"80",
                "thumbnailHeight"=>"80",
                "parallelUploads"=>"20",
                "maxFiles"=>"30",
                "maxFileSize"=>"",
                "acceptedMimeTypes"=>it()->acceptedMimeTypes("image"),
                "autoQueue"=>true,
                "dz_param"=>"about_image",
                "type"=>"edit",
                "id"=>setting()->id,
                "route"=>"settings",
                "path"=>"setting/".setting()->id,
                ])
            </div>
              <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="exampleInputFile">{{ trans('admin.video') }}</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    {!! Form::file('video',['class'=>'custom-file-input','placeholder'=>trans('admin.video'),"accept"=>it()->acceptedMimeTypes("mp4"),"id"=>"video"]) !!}
                                    {!! Form::label('video',trans('admin.video'),['class'=>'custom-file-label']) !!}
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <br />
                        @include('admin.show_video',['video'=>setting()->video])
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <input type="hidden" name="lat" id="lat" value="{{setting()->lat}}">
                <input type="hidden" name="lng" id="lng" value="{{setting()->lng}}">
                {!! Form::label('site_location',trans('admin.site_location'),['class'=>'control-label']) !!}
                <div  id="latlongmap" style="height:250px;margin-right: 162px; left: 16%;"></div>

            </div>
            <div class="form-group col-md-12">
                {!! Form::label('policy',trans('admin.policy'),['class'=>'control-label']) !!}
                {!! Form::textarea('policy',setting()->policy,['class'=>'form-control','placeholder'=>trans('admin.policy')]) !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::label('system_status',trans('admin.system_status'),['class'=>'control-label']) !!}
                {!! Form::select('system_status',['open'=>trans('admin.open'),'close'=>trans('admin.close')],setting()->system_status,['class'=>'form-control','placeholder'=>trans('admin.system_status')]) !!}
            </div>
            <div class="form-group col-md-12">
                {!! Form::label('system_message',trans('admin.system_message'),['class'=>'control-label']) !!}
                {!! Form::textarea('system_message',setting()->system_message,['class'=>'form-control','placeholder'=>trans('admin.system_message')]) !!}
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        {!! Form::submit(trans('admin.save'),['class'=>'btn btn-success btn-flat']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection
@push('js')
<script>
   function initialize() {
    var e = new google.maps.LatLng({{setting()->lat}}, {{setting()->lng}}), t = {
        zoom: 8,
        center: e,
        panControl: !0,
        scrollwheel: !1,
        scaleControl: !0,
        overviewMapControl: !0,
        overviewMapControlOptions: {opened: !0},
        mapTypeId: google.maps.MapTypeId.terrain
    };
    map = new google.maps.Map(document.getElementById("latlongmap"), t), geocoder = new google.maps.Geocoder, marker = new google.maps.Marker({
        position: e,
        map: map
    }), map.streetViewControl = !1, infowindow =
    new google.maps.InfoWindow({content: "({{setting()->lat}}, {{setting()->lng}}))"}), google.maps.event.addListener(map, "click", function (e) {
        marker.setPosition(e.latLng);
        var t = e.latLng, o = "(" + t.lat().toFixed(6) + ", " + t.lng().toFixed(6) + ")";
        infowindow.setContent(o),
        document.getElementById("lat").value = t.lat().toFixed(6),
        document.getElementById("lng").value = t.lng().toFixed(6)
    })
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_wPc4ukHgXijCSXQ35li3Bkipx-XfM1E&callback=initialize"></script>
@endpush
