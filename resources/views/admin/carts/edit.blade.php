@extends('admin.index')
@section('content')
@include('admin.ajax',[
	'typeForm'=>'edit',
	'selectID'=>'city_id',
	'parentValue'=>$carts->city_id,
	'outputClass'=>'region_id',
	'selectedvalue'=>$carts->region_id,
	'url'=>aurl('carts/get/region/id'),
])


<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>{{!empty($title)?$title:''}}</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('carts')}}" class="dropdown-item" style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}} </a>
				<a href="{{aurl('carts/'.$carts->id)}}" class="dropdown-item" style="color:#343a40">
				<i class="fa fa-eye"></i> {{trans('admin.show')}} </a>
				<a class="dropdown-item" style="color:#343a40" href="{{aurl('carts/create')}}">
					<i class="fa fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$carts->id}}" class="dropdown-item" style="color:#343a40" href="#">
					<i class="fa fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$carts->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>   {{trans('admin.ask_del')}} {{trans('admin.id')}}  ({{$carts->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
						'method' => 'DELETE',
						'route' => ['carts.destroy', $carts->id]
						]) !!}
						{!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						<a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('admin.cancel')}}</a>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
										
{!! Form::open(['url'=>aurl('/carts/'.$carts->id),'method'=>'put','id'=>'carts','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('product_name',trans('admin.product_name'),['class'=>'control-label']) !!}
        {!! Form::text('product_name', $carts->product_name ,['class'=>'form-control','placeholder'=>trans('admin.product_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('product_price',trans('admin.product_price'),['class'=>'control-label']) !!}
        {!! Form::text('product_price', $carts->product_price ,['class'=>'form-control','placeholder'=>trans('admin.product_price')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('product_id',trans('admin.product_id'),['class'=>'control-label']) !!}
{!! Form::select('product_id',App\Models\Product::pluck('product_name','id'), $carts->product_id ,['class'=>'form-control select2','placeholder'=>trans('admin.product_id')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('client_id',trans('admin.client_id'),['class'=>'control-label']) !!}
{!! Form::select('client_id',App\Models\User::pluck('first_name','id'), $carts->client_id ,['class'=>'form-control select2','placeholder'=>trans('admin.client_id')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('client_name',trans('admin.client_name'),['class'=>'control-label']) !!}
        {!! Form::text('client_name', $carts->client_name ,['class'=>'form-control','placeholder'=>trans('admin.client_name')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('client_mobile',trans('admin.client_mobile'),['class'=>'control-label']) !!}
        {!! Form::text('client_mobile', $carts->client_mobile ,['class'=>'form-control','placeholder'=>trans('admin.client_mobile')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('client_email',trans('admin.client_email'),['class'=>'control-label']) !!}
            {!! Form::email('client_email', $carts->client_email ,['class'=>'form-control','placeholder'=>trans('admin.client_email')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('client_id_card',trans('admin.client_id_card'),['class'=>'control-label']) !!}
        {!! Form::text('client_id_card', $carts->client_id_card ,['class'=>'form-control','placeholder'=>trans('admin.client_id_card')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('client_job',trans('admin.client_job'),['class'=>'control-label']) !!}
        {!! Form::text('client_job', $carts->client_job ,['class'=>'form-control','placeholder'=>trans('admin.client_job')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 report_image">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <label for="'report_image'">{{ trans('admin.report_image') }}</label>
                <div class="input-group">
                    <div class="custom-file">
                        {!! Form::file('report_image',['class'=>'custom-file-input','placeholder'=>trans('admin.report_image'),"accept"=>it()->acceptedMimeTypes("image"),"id"=>"report_image"]) !!}
                        {!! Form::label('report_image',trans('admin.report_image'),['class'=>'custom-file-label']) !!}
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">{{ trans('admin.upload') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="padding-top: 30px;">
            @include("admin.show_image",["image"=>$carts->report_image])
        </div>
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('city_id',trans('admin.city_id'),['class'=>'control-label']) !!}
{!! Form::select('city_id',App\Models\City::pluck('city_name','id'), $carts->city_id ,['class'=>'form-control select2','placeholder'=>trans('admin.city_id')]) !!}
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
		<div class="form-group">
				{!! Form::label('region_id',trans('admin.region_id'),['class'=>'control-label']) !!}
		<span class="region_id"></span>
		</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('vat',trans('admin.vat'),['class'=>'control-label']) !!}
        {!! Form::text('vat', $carts->vat ,['class'=>'form-control','placeholder'=>trans('admin.vat')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('total_price',trans('admin.total_price'),['class'=>'control-label']) !!}
        {!! Form::text('total_price', $carts->total_price ,['class'=>'form-control','placeholder'=>trans('admin.total_price')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('north',trans('admin.north'),['class'=>'control-label']) !!}
        {!! Form::text('north', $carts->north ,['class'=>'form-control','placeholder'=>trans('admin.north')]) !!}
    </div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('east',trans('admin.east'),['class'=>'control-label']) !!}
        {!! Form::text('east', $carts->east ,['class'=>'form-control','placeholder'=>trans('admin.east')]) !!}
    </div>
</div>

</div>
		<!-- /.row -->
		</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="save" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save') }}</button>
<button type="submit" name="save_back" class="btn btn-success btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save_back') }}</button>
{!! Form::close() !!}
</div>
</div>
@endsection