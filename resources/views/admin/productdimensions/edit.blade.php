@extends('admin.index')
@section('content')

@include("admin.layouts.components.submit_form_ajax",["form"=>"#productdimensions"])
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>{{!empty($title)?$title:''}}</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{aurl('productdimensions')}}" class="dropdown-item" style="color:#343a40">
				<i class="fas fa-list"></i> {{trans('admin.show_all')}} </a>
				<a href="{{aurl('productdimensions/'.$productdimensions->id)}}" class="dropdown-item" style="color:#343a40">
				<i class="fa fa-eye"></i> {{trans('admin.show')}} </a>
				<a class="dropdown-item" style="color:#343a40" href="{{aurl('productdimensions/create')}}">
					<i class="fa fa-plus"></i> {{trans('admin.create')}}
				</a>
				<div class="dropdown-divider"></div>
				<a data-toggle="modal" data-target="#deleteRecord{{$productdimensions->id}}" class="dropdown-item" style="color:#343a40" href="#">
					<i class="fa fa-trash"></i> {{trans('admin.delete')}}
				</a>
			</div>
		</div>
		</h3>
		@push('js')
		<div class="modal fade" id="deleteRecord{{$productdimensions->id}}">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">{{trans('admin.delete')}}</h4>
						<button class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<i class="fa fa-exclamation-triangle"></i>   {{trans('admin.ask_del')}} {{trans('admin.id')}}  ({{$productdimensions->id}})
					</div>
					<div class="modal-footer">
						{!! Form::open([
						'method' => 'DELETE',
						'route' => ['productdimensions.destroy', $productdimensions->id]
						]) !!}
						{!! Form::submit(trans('admin.approval'), ['class' => 'btn btn-danger btn-flat']) !!}
						<a class="btn btn-default btn-flat" data-dismiss="modal">{{trans('admin.cancel')}}</a>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
		@endpush
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
										
{!! Form::open(['url'=>aurl('/productdimensions/'.$productdimensions->id),'method'=>'put','id'=>'productdimensions','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
		<div class="form-group">
				{!! Form::label('product_id',trans('admin.product_id'),['class'=>'control-label']) !!}
{!! Form::select('product_id',App\Models\Product::pluck('product_name','id'), $productdimensions->product_id ,['class'=>'form-control select2','placeholder'=>trans('admin.product_id')]) !!}
		</div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('north',trans('admin.north'),['class'=>'control-label']) !!}
        {!! Form::text('north', $productdimensions->north ,['class'=>'form-control','placeholder'=>trans('admin.north')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('east',trans('admin.east'),['class'=>'control-label']) !!}
        {!! Form::text('east', $productdimensions->east ,['class'=>'form-control','placeholder'=>trans('admin.east')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('south',trans('admin.south'),['class'=>'control-label']) !!}
        {!! Form::text('south', $productdimensions->south ,['class'=>'form-control','placeholder'=>trans('admin.south')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('west',trans('admin.west'),['class'=>'control-label']) !!}
        {!! Form::text('west', $productdimensions->west ,['class'=>'form-control','placeholder'=>trans('admin.west')]) !!}
    </div>
</div>

</div>
		<!-- /.row -->
		</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="save" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save') }}</button>
<button type="submit" name="save_back" class="btn btn-success btn-flat"><i class="fa fa-save"></i> {{ trans('admin.save_back') }}</button>
{!! Form::close() !!}
</div>
</div>
@endsection