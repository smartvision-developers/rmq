@extends('admin.index')
@section('content')

@include("admin.layouts.components.submit_form_ajax",["form"=>"#productdimensions"])
<div class="card card-dark">
	<div class="card-header">
		<h3 class="card-title">
		<div class="">
			<span>
			{{ !empty($title)?$title:'' }}
			</span>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<span class="caret"></span>
			<span class="sr-only"></span>
			</a>
			<div class="dropdown-menu" role="menu">
				<a href="{{ aurl('productdimensions') }}"  style="color:#343a40"  class="dropdown-item">
				<i class="fas fa-list"></i> {{ trans('admin.show_all') }}</a>
			</div>
		</div>
		</h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
								
{!! Form::open(['url'=>aurl('/productdimensions'),'id'=>'productdimensions','files'=>true,'class'=>'form-horizontal form-row-seperated']) !!}
<div class="row">

<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
	<div class="form-group">
		{!! Form::label('product_id',trans('admin.product_id')) !!}
		{!! Form::select('product_id',App\Models\Product::pluck('product_name','id'),old('product_id'),['class'=>'form-control select2','placeholder'=>trans('admin.choose')]) !!}
	</div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('north',trans('admin.north'),['class'=>' control-label']) !!}
            {!! Form::text('north',old('north'),['class'=>'form-control','placeholder'=>trans('admin.north')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('east',trans('admin.east'),['class'=>' control-label']) !!}
            {!! Form::text('east',old('east'),['class'=>'form-control','placeholder'=>trans('admin.east')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('south',trans('admin.south'),['class'=>' control-label']) !!}
            {!! Form::text('south',old('south'),['class'=>'form-control','placeholder'=>trans('admin.south')]) !!}
    </div>
</div>
<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('west',trans('admin.west'),['class'=>' control-label']) !!}
            {!! Form::text('west',old('west'),['class'=>'form-control','placeholder'=>trans('admin.west')]) !!}
    </div>
</div>

</div>
		<!-- /.row -->
	</div>
	<!-- /.card-body -->
	<div class="card-footer"><button type="submit" name="add" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add') }}</button>
<button type="submit" name="add_back" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> {{ trans('admin.add_back') }}</button>
{!! Form::close() !!}	</div>
</div>
@endsection