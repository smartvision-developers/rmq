@extends('style/index')
@push('custom-css')
<style type="text/css">
    .country_code{
        position: absolute;
        top: 46px;
        left: 35px;
        color: #80808b;
    }
</style>
@endpush
@section('content')

        <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.login')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.login')</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap col-xs-12">
                <div class="container">
                    <div class="log-form col-xs-12">
                        <ul class="nav-tabs col-xs-12">
                            <li class="active">
                                <a href="#" data-toggle="tab" data-target="#t1">@lang('main.login')</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="tab" data-target="#t2">@lang('main.register')</a>
                            </li>
                        </ul>
                        <div class="tab-content col-xs-12">
                            <div class="tab-pane fade active in" id="t1">
                                <div class="alert alert-danger print-login-error-msg" style="display:none">
                                    <ul></ul>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.email')</h4>
                                        <input type="email" name="email" value="{{old('email')}}" id="email" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.password-no')</h4>
                                        <input type="password" name="password" class="form-control" id="pass">
                                        <button type="button" class="show-pass" toggle="#pass">
                                            <i class="la la-eye-slash"></i>
                                        </button>
                                    </div>
                                    <div class="form-group rems col-xs-12">
                                        <div>
                                            <label>
                                                <input name="remember_me" type="checkbox">
                                                <span>@lang('main.remember-me')</span>
                                            </label>
                                            <a href="{{url('/forget_password')}}" class="forget">@lang('main.forget-pass') ؟</a>
                                        </div>
                                    </div>
                                    <div class="form-group has-btn col-xs-12">
                                        <button type="submit" class="btn login_customer">@lang('main.login')</button>
                                        <p class="hint">
                                        @lang('main.dont-have-account') <a href="#" data-target="#t2" data-toggle="tab">@lang('main.register')</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="t2">
                                <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                                </div>
                                <form>
                                    {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> <i>*</i>@lang('main.first_name')</h4>
                                        <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> <i>*</i>@lang('main.last_name')</h4>
                                        <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4><i>*</i> @lang('main.email')</h4>
                                        <input type="email" id="reg_email" name="email" value="{{old('email')}}" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4><i>*</i>@lang('main.mobile')</h4>
                                        <input type="text" name="mobile" value="{{old('mobile')}}" class="form-control" id="mobile">
                                        <span class="country_code">966+</span>
                                    </div>
                                     <div class="form-group col-md-6 col-xs-12">
                                        <h4> <i>*</i>@lang('main.password-no')</h4>
                                        <input type="password" id="reg_password" name="password" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4><i>*</i> @lang('main.city')</h4>
                                        <select class="form-control nice-select" name="city_id">
                                            <option value=""> @lang('main.choose-city')</option>
                                            @foreach($citys as $key => $city)
                                                <option value="{{$city->id}}">
                                                    {{$city->city_name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4><i>*</i>@lang('main.region')</h4>
                                        <select class="form-control nice-select" 
                                        name="region_id">
                                        </select>
                                    </div>
                                    <div class="form-group has-btn col-xs-12">
                                        <button type="submit" class="btn btn-submit">@lang('main.register')</button>
                                        <p class="hint">
                                        @lang('main.have-account') <a href="#" data-target="#t1" data-toggle="tab">@lang('main.login')</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
@endsection
@push('custom-scripts')
<script type="text/javascript">
    
    $(document).ready(function() {
           $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{url('/')}}/registerUser/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- @lang('main.choose-region') ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="region_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });
        
        $(".btn-submit").click(function(e){
            e.preventDefault();
       
            var _token = $("input[name='_token']").val();
            var first_name = $("input[name='first_name']").val();
            var last_name = $("input[name='last_name']").val();
            var email = $("#reg_email").val();
            var mobile = $("input[name='mobile']").val();
            var password = $("#reg_password").val();
            var city_id = $("select[name='city_id']").val();
            var region_id = $("select[name='region_id']").val();
           
            $.ajax({
                url: "{{ route('storeClient') }}",
                type:'POST',
                data: {_token:_token, first_name:first_name, last_name:last_name, email:email, mobile:mobile, password:password, city_id:city_id, region_id:region_id,},
                success: function(data) {
                     if ((data.errors)) {
                    printErrorMsg(data.errors);
            }
                             if (data == 1) {
                    $(".print-error-msg").css('display','none');
                      toastr.success('   @lang('main.success-register')');

                     setTimeout(function() {
                 window.location.href = ('{{url()->previous()}}');

                        }, 2000); // 2 second

                }          
                },

                error: function (data) {
                  toastr.error("@lang('main.error')");                

                }
            });
       
        }); 
         $('.login_customer').click(function(event){
            event.preventDefault();
            var email = $('#email').val();
            var password = $('#pass').val();
            $.ajax({

                type: "post",
                url: '{{route('login')}}',
                data: {
              _token: '{{csrf_token()}}',email:email, password:password },
                dataType: "json",
                success: function(result){
                 if ((result.errors)) {
                    printLoginErrorMsg(result.errors);
                    }
                    if (result == 1) {
                            $(".print-error-msg").css('display','none');
                      toastr.success('@lang('main.logged-success')');
       setTimeout(function() {
                                  window.location.href = ('{{url()->previous()}}');
       }, 2000); // 2 second

                    }
                    
                     if (result == 2) {
                            $(".print-error-msg").css('display','none');
                      toastr.error("@lang('main.error-logged')");

                    }
                    
                },
                error: function (result) {
                    toastr.error("@lang('main.error')");
                }
            });
        });
        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }

          function printLoginErrorMsg (msg) {
            $(".print-login-error-msg").find("ul").html('');
            $(".print-login-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-login-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });
</script>
@endpush