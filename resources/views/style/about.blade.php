@extends('style/index')
@section('content')
 <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.about-us')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.about-us')</li>
                    </ul>
                </div>
            </div>
            <div class="ab-wrap col-xs-12">
                <div class="about-s col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="ab-img col-md-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                <div class="ab-slider owl-carousel">
                                   @foreach($about_files as $file)
                                    <div class="item">
                                        <img src="{{it()->url($file->full_path)}}" alt="">
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <div class="ab-data col-md-6 col-xs-12">
                                <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                    <h3>@lang('main.welcome-in') <b>{{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }}</b></h3>
                                </div>
                                <p data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">{!! setting()->about_us !!}
                                    </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vid vid-inner col-xs-12">
               <div class="inner" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <img src="{{it()->url($about_files[0]->full_path)}}" alt="">
                     <a href="{{it()->url(setting()->video) }}" data-fancybox>
                         <i class="la la-play"></i>
                     </a>
                        </div>
                 </div>
                 <div class="why-us col-xs-12">
                     <div class="container">
                         <div class="g-head col-xs-12">
                             <h3>@lang('main.why-us')</h3>
                         </div>
                         <div class="g-body col-xs-12">
                             <div class="row">
                                @foreach($about as $key => $value)
                                 <div class="block col-md-3 col-sm-6 col-xs-12">
                                     <div class="inner">
                                         <div class="i-img">
                                             <img src="{{it()->url($value->image)}}" alt="{{$value->title}}">
                                         </div>
                                         <h3>{{$value->title}}</h3>
                                         <p>{!! $value->content !!}</p>
                                     </div>
                                 </div>
                                 @endforeach
                             </div>
                         </div>
                     </div>
                 </div>
            </div>
@endsection