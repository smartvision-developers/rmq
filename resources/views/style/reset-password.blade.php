<!DOCTYPE html>
<html>
<head>
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

  <main class="main-content col-xs-12">
            <div class="user-log-wrap col-xs-12">
             
                <div class="user-inner-wrap forget_wrap col-xs-12">
                    <div class="container">
                    <div class="row">
                        <div class="log-data col-md-12 col-xs-12">
                             <h2>Reset Password</h2>
                             <br>
                             <hr>
                        </div>
                        <div class="log-form col-md-6 col-xs-12">
                            <div class="log-form-card col-xs-12">
                            <h3>Hello,</h3>
                            <p>You are receiving this email because we received a reset password request for your account. with code {{$code}}</p>
                            <a href="{{$link}}" class=" btn btn-info">
                                Click Here </a><br>
                                <h4>Regards,</h4>
                                <h4>Sound Nature Team</h4>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
</body>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
</html>
