@extends('style/index')
@section('content')
       
        <main class="main-content col-xs-12">
            <div class="hero-sec col-xs-12">
                <div class="slider">
                    <div class="one owl-carousel">
                        @foreach($sliders as $key=> $slider)
                        <div class="item-box">
                            <img src="{{it()->url($slider->slider_image)}}" alt="">
                            <div class="i-cap">
                                <h3>{{$slider->slider_title}}</h3>
                                <p>{!! $slider->slider_content !!}</p>
                                <a href="{{url('/products')}}" class="btn btn-border">@lang('main.buy-now')</a>
                            </div>
                        </div>
                        @endforeach
                    
                    </div>
                </div>
                <div class="slider-two">
                    <div class="two owl-carousel">
                        @foreach($sliders as $key=> $slider)
                        <div class="item">
                            <img src="{{it()->url($slider->slider_image)}}" alt="">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="about-s col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="ab-img col-md-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <div class="ab-slider owl-carousel">
                                 @foreach($about_files as $file)
                                    <div class="item">
                                        <img src="{{it()->url($file->full_path)}}" alt="">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="ab-data col-md-6 col-xs-12">
                            <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                <h3>@lang('main.welcome-in') <b>{{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }}</b></h3>
                            </div>
                            <p data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">{!! setting()->about_us !!}
                                </p>
                                <a href="{{url('/about-us')}}" class="btn" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="600">@lang('main.knowing-us')</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blocks col-xs-12">
                <div class="container">
                    <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                        <h3>@lang('main.choose-product')</h3>
                    </div>
                    <div class="g-body col-xs-12">
                        <div class="row">
                            @include('style.partial.single_product')
                        </div>
                        <div class="g-more col-xs-12">
                            <a href="{{url('/products')}}" class="btn">@lang('main.more')</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vid col-xs-12">
                <div class="container">
           <div class="inner" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <img src="{{it()->url($about_files[0]->full_path)}}" alt="">
                <a href="{{it()->url(setting()->video) }}" data-fancybox>
                    <i class="la la-play"></i>
                </a>
                    </div>
     </div>
             </div>
            <div class="most col-xs-12">
                <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <div class="container">
                        <h3>@lang('main.most-popular')</h3>
                    </div>
                </div>
                <div class="g-body col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <div class="most-slider owl-carousel">
                        @foreach($most_popular as $key=> $value)
                        <div class="item">
                            @include('style.partial.col_product')
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
            <div class="counters col-xs-12">
                <div class="container">
                    <div class="co-inner" style="background-image: url({{url('style')}}/images/hero.jpg);">
                        @foreach($statistics as $val)
                        <div class="block col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                            <div class="inner">
                                <span class="counter">{{$val->statistic_number}}</span>
                                <h4>{{$val->statistic_title}}</h4>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
            <div class="contact col-xs-12">
                <div class="g-head col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                    <div class="container">
                        <h3>@lang('main.contact-us')</h3>
                    </div>
                </div>
                <div class="g-body col-xs-12" style="background-image: 
                url({{url('style')}}/images/hero2.jpg);">
                    <div class="container">
                       
                        <form>
                                {{ csrf_field() }}
                                <div class="inner col-xs-12">
                                    <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                            </div>
                                    <div class="form-group col-md-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                        <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="@lang('main.enter-name')">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="400">
                                        <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="@lang('main.email')">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
                                        <input type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="@lang('main.title-msg')">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">
                                        <textarea class="form-control" name="message"  placeholder="@lang('main.enter-content')">{{old('message')}}</textarea>
                                    </div>
                                    <div class="form-group col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1200">
                                        <button type="submit" class="btn btn-submit">@lang('main.send')</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
           
@endsection 
@push('custom-scripts')
<script type="text/javascript">  
    $(document).ready(function() {
          $(".btn-submit").click(function(e){
            e.preventDefault();
       
            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var title = $("input[name='title']").val();
            var message = $("textarea[name='message']").val();
            $.ajax({
                url: "{{ route('storeContact') }}",
                type:'POST',
                data: {_token:_token, name:name, email:email, title:title, message:message},
                success: function(data) {
                    if ((data.errors)) {
                        printErrorMsg(data.errors);
                    }
                    if (data == 1) {
                        $("input[name='name']").val('');
                        $("input[name='email']").val('');
                        $("input[name='title']").val('');
                        $("textarea[name='message']").val('');
                        $(".print-error-msg").css('display','none');
                          toastr.success('   @lang('main.message-sent')');

                     //     setTimeout(function() {
                     // window.location.href = ('{{url('/')}}');

                     //        }, 2000); // 2 second
                    }          
                },
                error: function (data) {
                  toastr.error("@lang('main.error')");                

                }
            });
       
        }); 
        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });
</script>
@endpush