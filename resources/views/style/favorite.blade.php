@extends('style/index')
@section('content')
  <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.my-account')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.favorite')</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap profile-wrap col-xs-12">
                <div class="container">
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <ul>
                             <li>
                                <a href="{{url('profile')}}">@lang('main.profile')</a>
                            </li>
                            <li class="active">
                                <a href="{{url('favorite')}}">@lang('main.favorite')</a>
                            </li>
                            <li>
                                <a href="{{url('change-password')}}">@lang('main.password-no')</a>
                            </li>
                            <li>
                                <a href="{{url('userLogout')}}">@lang('main.logout')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="prof-content col-md-9 col-xs-12">
                        <div class="p-head col-xs-12">
                            <h4>@lang('main.favorite')</h4>
                        </div>
                        <div class="p-blocks col-xs-12">
                            <div class="row">
                                @if($favs->isEmpty())
                                <h4>@lang('main.nothig-to-show')</h4>
                                @endif
                                @foreach($favs as $key=> $value)
                                <div class="block col-md-4 col-sm-6 col-xs-12">
                                 @include('style.partial.col_product')
                                </div>                              
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection