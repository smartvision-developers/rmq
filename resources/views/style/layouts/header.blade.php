<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <title>{{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }} </title>
    <meta property="og:title" content="{{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }} " />
    <meta property="og:type" content="eCommerce" />
    <meta property="og:url" content="{{url('/')}}" />
    <meta property="og:image" content="{{it()->url(setting()->logo)}}" />
    <meta property="og:determiner" content="the" />
    <meta property="og:locale" content="ar_AR" />
    <meta property="og:locale:alternate" content="en_GB" />
    <meta property="og:site_name" content="{{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }}" />

    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta charset="utf-8" />
    <meta name="author" content="Amir Nageh" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000000">

    <!-- Css Files -->
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link href="{{url('/style')}}/css/style.css" rel="stylesheet" />
    <link href="{{url('/style')}}/css/bootstrap-rtl.min.css" rel="stylesheet" />
    <link href="{{url('/style')}}/css/style-res.css" rel="stylesheet" />
       <!-- <link href="{{url('/style')}}/css/style-en.css" rel="stylesheet"> -->
    <!-- lavicons -->
    @stack('custom-css')
     {!! Html::style('/toastr/toastr-rtl.min.css') !!}

    <link rel="shortcut icon" href="{{it()->url(setting()->icon)}}" />
    <style type="text/css">
        
 .view-count{
        position: absolute;
    left: 20px;
    z-index: 9999;
    font-size: 18px;
    color: white;
    }
    .no-views{
            color: white;
    padding: 8px;
    line-height: normal;
    font-size: 12px;
    }
.sharethis-inline-share-buttons .st-btn{
    width: 40px;
    height: 40px !important;
    text-align: center;
    line-height: 40px;
    background-color: rgb(0 0 0 / 50%) !important;
    border-radius: 100% !important;
    border: none !important;
    color: #fff;
    margin-bottom: 20px;
    transition: all .3s;
    font-size: 17px;
    }
    #st-2 .st-btn > img {
        top: 12px !important;
    }
    </style>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=61efaed5aa3f60001954c40c&product=sop' async='async'></script>
</head>

<body>
    <div id="loading">
        <div class="loading"></div>
    </div>

    <!-- Dark theme add class="dark" to wrapper -->
    <div class="wrapper col-xs-12">
       