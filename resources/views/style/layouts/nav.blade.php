 <header class="main-head col-xs-12">
            <div class="container">
                <div class="logo">
                    <a href="{{url('/')}}">
                        <img src="{{it()->url(setting()->logo)}}" alt="">
                    </a>
                </div>
                <div class="nav-manu">
                    <button type="button" class="cl-menu">
                        <i class="la la-close"></i>
                    </button>
                    <ul>
                         <li class="{{ (Request::path() == '/') ? 'active' : '' }}">
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>

                        @foreach($website_links as $key=> $value)
                        @if($value->link_position == 'header')
                        <li class="menu-item-has-children {{ (Request::url() === $value->link_url) ? 'active' : '' }}">
                            <a href="{{url($value->link_url)}}" target="_{{$value->link_target}}">
                                {{$value->link_name}}
                            </a>
                            @if($value->child->isNotEmpty())
                            <ul class="sub-menu">
                                
                                @foreach($value->child->take(20) as $val)
                                <li class="list-group-item">
                                    <a href="{{url($val->link_url)}}" target="{{$val->link_target}}">
                                            {{$val->link_name}}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endif
                        @endforeach 

                    </ul>
                </div>
                <div class="h-extra">
                    <a href="#" data-toggle="modal" data-target="#search_pop">
                        <i class="la la-search"></i>
                    </a>
                    <ul class="user-area">
                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">
                                <i class="la la-user"></i>
                            </a>
                            <ul class="sub-menu">
                                @auth
                                <li>
                                    <a href="{{url('/profile')}}">@lang('main.profile')</a>
                                </li>
                                <li>
                                    <a href="{{url('/userLogout')}}">@lang('main.logout')</a>
                                </li>
                                @endauth
                                 @guest
                                <li>
                                    <a href="{{url('/login')}}">@lang('main.login') </a>
                                </li>
                                
                                @endguest
                            </ul>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="op-menu">
                        <i class="la la-bars"></i>
                    </a>
                </div>
            </div>
        </header>
        <div class="overlay-s"></div>