   <div class="partners col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                <div class="container">
                    <div class="part-slider owl-carousel">
                        @foreach($trademarks as $value)
                        <div class="item">
                            <img src="{{it()->url($value->image)}}" alt="{{$value->name}}">
                        </div>
                        @endforeach
                        
                    </div>
                </div>
            </div>
        </main>
  <footer class="main-footer col-xs-12" style="background-image: url({{url('style')}}/images/hero2.jpg);">
            <div class="container">
                <div class="f-top col-xs-12">
                    <div class="row">
                        <div class="f-item col-md-4 col-xs-12">
                            <h4>@lang('main.about-us')</h4>
                            <p>{!! setting()->about_us !!}</p>
                        </div>
                        <div class="f-item col-md-2 col-xs-12">
                            <h4>@lang('main.important-link')</h4>
                            <ul class="sitemap">
                                @foreach($website_links as $key=> $value)
                                <li>
                                    @if($value->link_position == 'footer')
                                    <a href="{{url($value->link_url)}}" target="_{{$value->link_target}}">
                                        {{$value->link_name}}
                                    </a>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="f-item col-md-3 col-xs-12">
                            <h4>@lang('main.contact-us')</h4>
                            <ul class="siteinfo">
                                <li>
                                    <i class="la la-crosshairs"></i>
                                    @lang('main.address')
                                    <p>
                                    <a target="_blank">‏{{setting()->address}}</a>
                                    </p>
                                </li>
                                <li>
                                    <i class="la la-envelope"></i>
                                    @lang('main.email')
                                    <p>
                                        <a href="mailto:{{setting()->email}}">{{setting()->email}}</a>
                                    </p>
                                </li>
                                <li>
                                    <i class="las la-crosshairs"></i>
                                    @lang('main.mobile')
                                    <p>
                                        <a href="tel:{{setting()->mobile}}">+{{setting()->mobile}}</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="f-item col-md-3 col-xs-12">
                            <h4>@lang('main.social-link')</h4>
                            <div class="soclia">
                                 @foreach($social_links as $social_link)
                                <a href="{{url($social_link->social_url)}}" class="{{$social_link->social_name}}">
                                    <i class="{{$social_link->social_icon}}"></i>
                                </a> 
                                @endforeach
                            </div>
                            <ul class="siteinfo">
                                <li>
                                    <i class="la la-file-invoice"></i>
                                    @lang('main.commercial-no')
                                    <p>{{setting()->commercial_registration_no}}</p>
                                </li>
                                <li>
                                    <i class="la la-file-invoice"></i>
                                    @lang('main.tax_no')
                                    <p>{{setting()->tax_no}}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="f-bottom col-xs-12">
                    <p>@lang('main.all-rights'){{ !empty($title) ? $title.' - '. setting()->{l('sitename')} :setting()->{l('sitename')} }} </p>
                    <a href="https://smartvision4p.com" target="_blank">
                        <img src="{{url('style')}}/images/dev.svg" alt="">
                    </a>
                </div>
            </div>
            <div class="toTop">
                <i class="la la-angle-up"></i>
            </div>
            <div class="float-icons">
              
                <a href="https://api.whatsapp.com/send?phone={{setting()->mobile}}" target="_blank" class="whats" data-tool="tooltip" title="@lang('main.whats-call')" data-placement="right">
                    <i class="la la-whatsapp"></i>
                </a>
            </div>       
        </footer>
        <!-- /.modal -->
    </div>

    <div class="modal fade" id="search_pop">
        <div class="modal-dialog">
          <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="la la-close"></i>
            </button>
            <div class="modal-body">
                <div class="search-wrap">
                     <form action="{{url('/search-result/')}}" method="get">
                            <div class="form-group">
                                <input type="search" required="" name="content" class="form-control" placeholder="@lang('main.write-search')">
                                <button type="submit" class="btn"> @lang('main.search')
                                </button>
                            </div>
                        </form>
                </div>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

    <div class="modal fade" id="confirm_pop">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
                <div class="confo-p">
                    <h4>اتفاقية الشراء</h4>
                    <p>{!! setting()->policy !!}</p>
                        <button type="button" data-dismiss="modal">اغلاق</button>
                </div>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    <!-- Javascript Files -->
    <script src="{{url('/style')}}/js/jquery-2.2.2.min.js"></script>
    <script src="{{url('/style')}}/js/bootstrap.min.js"></script>
    <script src="{{url('/style')}}/js/jquery.fancybox.min.js"></script>
    <script src="{{url('/style')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('/style')}}/js/aos.js"></script>
    <script src="{{url('/style')}}/js/jquery.nice-select.min.js"></script>
    <script src="{{url('/style')}}/js/visa-format.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="{{url('/style')}}/js/jquery.counterup.min.js"></script>
      @stack('custom-scripts')
    <script src="{{url('/style')}}/js/script.js"></script>
    {!! Html::script('/toastr/toastr.min.js') !!}

    @if(Session::has('flash_message'))
    <script type="text/javascript">
      Command: toastr["success"]("{{ Session::get('flash_message') }}")
    </script>
    @elseif(Session::has('error_flash_message'))
    <script type="text/javascript">
      Command: toastr["error"]("{{ Session::get('error_flash_message') }}")
    </script>
    @elseif(Session::has('info_flash_message'))
    <script type="text/javascript">
      Command: toastr["info"]("{{ Session::get('info_flash_message') }}")
    </script>
    @endif
<script type="text/javascript">
 function addProductToFav(e, productID) {
           
            var x1 = productID;
            $.ajax({

                url: '{{route('site.add_fav')}}',
                type: 'post',
                dataType: 'json',
                data:{ product_id : x1} ,
                success: function (da) {
                    if (da == 1) {
                       toastr.success('@lang('main.add-fav')');
                $('.fav_'+ x1).html( '<i class="la la-heart" style="color:red;"></i>');
                    } else {
                      toastr.success('@lang('main.del-fav')');

                    $('.fav_'+ x1).html( '<i class="la la-heart" ></i>');

                    }
                },
                error: function (da) {
                      toastr.error('@lang('main.login-first')');
                }
            });

        }
</script>
</body>

</html>