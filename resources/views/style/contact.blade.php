@extends('style/index')
@section('content')
       <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.contact-us')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.contact-us')</li>
                    </ul>
                </div>
            </div>
            <div class="contact-wrap col-xs-12">
                <div class="container">
                    <div class="conto-form col-md-8 col-xs-12">
                        <div class="cop-head col-xs-12">
                            <h4>@lang('main.enter-msg')</h4>
                           <!--  <p>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.
                            </p> -->
                        </div>
                        <div class="cop-body col-xs-12">
                             <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                            </div>
                            <form>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="@lang('main.enter-name')">
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="@lang('main.email')">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <input type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="@lang('main.title-msg')">
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <textarea class="form-control" name="message"  placeholder="@lang('main.enter-content')">{{old('message')}}</textarea>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <button type="submit" class="btn btn-submit">@lang('main.send')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="conto-data col-md-4 col-xs-12">
                        <div class="cop-head col-xs-12">
                            <h4>@lang('main.location')</h4>
                        </div>
                        <ul>
                            <li>
                                <i class="la la-map-marker"></i>
                                <a target="_blank">
                                    {{setting()->address}}
                                </a>
                            </li>
                            <li>
                                <i class="la la-envelope"></i>
                                <a href="mailto:{{setting()->email}}">{{setting()->email}}</a>
                            </li>
                            <li>
                                <i class="la la-phone"></i>
                                <a href="tel:+{{setting()->mobile}}">+{{setting()->mobile}}</a>
                            </li>
                        </ul>
                        <div class="cop-head col-xs-12">
                            <h4>@lang('main.map')</h4>
                        </div>
                        <div class="cop-map">
                            <input type="hidden" name="lat" id="lat" value="{{setting()->lat}}">
                            <input type="hidden" name="lng" id="lng" value="{{setting()->lng}}">
                           
                            <div  id="latlongmap" style="height:250px;"></div>

                           <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3418.699866967512!2d31.366383685338207!3d31.034610678072703!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f79d99ef7ea253%3A0xe92319f27469b714!2z2LPZhdin2LHYqiDZgdmK2KzZhiDZhNiq2LXZhdmK2YUg2YjYqNix2YXYrNipINin2YTZhdmI2KfZgti5INmI2KfZhNiq2LfYqNmK2YLYp9iqINin2YTYp9mE2YPYqtix2YjZhtmK2Kk!5e0!3m2!1sar!2seg!4v1629677229468!5m2!1sar!2seg"></iframe> -->
                        </div>
                    </div>
                </div>
            </div>
@endsection
@push('custom-scripts')
<script type="text/javascript">  
    $(document).ready(function() {
          $(".btn-submit").click(function(e){
            e.preventDefault();
       
            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var title = $("input[name='title']").val();
            var message = $("textarea[name='message']").val();
            $.ajax({
                url: "{{ route('storeContact') }}",
                type:'POST',
                data: {_token:_token, name:name, email:email, title:title, message:message},
                success: function(data) {
                    if ((data.errors)) {
                        printErrorMsg(data.errors);
                    }
                    if (data == 1) {
                         $("input[name='name']").val('');
                        $("input[name='email']").val('');
                        $("input[name='title']").val('');
                        $("textarea[name='message']").val('');
                        $(".print-error-msg").css('display','none');
                          toastr.success('   @lang('main.message-sent')');

                     //     setTimeout(function() {
                     // window.location.href = ('{{url('/')}}');

                     //        }, 2000); // 2 second
                    }          
                },
                error: function (data) {
                  toastr.error("@lang('main.error')");                

                }
            });
       
        }); 
        function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
    });

</script>
<script type="text/javascript">
     function initialize() {
    var e = new google.maps.LatLng({{setting()->lat}}, {{setting()->lng}}), t = {
        zoom: 8,
        center: e,
        panControl: !0,
        scrollwheel: !1,
        scaleControl: !0,
        overviewMapControl: !0,
        overviewMapControlOptions: {opened: !0},
        mapTypeId: google.maps.MapTypeId.terrain
    };
    map = new google.maps.Map(document.getElementById("latlongmap"), t), geocoder = new google.maps.Geocoder, marker = new google.maps.Marker({
        position: e,
        map: map
    }), map.streetViewControl = !1, infowindow =
    new google.maps.InfoWindow({content: "({{setting()->lat}}, {{setting()->lng}}))"}), google.maps.event.addListener(map, "click", function (e) {
        marker.setPosition(e.latLng);
        var t = e.latLng, o = "(" + t.lat().toFixed(6) + ", " + t.lng().toFixed(6) + ")";
        infowindow.setContent(o),
        document.getElementById("lat").value = t.lat().toFixed(6),
        document.getElementById("lng").value = t.lng().toFixed(6)
    })
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_wPc4ukHgXijCSXQ35li3Bkipx-XfM1E&callback=initialize"></script>
@endpush