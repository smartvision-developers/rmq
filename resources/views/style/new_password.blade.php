@extends('style/index')
@section('content')

      <main class="main-content col-xs-12">
        <div class="breads col-xs-12">
           <img src="{{url('/front')}}/images/hero.jpg" alt="">
            <div class="container">
                <h3>@lang('translate.update-pass')</h3>
                <ul>
                    <li>
                        <a href="{{asset('/')}}">@lang('translate.home')</a>
                    </li>
                    <li>@lang('translate.update-pass')</li>
                </ul>
            </div>
        </div>
    @if($data->forget_password == 0)
        <div class="log-wrap col-xs-12">
          <div class="container">
              <div class="log-form col-xs-12">
                  <div class="form-head col-xs-12">
                      <h4>كود التحقق</h4>
                  </div>
                <form action="{{route('site.checking')}}" method="post">
                        @csrf
                    <input type="hidden" name="email" type="email" value="{{$data->email}}">
                  <div class="form-body col-xs-12">
                      <div class="form-group col-md-6 col-xs-12">
                          <h4>كود التحقق</h4>
                        <input type="text" name="code[]" class="form-control">
                      </div>
                          <div class="form-group has-btn col-xs-12">
                              <button type="submit" class="btn">حفظ</button>
                          </div>
                  </div>
                </form>
              </div>
          </div>
      </div>
      @endif



    @if($data->forget_password == 1)
        <div class="log-wrap col-xs-12">
          <div class="container">
              <div class="log-form col-xs-12">
                  <div class="form-head col-xs-12">
                      <h4>@lang('translate.update-pass')</h4>
                  </div>
                <form action="{{route('site.checking')}}" method="post">
                        @csrf
                        
                    <input type="hidden" name="email" type="email" value="{{$data->email}}">
                  <div class="form-body col-xs-12">
                      <div class="form-group col-md-6 col-xs-12">
                          <h4>@lang('translate.new-pass')</h4>
                          <input type="password" name="new_password" required class="form-control" id="pass_1">
                          <button type="button" class="show-pass" toggle="#pass_1">
                              <i class="la la-eye-slash"></i>
                          </button>
                      </div>
                      <div class="form-group col-md-6 col-xs-12">
                          <h4>@lang('translate.pass-again')</h4>
                          <input type="password" name="confirm_password" required class="form-control" id="pass_2">
                          <button type="button" class="show-pass" toggle="#pass_2">
                              <i class="la la-eye-slash"></i>
                          </button>
                      </div>
                          <div class="form-group has-btn col-xs-12">
                              <button type="submit" class="btn">@lang('translate.save')</button>
                          </div>
                  </div>
                </form>
              </div>
          </div>
      </div>
      @endif
      </main>

 @endsection