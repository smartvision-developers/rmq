                                    <div class="m-block">
                                        <img src="{{it()->url($value->product_id()->first()->product_image)}}" alt="{{$value->product_id()->first()->product_name}}">
                                        <div class="icap-o">
                                            <a href="{{url('product/'.$value->product_id)}}" class="title">{{$value->product_id()->first()->product_name}}</a>
                                            <div class="overl">
                                                <ul>
                                                    <li>
                                                        <img src="{{url('style')}}/images/ic1.png" alt="">
                                                        @lang('main.floor-no')
                                                        <span>{{$value->product_id()->first()->number_floor}}</span>
                                                    </li>
                                                    <li>
                                                        <img src="{{url('style')}}/images/ic2.png" alt="">
                                                        @lang('main.room-no') 
                                                        <span>{{$value->product_id()->first()->number_room}}</span>
                                                    </li>
                                                    <li>
                                                        <img src="{{url('style')}}/images/ic3.png" alt="">
                                                        @lang('main.land-area')
                                                        <span>{{$value->product_id()->first()->land_area}} @lang('main.meter-square')</span>
                                                    </li>
                                                </ul>
                                                <p>@lang('main.design-by'): {{$value->design_by}}</p>
                                            </div>
                                            <h4>&rlm;{{$value->product_id()->first()->price}} @lang('main.sar')</h4>
                                            <a href="{{url('product/'.$value->product_id)}}" class="btn btn-border">@lang('main.view')</a>
                                            <div class="i-actions">
                                @auth
                                                  @php
                $fav=\App\Models\ProductFavourite::where('product_id', $value->product_id()->first()->id)->where('user_id',auth()->user()->id)->first();
                                    @endphp
                                    @if(!$fav)
                                            <a class="fav fav_{{$value->product_id()->first()->id}}" onclick="addProductToFav(this,{{$value->product_id()->first()->id}})" >
                                                <i class="la la-heart"></i>
                                            </a>
                                            @else
                                            <a class="fav fav_{{$value->product_id()->first()->id}}" onclick="addProductToFav(this,{{$value->product_id()->first()->id}})" >
                                                <i class="la la-heart" style="color:red;"></i>
                                            </a>
                                            @endif
                                     @endauth
                                     @guest
                                      <a class="fav fav_{{$value->product_id()->first()->id}}" onclick="addProductToFav(this,{{$value->product_id()->first()->id}})" >
                                                <i class="la la-heart"></i>
                                            </a>
                                    @endguest
                                                <!-- <a href="#">
                                                    <i class="la la-share"></i>
                                                </a> -->
                                            </div>
                                        </div>
                                    </div>

