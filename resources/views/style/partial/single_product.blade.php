  @foreach($products as $key=> $value)
                            <div class="block col-md-4 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                                <div class="inner">
                                    <div class="i-img">
                                        <img src="{{it()->url($value->product_image)}}" alt="">
                                        <span>‏{{$value->price}} @lang('main.sar')</span>
                                        <div class="i-actions">
                                            <!-- ShareThis BEGIN --><div class="sharethis-inline-share-buttons"></div><!-- ShareThis END -->
                                           <!--  <a href="https://twitter.com/intent/tweet?text=Share+title&url=http://jorenvanhocht.be" target="_blank">
                                                <i class="la la-share"></i>
                                            </a> -->
                            @auth
                                             @php
                $fav=\App\Models\ProductFavourite::where('product_id', $value->id)->where('user_id',auth()->user()->id)->first();
                                    @endphp
                                    @if(!$fav)
                                            <a class="fav fav_{{$value->id}}" onclick="addProductToFav(this,{{$value->id}})">
                                                <i class="la la-heart"></i>
                                            </a>
                                            @else
                                            <a class="fav fav_{{$value->id}}" onclick="addProductToFav(this,{{$value->id}})">
                                                <i class="la la-heart" style="color:red;"></i>
                                            </a>
                                            @endif
                            @endauth
                            @guest
                            <a class="fav fav_{{$value->id}}" onclick="addProductToFav(this,{{$value->id}})">
                                                <i class="la la-heart"></i>
                                            </a>
                            @endguest
                                        </div>
                                    </div>
                                    <div class="i-data">
                                        <a href="{{url('/product/'.$value->id)}}" class="title">{{$value->product_name}}</a>
                                        <div class="i-extra">
                                            <ul>
                                                <li>
                                                    <img src="{{url('style')}}/images/ic1.png" alt="">
                                                    <p> @lang('main.floor-no')</p>
                                                    <span>{{$value->number_floor}}</span>
                                                </li>
                                                <li>
                                                    <img src="{{url('style')}}/images/ic2.png" alt="">
                                                    <p> @lang('main.room-no')</p>
                                                    <span>{{$value->number_room}}</span>
                                                </li>
                                                <li>
                                                    <img src="{{url('style')}}/images/ic3.png" alt="">
                                                    <p> @lang('main.land-area')</p>
                                                    <span>{{$value->land_area}}  @lang('main.meter-square')</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <p class="desc">@lang('main.design-by'): {{$value->design_by}}</p>
                                        <a href="{{url('/product/'.$value->id)}}" class="btn">@lang('main.view-product')
                                        <span class="view-count"><i class="las la-eye"></i><span class="no-views">{{$value->view_count}}</span></span></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
    