@extends('style/index')
@section('content')
  <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.products')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.products')</li>
                    </ul>
                </div>
            </div>
            <div class="blocks blocks_inner col-xs-12">
                <div class="container">
                    <div class="g-body col-xs-12">
                        <div class="row grid__">
                            @include('style.partial.single_product')
                        </div>
                        <div class="g-more col-xs-12">
                            <input type="hidden" class="lastPage" name="lastPage" value="{{$products->lastPage()}}">
                            <button class="see-more btn" data-page="2" data-link="{{asset('/products')}}?page=" data-div=".grid__">@lang('main.more')</button> 
                        </div>
                    </div>
                </div>
            </div>
@endsection
@push('custom-scripts')
<script type="text/javascript">
$page = $(".see-more").data('page');
if($('.lastPage').val() == $page-1){
    $(".see-more").hide();
}
$(".see-more").click(function() {
  $div = $($(this).data('div')); //div to append
  
  $link = $(this).data('link'); //current URL

  $page = $(this).data('page'); //get the next page #
  $href = $link + $page; //complete URL
  $.get($href, function(response) { //append data
    $html = $(response).find(".grid__").html(); 
 //   console.log($page);
    $div.append($html);
});

  $(this).data('page', (parseInt($page) + 1)); //update page #
if($('.lastPage').val() == $page){
    $(".see-more").hide();
}



});

</script>

@endpush