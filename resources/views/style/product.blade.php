@extends('style/index')
@section('content')
   <main class="main-content col-xs-12">
            <div class="breads hero-breads col-xs-12">
                <img src="{{it()->url($product->product_image)}}" alt="">
                <div class="container">
                    <h3>{{$product->product_name}}</h3>
                    <p>{!! $product->product_description !!}</p> 
                    <div class="row">
                        <div class="block col-md-3 col-sm-6 col-xs-6">
                            <div class="inner">
                                <div class="i-img">
                                    <img src="{{url('style')}}/images/ic1.png" alt="">
                                </div>
                                <h4> @lang('main.floor-no')</h4>
                                <span>{{$product->number_floor}}</span>
                            </div>
                        </div>
                        <div class="block col-md-3 col-sm-6 col-xs-6">
                            <div class="inner">
                                <div class="i-img">
                                    <img src="{{url('style')}}/images/ic2.png" alt="">
                                </div>
                                <h4> @lang('main.room-no')</h4>
                                <span>{{$product->number_room}}</span>
                            </div>
                        </div>
                        <div class="block col-md-3 col-sm-6 col-xs-6">
                            <div class="inner">
                                <div class="i-img">
                                    <img src="{{url('style')}}/images/ic3.png" alt="">
                                </div>
                                <h4> @lang('main.land-area')</h4>
                                <span>{{$product->land_area}} @lang('main.meter-square')</span>
                            </div>
                        </div>
                        <div class="block col-md-3 col-sm-6 col-xs-6">
                            <div class="inner">
                                <div class="i-img">
                                    <img src="{{url('style')}}/images/ic4.png" alt="">
                                </div>
                                <h4> @lang('main.design-by')</h4>
                                <span>{{$product->design_by}}</span>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="single-wrap col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="single-box col-md-8 col-xs-12">
                           @foreach($product_files as $file)
                            <div class="post-img col-xs-12">
                                <img src="{{it()->url($file->full_path)}}" alt="{{$product->product_name}}">
                            </div>
                           @endforeach
                            <div class="post-data col-xs-12">
                              {!! $product->product_content !!}
                            </div>
                        </div>
                        <div class="single-sidebar col-md-4 col-xs-12">
                            @if($product->chart_information)
                            <div class="s-widget col-xs-12">
                                <h4>@lang('main.design-info')</h4>
                                <div class="s-inner">
                                    <ul>
                                        {!!  $product->chart_information !!}
                                    </ul>
                                </div>
                            </div>
                            @endif
                            @if(! $product->product_book->isEmpty())
                            <div class="s-widget col-xs-12">
                                <h4>@lang('main.design-detail')</h4>
                                <div class="s-inner">
                                    <ul>
                                        @foreach($product->product_book as $value)
                                        <li>
                                            <i class="la la-check"></i>
                                            {{$value->product_detail}}
                                        </li>
                                        @endforeach  
                                    </ul>
                                </div>
                            </div>
                            @endif
                            @if( $product->design_book)
                            <div class="s-widget col-xs-12">
                                <h4>@lang('main.design-detail')</h4>
                                <div class="s-inner">
                                    <div class="s-btns">
                                        <a href="{{it()->url($product->design_book)}}" data-tool="tooltip" title="@lang('main.view-book')">
                                            <i class="la la-eye"></i>
                                        </a>
                                        <a href="{{it()->url($product->design_book)}}" download="" data-tool="tooltip" title="@lang('main.download-book')">
                                            <i class="la la-download"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if( $product->notes)
                            <div class="s-widget col-xs-12">
                                <h4>@lang('main.design-notes')</h4>
                                <div class="s-inner">
                                    <ul>
                                     {!! $product->notes !!}
                                    </ul>
                                </div>
                                    
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if($product->product_video != null)
            <div class="vid vid-inner col-xs-12">
                <div class="inner" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
                     <img src="{{it()->url($product->product_image)}}" alt="{{$product->product_name}}">
                      <a href="{{it()->url($product->product_video)}}" data-fancybox>
                          <i class="la la-play"></i>
                      </a>
                         </div>
            </div>
            @endif
            @if($product->product_dimension->isNotEmpty())
            <div class="single-dimension col-xs-12">
                <div class="container">
                    <div class="row">
                        <div class="dim-img col-md-3 col-xs-12">
                            <div class="i-horz">
                                <span>@lang('main.north')</span>
                                <span>@lang('main.east')</span>
                                <span>@lang('main.south')</span>
                                <span>@lang('main.west')</span>
                            </div>
                            <div class="i-area">
                                <span>{{$product->land_area}} @lang('main.meter-square')</span>
                            </div>
                        </div>
                        <div class="dim-body col-md-9 col-xs-12">
                            <div class="dim-head col-xs-12">
                                <h3>@lang('main.land-meter') </h3>
                            </div>
                            <div class="dim-form col-xs-12">
                                <div class="row">
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <h5>@lang('main.west')</h5>
                                        <select name="west" class="form-control nice-select">
                                            <option>@lang('main.choose')</option>
                                        </select>
                                    </div>   
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <h5>@lang('main.south')</h5>
                                        <select name="south" class="form-control nice-select">
                                            <option>@lang('main.choose')</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <h5>@lang('main.east')</h5>
                                        <select class="form-control nice-select" name="east">
                                            <option>@lang('main.choose')</option>
                                            @foreach($product->product_dimension()->groupBy('east')->limit(5)->get() as $value)
                                                <option value="{{$value->east}}">{{$value->east}} م</option>
                                            @endforeach
                                        </select>
                                    </div>
                                
                                     <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                        <h5>@lang('main.north')</h5>
                                        <select class="form-control nice-select" name="north">
                                            <option>@lang('main.choose')</option>
                                            @foreach($product->product_dimension()->groupBy('north')->limit(5)->get() as $value)
                                                <option value="{{$value->north}}">{{$value->north}} م</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dim-btn col-xs-12">
                            <a class="btn" onclick="add_product_cart(this,{{$product->id}})"> @lang('main.buy-design')</a>
                        </div>
                    </div>
                </div>
            </div>
            @endif
@endsection

@push('custom-scripts')
<script type="text/javascript">
           $('select[name="north"]').on('change', function(e) {
                   e.preventDefault();

            var north = $(this).val();
            if(north) {
                $.ajax({
                    url: '{{url('/')}}/getDimension/{{$product->id}}/'+north,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {                        
                        $('select[name="south"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="south"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="south"]').niceSelect('update');
                    }
                });
            }else{
                $('select[name="south"]').empty();
            }
        });
        
             $('select[name="east"]').on('change', function(e) {
                   e.preventDefault();

            var east = $(this).val();
            if(east) {
                $.ajax({
                    url: '{{url('/')}}/getWestDimension/{{$product->id}}/'+east,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {                        
                        $('select[name="west"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="west"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="west"]').niceSelect('update');
                    }
                });
            }else{
                $('select[name="west"]').empty();
            }
        });
        
    function add_product_cart(e,productID){
       var x1 = productID;
            var _token = $("input[name='_token']").val();
            var north = $("select[name='north']").val();
            var east = $("select[name='east']").val();

            $.ajax({
                url: "{{route('site.add_cart')}}",
                type:'POST',
                data: {_token:_token, east:east, north:north, product_id:x1},
                success: function(data) {
                     if ((data.errors)) {
                    printErrorMsg(data.errors);
            }
                if (data == 1) {
                      toastr.success('تم الاضافه بنجاح');

                     setTimeout(function() {
                 window.location.href = ('{{url('/checkout?design='.$product->id)}}');

                        }, 2000); // 2 second

                }    
                 if (data == 3) {
                      toastr.error('من فضلك اختر أبعاد الأرض');
                }     
                 if (data == 2) {
                      toastr.error('يوجد خطأ في اختيار الأبعاد حاول مره أخري');
                }     
                if (data == 0) {
                      toastr.error('لقد اشتريت التصميم بالفعل');
                }          
                },

                error: function (data) {
                  toastr.error("من فضلك سجل الدخول أولا");                

                }
            });
       
        }

          function printErrorMsg (msg) {
            $(".print-login-error-msg").find("ul").html('');
            $(".print-login-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-login-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
</script>
@endpush