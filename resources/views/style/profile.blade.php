@extends('style/index')
@section('content')
  <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.my-account')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.profile')</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap profile-wrap col-xs-12">
                <div class="container">
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <ul>
                            <li class="active">
                                <a href="{{url('profile')}}">@lang('main.profile')</a>
                            </li>
                            <li>
                                <a href="{{url('favorite')}}">@lang('main.favorite')</a>
                            </li>
                            <li>
                                <a href="{{url('change-password')}}">@lang('main.password-no')</a>
                            </li>
                            <li>
                                <a href="{{url('userLogout')}}">@lang('main.logout')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="prof-content col-md-9 col-xs-12">
                        <div class="p-head col-xs-12">
                            <h4>@lang('main.profile')</h4>
                            <a class="btn edit-data">@lang('main.edit')</a>
                        </div>
                        <div class="pers-wrap col-xs-12">
                            <div class="prev-form col-xs-12">
                                <ul>
                                    <li>{{Auth::user()->first_name}} {{Auth::user()->last_name}} : @lang('main.name') </li>
                                    <li>{{Auth::user()->email}}  : @lang('main.email') </li>
                                    <li>{{Auth::user()->country_code}} {{Auth::user()->mobile}}: @lang('main.mobile') </li>
                                    <li>{{Auth::user()->city_id()->first()->city_name}} : @lang('main.city') </li>
                                    <li>@lang('main.region') : {{Auth::user()->region_id()->first()->region_name}}</li>
                                </ul>
                            </div>
                    <div class="edit-form col-xs-12" style="display: none;">
                              <form method="post">
                               @csrf
                               @method('put')
                               <div class="alert alert-danger print-error-msg" style="display:none">
                                    <ul></ul>
                                </div>
                                <div class="row">

                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> <i>*</i>@lang('main.first_name')</h4>
                                        <input type="text" name="first_name" value="{{Auth::user()->first_name}}" class="form-control" required>
                                        <i class="la la-user place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> <i>*</i>@lang('main.last_name')</h4>
                                        <input type="text" name="last_name" value="{{Auth::user()->last_name}}" class="form-control" required>
                                        <i class="la la-user place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.email')</h4>
                                        <input type="email" name="email" value="{{Auth::user()->email}}" class="form-control">
                                        <i class="la la-envelope place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.mobile')</h4>
                                        <input type="text" name="mobile" value="{{Auth::user()->mobile}}" class="form-control">
                                        <i class="la la-phone place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.city')</h4>
                                        <select name="city_id" class="form-control nice-select">
                                            <option>@lang('main.choose-city')</option>
                                            @foreach($citys as $city)
                                            <option value="{{$city->id}}" @if(Auth::user()->city_id == $city->id) selected @endif>{{$city->city_name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="la la-map-marker place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.region')</h4>
                                        <select name="region_id" class="form-control nice-select">
                                            @if(Auth::user()->region_id != null )
                                                <option value="{{Auth::user()->region_id}}" selected="selected">{{Auth::user()->region_id()->first()->region_name}}</option>
                                            @endif 
                                        </select>
                                        <i class="la la-map-marker place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <button type="submit" class="btn btn-submit">@lang('main.save')</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@push('custom-scripts')
<script type="text/javascript">
    
      
      $(".edit-data").click(function () {
        $(".prev-form").hide();
        $(".edit-form").show();
        
    });


    $(document).ready(function() {
           $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{url('/')}}/registerUser/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- اختر المدينه ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option @if (old('region_id') == '+ key +') selected="selected" @endif  value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="region_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });

        $(".btn-submit").click(function(e){
            e.preventDefault();
       
            var _token = $("input[name='_token']").val();
            var first_name = $("input[name='first_name']").val();
            var last_name = $("input[name='last_name']").val();
            var email = $("input[name='email']").val();
            var mobile = $("input[name='mobile']").val();
            var city_id = $("select[name='city_id']").val();
            var region_id = $("select[name='region_id']").val();
            $.ajax({
                url: "{{ route('updateProfile') }}",
                type:'put',
                data: {_token:_token, first_name:first_name, last_name:last_name, email:email, mobile:mobile, city_id:city_id, region_id:region_id},
                success: function(data) {
                     if ((data.errors)) {
                    printErrorMsg(data.errors);
            }
                             if (data == 1) {
                    $(".print-error-msg").css('display','none');
                      toastr.success('   @lang('main.edit-done')');

                     setTimeout(function() {
                 window.location.href = ('{{url('/profile')}}');

                        }, 2000); // 2 second

                }        
                if (data == 2) {
                    printErrorMsg(data.errors);

                     setTimeout(function() {
                 window.location.href = ('{{url('/profile')}}');

                        }, 2000); // 2 second

                }          
                },

                error: function (data) {
                  toastr.error("@lang('main.error')");                

                }
            });
       
        }); 

            function printErrorMsg (msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
        }
});

</script>
@endpush