@extends('style/index')
@push('custom-css')
<style type="text/css">
    .error-txt{
        color: red !important;
    }
</style>
@endpush
@section('content')
<main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.my-account')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.profile')</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap profile-wrap col-xs-12">
                <div class="container">
                    <div class="prof-sidebar col-md-3 col-xs-12">
                        <ul>
                            <li>
                                <a href="{{url('profile')}}">@lang('main.profile')</a>
                            </li>
                            <li>
                                <a href="{{url('favorite')}}">@lang('main.favorite')</a>
                            </li>
                            <li class="active">
                                <a href="{{url('change-password')}}">@lang('main.password-no')</a>
                            </li>
                            <li>
                                <a href="{{url('userLogout')}}">@lang('main.logout')</a>
                            </li>
                        </ul>
                    </div>
                    <div class="prof-content col-md-9 col-xs-12">
                        <div class="p-head col-xs-12">
                            <h4>@lang('main.change-password')</h4>
                        </div>
                        <div class="pers-wrap col-xs-12">
                            <div class="edit-form col-xs-12">
                                <form action="{{route('updatePassword')}}" method="post">
                                                    @csrf
                                                    @method('put')
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12">
                                        <h4>@lang('main.current-password')</h4>
                                        <input type="password"  required name="password" class="form-control" id="pass__1">
                                        <button type="button" class="show-pass" toggle="#pass__1">
                                            <i class="la la-eye-slash"></i>
                                        </button>
                                        <i class="la la-lock place-icon"></i>
                                        @error('password')
                                        <strong class="error-txt">{{$message}}</strong>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <h4>@lang('main.new-password')</h4>
                                        <input type="password" required name="new_password" class="form-control" id="pass__2">
                                        <button type="button" class="show-pass" toggle="#pass__2">
                                            <i class="la la-eye-slash"></i>
                                        </button>
                                        <i class="la la-lock place-icon"></i>
                                        @error('new_password')
                                        <strong>{{$message}}</strong>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <h4>@lang('main.confirm-password')</h4>
                                        <input type="password" required name="confirm_password" class="form-control" id="pass__3">
                                        <button type="button" class="show-pass" toggle="#pass__3">
                                            <i class="la la-eye-slash"></i>
                                        </button>
                                        <i class="la la-lock place-icon"></i>
                                        @error('confirm_password')
                                        <strong>{{$message}}</strong>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <button type="submit" class="btn">@lang('main.save')</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection