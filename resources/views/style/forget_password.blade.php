@extends('style/index')
@section('content')
   <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero2.jpg" alt="">
                <div class="container">
                    <h3>@lang('main.forget-pass')</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">@lang('main.home')</a>
                        </li>
                        <li>@lang('main.forget-pass')</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap col-xs-12">
                <div class="container">
                    <div class="log-form col-xs-12">
                        <div class="form-head col-xs-12">
                            <h4>@lang('main.forget-pass')</h4>
                        </div>
                          <form action="{{route('get-pass')}}" method="get">
                                @csrf
                             <div class="form-body col-xs-12">
                                <div class="form-group col-md-6 col-xs-12">
                                    <h4>@lang('main.email')</h4>
                                    <input type="email" name="email" required class="form-control">
                                </div>
                                <div class="form-group has-btn col-xs-12">
                                    <p class="green-par">@lang('main.write-your-email')</p>
                                    <button type="submit" class="btn">ارسال</button>
                                    <p class="hint">
                                        @lang('main.no-send')<a href="{{route('get-pass')}}">@lang('main.resend')</a>
                                    </p>
                                </div>
                              </div>
                              </form>
                    </div>
                </div>
            </div>
@endsection