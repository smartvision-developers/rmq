@extends('style/index')
@section('content')
  <main class="main-content col-xs-12">
            <div class="breads col-xs-12">
                <img src="{{url('style')}}/images/hero.jpg" alt="">
                <div class="container">
                    <h3>{{$cart->product_name}}</h3>
                    <ul>
                        <li>
                            <a href="{{url('/')}}">الرئيسية</a>
                        </li>
                        <li>شراء المنتج</li>
                    </ul>
                </div>
            </div>
            <div class="log-wrap profile-wrap checkout col-xs-12">
                <div class="container">
                <div class="prof-content col-md-9 col-xs-12">
                <form method="post" action="{{url('payProduct?design='.$cart->product_id)}}" >
                    @csrf
                    @method('put')
                    <input type="hidden" value="{{$cart->product_id}}" name="design">
                                           <div class="pers-wrap col-xs-12">
                            <h3>معلومات المشترى</h3>
                            <div class="edit-form col-xs-12">
                                @if(count($errors->all()) > 0)
                                <div class="alert alert-warning alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h6><i class="icon fas fa-exclamation-triangle"></i> {{ trans('admin.alert') }}!</h6>
                                 <ol>
                                    @foreach($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                    @endforeach
                                 </ol>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.full-name') <i>*</i></h4>
                                        <input type="text" name="client_name" value="{{old('client_name')}}" class="form-control" required>
                                        <i class="la la-user place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.client-card') <i>*</i></h4>
                                        <input type="text" name="client_id_card" value="{{old('client_id_card')}}" class="form-control" required>
                                        <i class="la la-id-card place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>@lang('main.mobile') <i>*</i></h4>
                                        <input type="text" value="{{old('client_mobile')}}" class="form-control" name="client_mobile" required>
                                        <i class="la la-mobile place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> @lang('main.email') <i>*</i></h4>
                                        <input type="email" class="form-control" name="client_email" value="{{old('client_email')}}" required>
                                        <i class="la la-envelope place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> @lang('main.city') <i>*</i></h4>
                                        <select name="city_id" class="form-control nice-select" required>
                                           <option>@lang('main.choose-city')</option>
                                            @foreach($citys as $city)
                                            <option @if(old('city_id') == $city->id ) selected @endif value="{{$city->id}}" >{{$city->city_name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="la la-map-marker place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4> @lang('main.region') <i>*</i></h4>
                                        <select name="region_id" class="form-control nice-select" required>
                                            <option>@lang('main.choose-region')</option>
                                        </select>
                                        <i class="la la-map-marker place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>  المهنة <i>*</i></h4>
                                        <input name="client_job" value="{{old('client_job')}}" type="text" class="form-control" required>
                                        <i class="la la-user-cog place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12">
                                        <h4>  صورة الصك او التقرير المساحى <i>*</i></h4>
                                        <div class="f-upload">
                                            <input type="file" required name="report_image">
                                            <i class="la la-file-text place-icon"></i>
                                            <input type="text" class="form-control" readonly="" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pers-wrap col-xs-12">
                            <h3>معلومات  بطاقة الدفع</h3>
                            <div class="edit-form col-xs-12">
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12">
                                        <h4>الاسم مثل البطاقة <i>*</i></h4>
                                        <input type="text" class="form-control" required>
                                        <i class="la la-user place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-12 col-xs-12">
                                        <h4> رقم البطاقة <i>*</i></h4>
                                        <input type="text" class="form-control ccFormatMonitor" placeholder="رقم البطاقة" maxlength='19' required>
                                        <i class="la la-credit-card place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <h4> العام <i>*</i></h4>
                                        <select class="form-control nice-select" required>
                                            <option>اختار العام</option>
                                            <option>اختار العام</option>
                                            <option>اختار العام</option>
                                            <option>اختار العام</option>
                                        </select>
                                        <i class="la la-calendar place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <h4> الشهر <i>*</i></h4>
                                        <select class="form-control nice-select" required>
                                            <option>اختار الشهر</option>
                                            <option>اختار الشهر</option>
                                            <option>اختار الشهر</option>
                                            <option>اختار الشهر</option>
                                        </select>
                                        <i class="la la-calendar place-icon"></i>
                                    </div>
                                    <div class="form-group col-md-4 col-xs-12">
                                        <h4>  ccv رقم ال <i>*</i></h4>
                                        <input type="password" class="form-control cvv" placeholder="***" maxlength='3' required>
                                        <i class="la la-credit-card place-icon"></i>
                                    </div>
                                    <div class="form-group confirm col-md-12 col-xs-12">
                                        <label>
                                            <input name="purchase_agreement" id="purchase_agreement" 
                                            @if(old('purchase_agreement')) checked @endif  type="checkbox">
                                            <span>أوافق على <a href="#" data-toggle="modal" data-target="#confirm_pop">@lang('main.policy')</a></span>
                                        </label>
                                        <button type="submit" class="btn">ادفع  @if($cart->price_after_coupon != null) {{$cart->price_after_coupon}} @else {{$cart->total_price}} @endif</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </form>
                </div>
                    <div class="checkout-sidebar col-md-3 col-xs-12">
                        @if(! $cart->product_id()->first()->product_book->isEmpty())
                        <div class="s-widget col-xs-12">
                            <h4>تفاصيل محتويات ملف التصميم</h4>
                            <div class="s-inner">
                                <ul>
                                   @foreach($cart->product_id()->first()->product_book as $value)
                                        <li>
                                            <i class="la la-check"></i>
                                            {{$value->product_detail}}
                                        </li>
                                    @endforeach 
                                </ul>
                            </div>
                                
                        </div>
                        @endif
                        <div class="w-widget col-xs-12">
                            <span>@lang('main.price') : {{$cart->product_price}} @lang('main.sar')</span>
                        </div>
                        @if($cart->price_after_coupon == null || $cart->coupon_id == null)
                        <div class="w-widget col-xs-12">
                            <form action="{{route('apply-coupon')}}?design={{$cart->product_id}}" method="post">
                                @csrf
                            <div class="form-group">
                                <input type="text" required name="coupon_code" class="form-control" placeholder="كود الخصم ">
                                <button type="submit" class="btn">خصم</button>
                            </div>
                            </form>
                        </div>
                        @endif
                        <div class="w-widget col-xs-12">
                            <span>@lang('main.vat') : {{$cart->vat}} %</span>
                        </div>
                        <div class="w-widget total col-xs-12">
                            <span>@lang('main.total-price')  : {{$cart->total_price}} @lang('main.sar')</span>
                        </div>

                        <div class="w-widget col-xs-12">
                            <span>السعر بعد الخصم : {{$cart->price_after_coupon}} رس</span>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@push('custom-scripts')
<script type="text/javascript">
    $(document).ready(function() {
       $("input[name='purchase_agreement']").prop('required',true);

           $('select[name="city_id"]').on('change', function(e) {
                   e.preventDefault();

            var cityID = $(this).val();
            console.log(cityID);
            if(cityID) {
                $.ajax({
                    url: '{{url('/')}}/registerUser/regions/'+cityID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="region_id"]').empty();
                          $('select[name="region_id"]').append('<option value="">--- اختر المدينه ---</option>');
                        $.each(data, function(key, value) {
                            $('select[name="region_id"]').append('<option @if (old('region_id') == '+ key +') selected="selected" @endif  value="'+ key +'">'+ value +'</option>');
                        });
                        $('select[name="region_id"]').niceSelect('update');


                    }
                });
            }else{
                $('select[name="region_id"]').empty();
            }
        });
});
</script>
@endpush