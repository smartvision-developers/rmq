<?php

use Illuminate\Support\Facades\Route;
use Spatie\Honeypot\ProtectAgainstSpam;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['middleware' => 'auth'],

	function () {
		Route::any('logout', 'Auth\LoginController@logout')->name('web.logout');
	});

Route::get('/', function () {
	return view('welcome');
});

Route::middleware(ProtectAgainstSpam::class)->group(function () {
	Auth::routes([  'register' => false, // Register Routes...

  'reset' => false, // Reset Password Routes...

  'verify' => false, // Email Verification Routes...
  'login' => false,
  'logout' => false,
]);

});




Route::group(['namespace' => 'Website', 'middleware' => 'Lang'], function () {
	Route::get('/', 'HomeController@index')->name('site');
	Route::get('/contact', 'HomeController@contact');
	Route::post('/storeContact', 'HomeController@storeContact')->name('storeContact');
	Route::get('/search-result', 'HomeController@search');

	Route::get('/products', 'ProductController@products');	
	Route::get('/product/{id}', 'ProductController@product');	
	Route::get('/about-us', 'AboutController@index');
	Route::get('/registerUser/regions/{id}', 'ClientController@get_ajax_region')->name('get_ajax_region');
	Route::get('/getDimension/{id}/{north}', 'ClientController@get_ajax_dimension')->name('get_ajax_dimension');
	Route::get('/getWestDimension/{id}/{east}', 'ClientController@get_ajax_west_dimension')->name('get_ajax_west_dimension');



Route::group(['middleware' => 'auth:web'], function () {
	    Route::post('/products/addFav', 'ProductController@add_product_fav')->name('site.add_fav');
	   	Route::get('/profile', 'ClientController@get_profile');
	    Route::put('/update-profile', 'ClientController@update_profile')->name('updateProfile');
	   	Route::get('/change-password', 'ClientController@change_password');
	    Route::put('/update-password', 'ClientController@update_password')->name('updatePassword');
	    Route::get('/favorite', 'ProductController@get_favs');
	    Route::get('/checkout', 'ClientController@get_checkout');
	    Route::post('/product/addCart', 'ClientController@add_product_cart')->name('site.add_cart');

	    Route::post('/apply-coupon', 'ClientController@apply_coupon')->name('apply-coupon');

	    Route::put('/payProduct', 'ClientController@pay_product')->name('payProduct');

		Route::get('/userLogout', 'ClientController@userLogout');
	});

	Route::group(['middleware' => 'guest:web'], function () {
		Route::get('/login', 'ClientController@register');
		Route::post('/storeClient', 'ClientController@store')->name('storeClient');

		Route::post('/clientLogin', 'ClientController@clientLogin')->name('login');
		Route::get('/forget_password', 'ClientController@forget_password');
		Route::get('/get_password', 'ClientController@get_password')->name('get-pass');
		Route::get('/new_password/{email}', 'ClientController@check_code')->name('site.checkCode');
		Route::post('/checking', 'ClientController@checking')->name('site.checking');



	});
});
