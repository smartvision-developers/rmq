<?php
use Illuminate\Support\Facades\Route;

\L::Panel(app('admin')); ///SetLangredirecttoadmin
\L::LangNonymous(); //RunRouteLang'namespace'=>'Admin',

Route::group(['prefix' => app('admin'), 'middleware' => 'Lang'], function () {
	Route::get('lock/screen', 'Admin\AdminAuthenticated@lock_screen');
	Route::get('theme/{id}', 'Admin\Dashboard@theme');
	Route::group(['middleware' => 'admin_guest'], function () {

		Route::get('login', 'Admin\AdminAuthenticated@login_page');
		Route::post('login', 'Admin\AdminAuthenticated@login_post');
		Route::view('forgot/password', 'admin.forgot_password');

		Route::post('reset/password', 'Admin\AdminAuthenticated@reset_password');
		Route::get('password/reset/{token}', 'Admin\AdminAuthenticated@reset_password_final');
		Route::post('password/reset/{token}', 'Admin\AdminAuthenticated@reset_password_change');
	});

	Route::view('need/permission', 'admin.no_permission');

	Route::group(['middleware' => 'admin:admin'], function () {
		if (class_exists(\UniSharp\LaravelFilemanager\Lfm::class)) {
			Route::group(['prefix' => 'filemanager'], function () {
				\UniSharp\LaravelFilemanager\Lfm::routes();
			});
		}

		////////AdminRoutes/*Start*///////////////
		Route::get('/', 'Admin\Dashboard@home');
		Route::any('logout', 'Admin\AdminAuthenticated@logout');
		Route::get('account', 'Admin\AdminAuthenticated@account');
		Route::post('account', 'Admin\AdminAuthenticated@account_post');
		Route::resource('settings', 'Admin\Settings');
		Route::post('settings/upload/multi','Admin\Settings@multi_upload');
		Route::post('settings/delete/file','Admin\Settings@delete_file'); 
		Route::resource('admingroups', 'Admin\AdminGroups');
		Route::post('admingroups/multi_delete', 'Admin\AdminGroups@multi_delete');
		Route::resource('admins', 'Admin\Admins');
		Route::post('admins/multi_delete', 'Admin\Admins@multi_delete');
		Route::resource('countrys','Admin\Countrys'); 
		Route::post('countrys/multi_delete','Admin\Countrys@multi_delete'); 
		Route::resource('citys','Admin\Citys'); 
		Route::post('citys/multi_delete','Admin\Citys@multi_delete'); 
		Route::resource('regions','Admin\Regions'); 
		Route::post('regions/multi_delete','Admin\Regions@multi_delete'); 
		Route::resource('users','Admin\Users'); 
		Route::post('users/multi_delete','Admin\Users@multi_delete'); 
		Route::post('users/get/region/id','Admin\Users@get_region_id'); 
		Route::resource('sliders','Admin\Sliders'); 
		Route::post('sliders/multi_delete','Admin\Sliders@multi_delete'); 
		Route::resource('contacts','Admin\Contacts'); 
		Route::post('contacts/multi_delete','Admin\Contacts@multi_delete'); 
		Route::resource('abouts','Admin\Abouts'); 
		Route::post('abouts/multi_delete','Admin\Abouts@multi_delete'); 
		Route::resource('websitelinks','Admin\WebsiteLinks'); 
		Route::post('websitelinks/multi_delete','Admin\WebsiteLinks@multi_delete'); 
		Route::resource('products','Admin\Products'); 
		Route::post('products/upload/multi','Admin\Products@multi_upload');
		Route::post('products/multi_delete','Admin\Products@multi_delete'); 
		Route::resource('productfavourites','Admin\ProductFavourites'); 
		Route::post('productfavourites/multi_delete','Admin\ProductFavourites@multi_delete'); 
		Route::resource('mostpopulars','Admin\MostPopulars'); 
		Route::post('mostpopulars/multi_delete','Admin\MostPopulars@multi_delete'); 
		Route::resource('statistics','Admin\Statistics'); 
		Route::post('statistics/multi_delete','Admin\Statistics@multi_delete'); 
		Route::resource('sociallinks','Admin\SocialLinks'); 
		Route::post('sociallinks/multi_delete','Admin\SocialLinks@multi_delete'); 
		Route::resource('tradmarks','Admin\Tradmarks'); 
		Route::post('tradmarks/multi_delete','Admin\Tradmarks@multi_delete'); 
		Route::resource('productbooks','Admin\ProductBooks'); 
		Route::post('productbooks/multi_delete','Admin\ProductBooks@multi_delete'); 
		Route::resource('carts','Admin\Carts'); 
		Route::post('carts/multi_delete','Admin\Carts@multi_delete'); 
		Route::post('carts/get/region/id','Admin\Carts@get_region_id'); 
		Route::resource('productdimensions','Admin\ProductDimensions'); 
		Route::post('productdimensions/multi_delete','Admin\ProductDimensions@multi_delete'); 
		Route::resource('productcoupons','Admin\ProductCoupons'); 
		Route::post('productcoupons/multi_delete','Admin\ProductCoupons@multi_delete'); 
		////////AdminRoutes/*End*///////////////
	});

});