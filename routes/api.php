<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
// your api is integerated but if you want reintegrate no problem
// to configure jwt-auth visit this link https://jwt-auth.readthedocs.io/en/docs/

Route::group(['middleware' => ['ApiLang', 'cors'], 'prefix' => 'v1', 'namespace' => 'Api\V1'], function () {

	Route::get('/', function () {

	});
	// Insert your Api Here Start //
	Route::group(['middleware' => 'guest'], function () {
		Route::post('login', 'Auth\AuthAndLogin@login')->name('api.login');
		Route::post('register', 'Auth\Register@register')->name('api.register');
	});

	Route::group(['middleware' => 'auth:api'], function () {
		Route::get('account', 'Auth\AuthAndLogin@account')->name('api.account');
		Route::post('logout', 'Auth\AuthAndLogin@logout')->name('api.logout');
		Route::post('refresh', 'Auth\AuthAndLogin@refresh')->name('api.refresh');
		Route::post('me', 'Auth\AuthAndLogin@me')->name('api.me');
		Route::post('change/password', 'Auth\AuthAndLogin@change_password')->name('api.change_password');
		//Auth-Api-Start//
		Route::apiResource("countrys","CountrysApi", ["as" => "api.countrys"]); 
			Route::post("countrys/multi_delete","CountrysApi@multi_delete"); 
			Route::apiResource("citys","CitysApi", ["as" => "api.citys"]); 
			Route::post("citys/multi_delete","CitysApi@multi_delete"); 
			Route::apiResource("regions","RegionsApi", ["as" => "api.regions"]); 
			Route::post("regions/multi_delete","RegionsApi@multi_delete"); 
			Route::apiResource("users","UsersApi", ["as" => "api.users"]); 
			Route::post("users/multi_delete","UsersApi@multi_delete"); 
			Route::apiResource("sliders","SlidersApi", ["as" => "api.sliders"]); 
			Route::post("sliders/multi_delete","SlidersApi@multi_delete"); 
			Route::apiResource("contacts","ContactsApi", ["as" => "api.contacts"]); 
			Route::post("contacts/multi_delete","ContactsApi@multi_delete"); 
			Route::apiResource("trademarks","TrademarksApi", ["as" => "api.trademarks"]); 
			Route::post("trademarks/multi_delete","TrademarksApi@multi_delete"); 
			Route::apiResource("abouts","AboutsApi", ["as" => "api.abouts"]); 
			Route::post("abouts/multi_delete","AboutsApi@multi_delete"); 
			Route::apiResource("websitelinks","WebsiteLinksApi", ["as" => "api.websitelinks"]); 
			Route::post("websitelinks/multi_delete","WebsiteLinksApi@multi_delete"); 
			Route::apiResource("products","ProductsApi", ["as" => "api.products"]); 
			Route::post("products/multi_delete","ProductsApi@multi_delete"); 
			Route::apiResource("productfavourites","ProductFavouritesApi", ["as" => "api.productfavourites"]); 
			Route::post("productfavourites/multi_delete","ProductFavouritesApi@multi_delete"); 
			Route::apiResource("mostpopulars","MostPopularsApi", ["as" => "api.mostpopulars"]); 
			Route::post("mostpopulars/multi_delete","MostPopularsApi@multi_delete"); 
			Route::apiResource("statistics","StatisticsApi", ["as" => "api.statistics"]); 
			Route::post("statistics/multi_delete","StatisticsApi@multi_delete"); 
			Route::apiResource("sociallinks","SocialLinksApi", ["as" => "api.sociallinks"]); 
			Route::post("sociallinks/multi_delete","SocialLinksApi@multi_delete"); 
			Route::apiResource("tradmarks","TradmarksApi", ["as" => "api.tradmarks"]); 
			Route::post("tradmarks/multi_delete","TradmarksApi@multi_delete"); 
			Route::apiResource("productbooks","ProductBooksApi", ["as" => "api.productbooks"]); 
			Route::post("productbooks/multi_delete","ProductBooksApi@multi_delete"); 
			Route::apiResource("carts","CartsApi", ["as" => "api.carts"]); 
			Route::post("carts/multi_delete","CartsApi@multi_delete"); 
			Route::apiResource("productdimensions","ProductDimensionsApi", ["as" => "api.productdimensions"]); 
			Route::post("productdimensions/multi_delete","ProductDimensionsApi@multi_delete"); 
			Route::apiResource("productcoupons","ProductCouponsApi", ["as" => "api.productcoupons"]); 
			Route::post("productcoupons/multi_delete","ProductCouponsApi@multi_delete"); 
			//Auth-Api-End//
	});
	// Insert your Api Here End //
});