<?php
namespace App\Providers;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */

	public function boot() {

		Paginator::useBootstrap();

		app()->singleton('admin', function () {
			return 'admin';
		});
		if (file_exists(base_path('config/itconfiguration.php'))) {
			Schema::defaultStringLength(config('itconfiguration.SchemadefaultStringLength'));
			if (config('itconfiguration.ForeignKeyConstraints')) {
				Schema::enableForeignKeyConstraints();
			} else {
				Schema::disableForeignKeyConstraints();
			}
		}
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */

	public function register() {
		view()->composer('*', function ($view) 
        {
           
		$website_links=\App\Models\WebsiteLink::whereNull('sub_link')->where('link_status','show')->with('child')->take(30)->get();
		// //dd($website_links);
		$social_links=\App\Models\SocialLink::whereStatus('show')->take(4)->get();
		 $trademarks=\App\Models\Tradmark::take(10)->get();

        $view->with(compact('website_links','trademarks','social_links'));

        });
	}
}
