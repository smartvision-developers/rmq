<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'product_name'=>'required|string',
             'product_price'=>'required|numeric',
             'product_id'=>'required|integer|exists:products,id',
             'client_id'=>'required|integer|exists:users,id',
             'client_name'=>'sometimes|nullable|string',
             'client_mobile'=>'sometimes|nullable|string',
             'client_email'=>'sometimes|nullable|email',
             'client_id_card'=>'sometimes|nullable|string',
             'client_job'=>'sometimes|nullable|string',
             'report_image'=>'sometimes|nullable|file|image',
             'city_id'=>'sometimes|nullable|integer|exists:cities,id',
             'region_id'=>'sometimes|nullable|integer|exists:regions,id',
             'vat'=>'sometimes|nullable|integer',
             'total_price'=>'sometimes|nullable|numeric',
             'north'=>'required',
             'east'=>'required',
		];
	}

	protected function onUpdate() {
		return [
             'product_name'=>'required|string',
             'product_price'=>'required|numeric',
             'product_id'=>'required|integer|exists:products,id',
             'client_id'=>'required|integer|exists:users,id',
             'client_name'=>'sometimes|nullable|string',
             'client_mobile'=>'sometimes|nullable|string',
             'client_email'=>'sometimes|nullable|email',
             'client_id_card'=>'sometimes|nullable|string',
             'client_job'=>'sometimes|nullable|string',
             'report_image'=>'sometimes|nullable|file|image',
             'city_id'=>'sometimes|nullable|integer|exists:cities,id',
             'region_id'=>'sometimes|nullable|integer|exists:regions,id',
             'vat'=>'sometimes|nullable|integer',
             'total_price'=>'sometimes|nullable|numeric',
             'north'=>'required',
             'east'=>'required',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_name'=>trans('admin.product_name'),
             'product_price'=>trans('admin.product_price'),
             'product_id'=>trans('admin.product_id'),
             'client_id'=>trans('admin.client_id'),
             'client_name'=>trans('admin.client_name'),
             'client_mobile'=>trans('admin.client_mobile'),
             'client_email'=>trans('admin.client_email'),
             'client_id_card'=>trans('admin.client_id_card'),
             'client_job'=>trans('admin.client_job'),
             'report_image'=>trans('admin.report_image'),
             'city_id'=>trans('admin.city_id'),
             'region_id'=>trans('admin.region_id'),
             'vat'=>trans('admin.vat'),
             'total_price'=>trans('admin.total_price'),
             'north'=>trans('admin.north'),
             'east'=>trans('admin.east'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}