<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductCouponsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		$today=date('m/d/Y');
		return [
             'product_id'=>'required|exists:products,id',
             'coupon_name'=>'required|string',
             'coupon_code'=>'required|string|min:3|max:10',
             'start_date'=>'required|date|date_format:Y-m-d|after_or_equal:'.$today,
			 'end_date'=>'required|date|date_format:Y-m-d|after:start_date',
             'coupon_discount'=>'required|numeric|between:0,99.99',
             'discount_type'=>'required|string|in:percent,sar',
             'status'=>'required|string|in:show,hide',
		];
	}

	protected function onUpdate() {
		$today=date('m/d/Y');
		return [
             'product_id'=>'required|exists:products,id',
             'coupon_name'=>'',
             'coupon_code'=>'',
			 'start_date'=>'sometimes|nullable|date|date_format:Y-m-d|after_or_equal:'.$today,
			 'end_date'=>'sometimes|nullable|date|date_format:Y-m-d|after:start_date',
             'coupon_discount'=>'required|numeric|between:0,99.99',
             'discount_type'=>'required|string|in:percent,sar',
             'status'=>'required|string|in:show,hide',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_id'=>trans('admin.product_id'),
             'coupon_name'=>trans('admin.coupon_name'),
             'coupon_code'=>trans('admin.coupon_code'),
             'start_date'=>trans('admin.start_date'),
             'end_date'=>trans('admin.end_date'),
             'coupon_discount'=>trans('admin.coupon_discount'),
             'discount_type'=>trans('admin.discount_type'),
             'status'=>trans('admin.status'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}