<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'product_name'=>'required|string',
             'number_floor'=>'required|integer',
             'number_room'=>'required|integer',
             'land_area'=>'required|numeric',
             'design_by'=>'required|string',
             'price'=>'required|numeric',
             'product_image'=>'required|file|image',
             'product_description'=>'required|string',
             'product_video'=>'sometimes|nullable|file|video|mp4',
             'product_content'=>'required|string',
             'chart_information'=>'sometimes|nullable|string',
             'design_book'=>'sometimes|nullable|file|pdf',
             'design_files'=>'sometimes|nullable|file|zip',
             'design_file_details'=>'sometimes|nullable|string',
             'notes'=>'sometimes|nullable|string',
             'stock'=>'required|integer|min:1',
             'sold'=>'sometimes|nullable|integer',
             'status'=>'required|string|in:show,hide',
		];
	}

	protected function onUpdate() {
		return [
             'product_name'=>'required|string',
             'number_floor'=>'required|integer',
             'number_room'=>'required|integer',
             'land_area'=>'required|numeric',
             'design_by'=>'required|string',
             'price'=>'required|numeric',
             'product_image'=>'sometimes|nullable|file|image',
             'product_description'=>'required|string',
             'product_video'=>'sometimes|nullable|file|video|mp4',
             'product_content'=>'required|string',
             'chart_information'=>'sometimes|nullable|string',
             'design_book'=>'sometimes|nullable|file|pdf',
             'design_files'=>'sometimes|nullable|file|zip',
             'design_file_details'=>'sometimes|nullable|string',
             'notes'=>'sometimes|nullable|string',
             'stock'=>'required|integer|min:1|gt:sold',
             'sold'=>'sometimes|nullable|integer',
             'status'=>'required|string|in:show,hide',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_name'=>trans('admin.product_name'),
             'number_floor'=>trans('admin.number_floor'),
             'number_room'=>trans('admin.number_room'),
             'land_area'=>trans('admin.land_area'),
             'design_by'=>trans('admin.design_by'),
             'price'=>trans('admin.price'),
             'product_image'=>trans('admin.product_image'),
             'product_description'=>trans('admin.product_description'),
             'product_video'=>trans('admin.product_video'),
             'product_content'=>trans('admin.product_content'),
             'chart_information'=>trans('admin.chart_information'),
             'design_book'=>trans('admin.design_book'),
             'design_file_details'=>trans('admin.design_file_details'),
             'notes'=>trans('admin.notes'),
             'stock'=>trans('admin.stock'),
             'sold'=>trans('admin.sold'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}