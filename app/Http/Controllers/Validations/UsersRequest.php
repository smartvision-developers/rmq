<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UsersRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'first_name'=>'required|string',
             'last_name'=>'required|string',
             'email'=>'required|email|unique:users,email',
             'mobile'=>'required|numeric|unique:users,mobile',
             'password'=>'required|string',
             'city_id'=>'required|integer|exists:cities,id',
             'region_id'=>'required|integer|exists:regions,id',
             'country_code'=>'required|string',
             'status'=>'required|string|in:pending,accepted,rejected',
             'decline_reason'=>'sometimes|nullable|string|required_if:status,==,rejected',
		];
	}

	protected function onUpdate() {
		return [
             'first_name'=>'required|string',
             'last_name'=>'required|string',
             'email'=>'required|email|unique:users,email,'.$this->segment(3).',id',
             'mobile'=>'required|numeric|unique:users,mobile,'.$this->segment(3).',id',
             'password'=>'sometimes|nullable|string',
             'city_id'=>'required|integer|exists:cities,id',
             'region_id'=>'sometimes|nullable|integer|exists:regions,id',
             'country_code'=>'required|string',
             'status'=>'sometimes|nullable|string|in:pending,accepted,rejected',
			'decline_reason'=>'sometimes|nullable|string|required_if:status,==,rejected',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'first_name'=>trans('admin.first_name'),
             'last_name'=>trans('admin.last_name'),
             'email'=>trans('admin.email'),
             'mobile'=>trans('admin.mobile'),
             'password'=>trans('admin.password'),
             'city_id'=>trans('admin.city_id'),
             'region_id'=>trans('admin.region_id'),
             'country_code'=>trans('admin.country_code'),
             'status'=>trans('admin.status'),
             'decline_reason'=>trans('admin.decline_reason'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}