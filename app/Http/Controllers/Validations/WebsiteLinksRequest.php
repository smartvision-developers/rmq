<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class WebsiteLinksRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'link_name'=>'required|string',
             'link_url'=>'required|url',
             'link_position'=>'required|string|in:header,footer',
             'sub_link'=>'sometimes|nullable',
             'link_target'=>'required|string|in:self,blank',
             'link_status'=>'required|string|in:show,hide',
		];
	}

	protected function onUpdate() {
		return [
             'link_name'=>'required|string',
             'link_url'=>'required|url',
             'link_position'=>'required|string|in:header,footer',
             'sub_link'=>'sometimes|nullable',
             'link_target'=>'required|string|in:self,blank',
             'link_status'=>'required|string|in:show,hide',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'link_name'=>trans('admin.link_name'),
             'link_url'=>trans('admin.link_url'),
             'link_position'=>trans('admin.link_position'),
             'sub_link'=>trans('admin.sub_link'),
             'link_target'=>trans('admin.link_target'),
             'link_status'=>trans('admin.link_status'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}