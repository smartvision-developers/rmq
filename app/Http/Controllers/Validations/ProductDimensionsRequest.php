<?php
namespace App\Http\Controllers\Validations;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductDimensionsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'product_id'=>'required|integer|exists:products,id',
             'north'=>'required|numeric',
             'east'=>'required|numeric',
             'south'=>'sometimes|nullable|numeric',
             'west'=>'sometimes|nullable|numeric',
		];
	}

	protected function onUpdate() {
		return [
             'product_id'=>'required|integer|exists:products,id',
             'north'=>'required|numeric',
             'east'=>'required|numeric',
             'south'=>'sometimes|nullable|numeric',
             'west'=>'sometimes|nullable|numeric',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_id'=>trans('admin.product_id'),
             'north'=>trans('admin.north'),
             'east'=>trans('admin.east'),
             'south'=>trans('admin.south'),
             'west'=>trans('admin.west'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}