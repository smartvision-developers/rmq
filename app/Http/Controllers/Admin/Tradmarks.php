<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\TradmarksDataTable;
use Carbon\Carbon;
use App\Models\Tradmark;

use App\Http\Controllers\Validations\TradmarksRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Tradmarks extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:tradmarks_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:tradmarks_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:tradmarks_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:tradmarks_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(TradmarksDataTable $tradmarks)
            {
               return $tradmarks->render('admin.tradmarks.index',['title'=>trans('admin.tradmarks')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.tradmarks.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(TradmarksRequest $request)
            {
                $data = $request->except("_token", "_method");
            	$data['image'] = "";
		  		$tradmarks = Tradmark::create($data); 
               if(request()->hasFile('image')){
              $tradmarks->image = it()->upload('image','tradmarks/'.$tradmarks->id);
              $tradmarks->save();
              }
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('tradmarks'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$tradmarks =  Tradmark::find($id);
        		return is_null($tradmarks) || empty($tradmarks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("tradmarks")) :
        		view('admin.tradmarks.show',[
				    'title'=>trans('admin.show'),
					'tradmarks'=>$tradmarks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$tradmarks =  Tradmark::find($id);
        		return is_null($tradmarks) || empty($tradmarks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("tradmarks")) :
        		view('admin.tradmarks.edit',[
				  'title'=>trans('admin.edit'),
				  'tradmarks'=>$tradmarks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new TradmarksRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(TradmarksRequest $request,$id)
            {
              // Check Record Exists
              $tradmarks =  Tradmark::find($id);
              if(is_null($tradmarks) || empty($tradmarks)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("tradmarks"));
              }
              $data = $this->updateFillableColumns(); 
               if(request()->hasFile('image')){
              it()->delete($tradmarks->image);
              $data['image'] = it()->upload('image','tradmarks');
               } 
              Tradmark::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('tradmarks'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$tradmarks = Tradmark::find($id);
		if(is_null($tradmarks) || empty($tradmarks)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("tradmarks"));
		}
               		if(!empty($tradmarks->image)){
			it()->delete($tradmarks->image);		}

		it()->delete('tradmark',$id);
		$tradmarks->delete();
		return redirectWithSuccess(aurl("tradmarks"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$tradmarks = Tradmark::find($id);
				if(is_null($tradmarks) || empty($tradmarks)){
					return backWithError(trans('admin.undefinedRecord'),aurl("tradmarks"));
				}
                    					if(!empty($tradmarks->image)){
				  it()->delete($tradmarks->image);
				}
				it()->delete('tradmark',$id);
				$tradmarks->delete();
			}
			return redirectWithSuccess(aurl("tradmarks"),trans('admin.deleted'));
		}else {
			$tradmarks = Tradmark::find($data);
			if(is_null($tradmarks) || empty($tradmarks)){
				return backWithError(trans('admin.undefinedRecord'),aurl("tradmarks"));
			}
                    
			if(!empty($tradmarks->image)){
			 it()->delete($tradmarks->image);
			}			it()->delete('tradmark',$data);
			$tradmarks->delete();
			return redirectWithSuccess(aurl("tradmarks"),trans('admin.deleted'));
		}
	}
            

}