<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class Settings extends Controller {

	public function __construct() {

		$this->middleware('AdminRole:settings_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:settings_edit', [
			'only' => ['store'],
		]);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('admin.settings', ['title' => trans('admin.settings')]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$rules = [
			'sitename_ar' => 'required',
			'sitename_en' => 'sometimes|nullable',
			'sitename_fr' => 'sometimes|nullable',
			'email' => 'required',
			'mobile' => 'required',
			'vat' => 'required',
			'commercial_registration_no' => 'required',
			'address' => 'required',
			'tax_no' => 'required',
			'logo' => 'sometimes|nullable|' . it()->image(),
			'icon' => 'sometimes|nullable|' . it()->image(),
			'system_status' => 'required',
			'system_message' => '',
			'lat' => '',
			'lng' => '',
			'about_us' => 'required',
			'video' => 'sometimes|nullable|file|video|mp4',
			'policy' => 'required', 
		];

		$data = $this->validate(request(), $rules, [], [
			'sitename_ar' => trans('admin.sitename_ar'),
			'sitename_en' => trans('admin.sitename_en'),
			'sitename_fr' => trans('admin.sitename_fr'),
			'email' => trans('admin.email'),
			'mobile' => trans('admin.mobile'),
			'address' => trans('admin.address'),
			'logo' => trans('admin.logo'),
			'icon' => trans('admin.icon'),
			'system_status' => trans('admin.system_status'),
			'system_message' => trans('admin.system_message'),
			'about_us' => trans('admin.about_us'),
			'video' => trans('admin.video'),
			'commercial_registration_no' => trans('admin.commercial_registration_no'),
			'tax_no' => trans('admin.tax_no'),
			'vat' => trans('admin.vat'),
			'lat' => trans('admin.lat'),
			'lng' => trans('admin.lng'),
			'policy' => trans('admin.policy'),
		]);
		if (request()->hasFile('logo')) {
			$data['logo'] = it()->upload('logo', 'setting');
		}
		if (request()->hasFile('icon')) {
			$data['icon'] = it()->upload('icon', 'setting');
		}
		if (request()->hasFile('video')) {
			$data['video'] = it()->upload('video', 'setting');
		}
		Setting::orderBy('id', 'desc')->update($data);
		session()->flash('success', trans('admin.updated'));
		return redirect(aurl('settings'));

	}
// Delete Files From Dropzone Library
	public function delete_file() {
		if (request("type_file") && request("type_id")) {
			if (it()->getFile(request("type_file"), request("type_id"))) {
				it()->delete(null, null, request("id"));
				return response([
					"status" => true,
				], 200);
			}
		}
	}

	// Multi upload with dropzone
	public function multi_upload() {
		if (request()->ajax()) {
			$rules = [];
			if(request()->hasFile("about_image")){
				$rules["about_image"] = "required|file|image";
			}


			$this->validate(request(), $rules, [], [
				 "about_image" => trans("admin.about_image"),

			]);

			if(request()->hasFile("about_image")){
				it()->upload("about_image", request("dz_path"), "settings", request("dz_id"));
			}

			return response([
				"status" => true,
				"type" => request("dz_type"),
				"file" => it()->getFile("setting", request("dz_id")),
			], 200);
		}

	}

}
