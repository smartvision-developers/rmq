<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\ProductCouponsDataTable;
use Carbon\Carbon;
use App\Models\ProductCoupon;
use App\Models\Product_Coupon;
use App\Models\Product;
use App\Http\Controllers\Validations\ProductCouponsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductCoupons extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:productcoupons_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:productcoupons_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:productcoupons_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:productcoupons_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(ProductCouponsDataTable $productcoupons)
            {
               return $productcoupons->render('admin.productcoupons.index',['title'=>trans('admin.productcoupons')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.productcoupons.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(ProductCouponsRequest $request)
            {
                $data = $request->except("_token", "_method",'product_id');
            			  		$productcoupons = ProductCoupon::create($data); 
            	foreach($request->product_id as $value){
		  			$get_product=Product::where('id',$value)->first();
				  		Product_Coupon::create([
				  			'product_id' => $value,
				  			'coupon_id' =>$productcoupons->id,
				  		]);
				  	}
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('productcoupons'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$productcoupons =  ProductCoupon::find($id);
        		return is_null($productcoupons) || empty($productcoupons)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productcoupons")) :
        		view('admin.productcoupons.show',[
				    'title'=>trans('admin.show'),
					'productcoupons'=>$productcoupons
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$productcoupons =  ProductCoupon::find($id);
        		return is_null($productcoupons) || empty($productcoupons)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productcoupons")) :
        		view('admin.productcoupons.edit',[
				  'title'=>trans('admin.edit'),
				  'productcoupons'=>$productcoupons
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new ProductCouponsRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(ProductCouponsRequest $request,$id)
            {
              // Check Record Exists
              $productcoupons =  ProductCoupon::find($id);
              if(is_null($productcoupons) || empty($productcoupons)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("productcoupons"));
              }
              $data = $this->updateFillableColumns(); 
              unset($data['product_id']);
                            ProductCoupon::where('id',$id)->update($data);

               if($request->product_id){
         	           Product_Coupon::where('coupon_id', $id)->delete();

         foreach($request->product_id as $value){
		  			$get_pro=Product::where('id',$value)->first();
		  		Product_Coupon::create([
		  			'product_id' => $value,
		  			'coupon_id' =>$productcoupons->id,
		  		]);
		  	}}
           
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('productcoupons'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$productcoupons = ProductCoupon::find($id);
		if(is_null($productcoupons) || empty($productcoupons)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("productcoupons"));
		}
               
		it()->delete('productcoupon',$id);
		$productcoupons->delete();
		return redirectWithSuccess(aurl("productcoupons"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$productcoupons = ProductCoupon::find($id);
				if(is_null($productcoupons) || empty($productcoupons)){
					return backWithError(trans('admin.undefinedRecord'),aurl("productcoupons"));
				}
                    	
				it()->delete('productcoupon',$id);
				$productcoupons->delete();
			}
			return redirectWithSuccess(aurl("productcoupons"),trans('admin.deleted'));
		}else {
			$productcoupons = ProductCoupon::find($data);
			if(is_null($productcoupons) || empty($productcoupons)){
				return backWithError(trans('admin.undefinedRecord'),aurl("productcoupons"));
			}
                    
			it()->delete('productcoupon',$data);
			$productcoupons->delete();
			return redirectWithSuccess(aurl("productcoupons"),trans('admin.deleted'));
		}
	}
            

}