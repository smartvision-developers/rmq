<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\CitysDataTable;
use Carbon\Carbon;
use App\Models\City;

use App\Http\Controllers\Validations\CitysRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Citys extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:citys_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:citys_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:citys_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:citys_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(CitysDataTable $citys)
            {
               return $citys->render('admin.citys.index',['title'=>trans('admin.citys')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.citys.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(CitysRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$citys = City::create($data); 
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('citys'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$citys =  City::find($id);
        		return is_null($citys) || empty($citys)?
        		backWithError(trans("admin.undefinedRecord"),aurl("citys")) :
        		view('admin.citys.show',[
				    'title'=>trans('admin.show'),
					'citys'=>$citys
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$citys =  City::find($id);
        		return is_null($citys) || empty($citys)?
        		backWithError(trans("admin.undefinedRecord"),aurl("citys")) :
        		view('admin.citys.edit',[
				  'title'=>trans('admin.edit'),
				  'citys'=>$citys
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new CitysRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(CitysRequest $request,$id)
            {
              // Check Record Exists
              $citys =  City::find($id);
              if(is_null($citys) || empty($citys)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("citys"));
              }
              $data = $this->updateFillableColumns(); 
              City::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('citys'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$citys = City::find($id);
		if(is_null($citys) || empty($citys)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("citys"));
		}
               
		it()->delete('city',$id);
		$citys->delete();
		return redirectWithSuccess(aurl("citys"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$citys = City::find($id);
				if(is_null($citys) || empty($citys)){
					return backWithError(trans('admin.undefinedRecord'),aurl("citys"));
				}
                    	
				it()->delete('city',$id);
				$citys->delete();
			}
			return redirectWithSuccess(aurl("citys"),trans('admin.deleted'));
		}else {
			$citys = City::find($data);
			if(is_null($citys) || empty($citys)){
				return backWithError(trans('admin.undefinedRecord'),aurl("citys"));
			}
                    
			it()->delete('city',$data);
			$citys->delete();
			return redirectWithSuccess(aurl("citys"),trans('admin.deleted'));
		}
	}
            

}