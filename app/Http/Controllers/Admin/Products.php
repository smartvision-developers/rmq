<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\ProductsDataTable;
use Carbon\Carbon;
use App\Models\Product;

use App\Http\Controllers\Validations\ProductsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Products extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:products_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:products_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:products_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:products_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(ProductsDataTable $products)
            {
               return $products->render('admin.products.index',['title'=>trans('admin.products')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
            $temp_id = (time()*rand(0000,9999));
            if(empty(request("temp_id")) || !request()->has("temp_id")){
					return redirect(aurl("products/create?temp_id=".$temp_id));
				}

               return view('admin.products.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(ProductsRequest $request)
            {
                $data = $request->except("_token", "_method");
            	
		// Unset or Remove Dropzone request
		$dz_path = request("dz_path");
		$dz_type = request("dz_type");
		$dz_id = request("dz_id");
		unset($data["dz_id"], $data["dz_type"], $data["dz_type"]);
		
$data['product_image'] = "";
$data['product_video'] = "";
$data['design_book'] = "";
$data['sold'] = 0;
		  		$products = Product::create($data); 
               if(request()->hasFile('product_image')){
              $products->product_image = it()->upload('product_image','products/'.$products->id);
              $products->save();
              }
               if(request()->hasFile('product_video')){
              $products->product_video = it()->upload('product_video','products/'.$products->id);
              $products->save();
              }
               if(request()->hasFile('design_book')){
              $products->design_book = it()->upload('design_book','products/'.$products->id);
              $products->save();
              }

		// rename or move files from tempfile Folder after Add record
		if ($dz_type == "create") {
			it()->rename("products", $dz_id, $products->id);
		}
		
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('products'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$products =  Product::find($id);
        		return is_null($products) || empty($products)?
        		backWithError(trans("admin.undefinedRecord"),aurl("products")) :
        		view('admin.products.show',[
				    'title'=>trans('admin.show'),
					'products'=>$products
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$products =  Product::find($id);
        		return is_null($products) || empty($products)?
        		backWithError(trans("admin.undefinedRecord"),aurl("products")) :
        		view('admin.products.edit',[
				  'title'=>trans('admin.edit'),
				  'products'=>$products
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new ProductsRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(ProductsRequest $request,$id)
            {
              // Check Record Exists
              $products =  Product::find($id);
              if(is_null($products) || empty($products)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("products"));
              }
              $data = $this->updateFillableColumns(); 
               if(request()->hasFile('product_image')){
              it()->delete($products->product_image);
              $data['product_image'] = it()->upload('product_image','products');
               } 
               if(request()->hasFile('product_video')){
              it()->delete($products->product_video);
              $data['product_video'] = it()->upload('product_video','products');
               } 
               if(request()->hasFile('design_book')){
              it()->delete($products->design_book);
              $data['design_book'] = it()->upload('design_book','products');
               } 
              Product::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('products'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$products = Product::find($id);
		if(is_null($products) || empty($products)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("products"));
		}
               		if(!empty($products->product_image)){
			it()->delete($products->product_image);		}
		if(!empty($products->product_video)){
			it()->delete($products->product_video);		}
		if(!empty($products->design_book)){
			it()->delete($products->design_book);		}

		it()->delete('product',$id);
		$products->delete();
		return redirectWithSuccess(aurl("products"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$products = Product::find($id);
				if(is_null($products) || empty($products)){
					return backWithError(trans('admin.undefinedRecord'),aurl("products"));
				}
                    					if(!empty($products->product_image)){
				  it()->delete($products->product_image);
				}				if(!empty($products->product_video)){
				  it()->delete($products->product_video);
				}				if(!empty($products->design_book)){
				  it()->delete($products->design_book);
				}
				it()->delete('product',$id);
				$products->delete();
			}
			return redirectWithSuccess(aurl("products"),trans('admin.deleted'));
		}else {
			$products = Product::find($data);
			if(is_null($products) || empty($products)){
				return backWithError(trans('admin.undefinedRecord'),aurl("products"));
			}
                    
			if(!empty($products->product_image)){
			 it()->delete($products->product_image);
			}			if(!empty($products->product_video)){
			 it()->delete($products->product_video);
			}			if(!empty($products->design_book)){
			 it()->delete($products->design_book);
			}			it()->delete('product',$data);
			$products->delete();
			return redirectWithSuccess(aurl("products"),trans('admin.deleted'));
		}
	}
            
	// Delete Files From Dropzone Library
	public function delete_file() {
		if (request("type_file") && request("type_id")) {
			if (it()->getFile(request("type_file"), request("type_id"))) {
				it()->delete(null, null, request("id"));
				return response([
					"status" => true,
				], 200);
			}
		}
	}

	// Multi upload with dropzone
	public function multi_upload() {
		if (request()->ajax()) {
			$rules = [];
			if(request()->hasFile("product_photos")){
				$rules["product_photos"] = "sometimes|nullable|file|image";
			}


			$this->validate(request(), $rules, [], [
				 "product_photos" => trans("admin.product_photos"),

			]);

			if(request()->hasFile("product_photos")){
				it()->upload("product_photos", request("dz_path"), "products", request("dz_id"));
			}

			return response([
				"status" => true,
				"type" => request("dz_type"),
				"file" => it()->getFile("products", request("dz_id")),
			], 200);
		}

	}

}