<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\CountrysDataTable;
use Carbon\Carbon;
use App\Models\Country;

use App\Http\Controllers\Validations\CountrysRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Countrys extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:countrys_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:countrys_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:countrys_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:countrys_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(CountrysDataTable $countrys)
            {
               return $countrys->render('admin.countrys.index',['title'=>trans('admin.countrys')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.countrys.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(CountrysRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$countrys = Country::create($data); 
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('countrys'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$countrys =  Country::find($id);
        		return is_null($countrys) || empty($countrys)?
        		backWithError(trans("admin.undefinedRecord"),aurl("countrys")) :
        		view('admin.countrys.show',[
				    'title'=>trans('admin.show'),
					'countrys'=>$countrys
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$countrys =  Country::find($id);
        		return is_null($countrys) || empty($countrys)?
        		backWithError(trans("admin.undefinedRecord"),aurl("countrys")) :
        		view('admin.countrys.edit',[
				  'title'=>trans('admin.edit'),
				  'countrys'=>$countrys
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new CountrysRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(CountrysRequest $request,$id)
            {
              // Check Record Exists
              $countrys =  Country::find($id);
              if(is_null($countrys) || empty($countrys)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("countrys"));
              }
              $data = $this->updateFillableColumns(); 
              Country::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('countrys'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$countrys = Country::find($id);
		if(is_null($countrys) || empty($countrys)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("countrys"));
		}
               
		it()->delete('country',$id);
		$countrys->delete();
		return redirectWithSuccess(aurl("countrys"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$countrys = Country::find($id);
				if(is_null($countrys) || empty($countrys)){
					return backWithError(trans('admin.undefinedRecord'),aurl("countrys"));
				}
                    	
				it()->delete('country',$id);
				$countrys->delete();
			}
			return redirectWithSuccess(aurl("countrys"),trans('admin.deleted'));
		}else {
			$countrys = Country::find($data);
			if(is_null($countrys) || empty($countrys)){
				return backWithError(trans('admin.undefinedRecord'),aurl("countrys"));
			}
                    
			it()->delete('country',$data);
			$countrys->delete();
			return redirectWithSuccess(aurl("countrys"),trans('admin.deleted'));
		}
	}
            

}