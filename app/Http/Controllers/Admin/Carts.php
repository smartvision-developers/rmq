<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\CartsDataTable;
use Carbon\Carbon;
use App\Models\Cart;

use App\Http\Controllers\Validations\CartsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Carts extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:carts_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:carts_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:carts_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:carts_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(CartsDataTable $carts)
            {
               return $carts->render('admin.carts.index',['title'=>trans('admin.carts')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.carts.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(CartsRequest $request)
            {
                $data = $request->except("_token", "_method");
            	$data['report_image'] = "";
		  		$carts = Cart::create($data); 
               if(request()->hasFile('report_image')){
              $carts->report_image = it()->upload('report_image','carts/'.$carts->id);
              $carts->save();
              }
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('carts'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$carts =  Cart::find($id);
        		return is_null($carts) || empty($carts)?
        		backWithError(trans("admin.undefinedRecord"),aurl("carts")) :
        		view('admin.carts.show',[
				    'title'=>trans('admin.show'),
					'carts'=>$carts
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$carts =  Cart::find($id);
        		return is_null($carts) || empty($carts)?
        		backWithError(trans("admin.undefinedRecord"),aurl("carts")) :
        		view('admin.carts.edit',[
				  'title'=>trans('admin.edit'),
				  'carts'=>$carts
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new CartsRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(CartsRequest $request,$id)
            {
              // Check Record Exists
              $carts =  Cart::find($id);
              if(is_null($carts) || empty($carts)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("carts"));
              }
              $data = $this->updateFillableColumns(); 
               if(request()->hasFile('report_image')){
              it()->delete($carts->report_image);
              $data['report_image'] = it()->upload('report_image','carts');
               } 
              Cart::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('carts'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$carts = Cart::find($id);
		if(is_null($carts) || empty($carts)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("carts"));
		}
               		if(!empty($carts->report_image)){
			it()->delete($carts->report_image);		}

		it()->delete('cart',$id);
		$carts->delete();
		return redirectWithSuccess(aurl("carts"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$carts = Cart::find($id);
				if(is_null($carts) || empty($carts)){
					return backWithError(trans('admin.undefinedRecord'),aurl("carts"));
				}
                    					if(!empty($carts->report_image)){
				  it()->delete($carts->report_image);
				}
				it()->delete('cart',$id);
				$carts->delete();
			}
			return redirectWithSuccess(aurl("carts"),trans('admin.deleted'));
		}else {
			$carts = Cart::find($data);
			if(is_null($carts) || empty($carts)){
				return backWithError(trans('admin.undefinedRecord'),aurl("carts"));
			}
                    
			if(!empty($carts->report_image)){
			 it()->delete($carts->report_image);
			}			it()->delete('cart',$data);
			$carts->delete();
			return redirectWithSuccess(aurl("carts"),trans('admin.deleted'));
		}
	}
            
	public function get_region_id() {
		if (request()->ajax()) {
			if (request("city_id") > 0) {
				$select = request("select") > 0 ? request("select") : "";
				return \Form::select("region_id",\App\Models\Region::where("city_id",request("city_id"))->pluck('region_name','id'), $select, ["class" => "form-control select2", "placeholder" => trans("admin.choose"), "id" => "region_id"]);
			}
		} else {
			return "<select class='form-control'></select>";
		}
	}



}