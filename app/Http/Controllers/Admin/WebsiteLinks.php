<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\WebsiteLinksDataTable;
use Carbon\Carbon;
use App\Models\WebsiteLink;

use App\Http\Controllers\Validations\WebsiteLinksRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class WebsiteLinks extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:websitelinks_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:websitelinks_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:websitelinks_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:websitelinks_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(WebsiteLinksDataTable $websitelinks)
            {
               return $websitelinks->render('admin.websitelinks.index',['title'=>trans('admin.websitelinks')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.websitelinks.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(WebsiteLinksRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$websitelinks = WebsiteLink::create($data); 
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('websitelinks'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$websitelinks =  WebsiteLink::find($id);
        		return is_null($websitelinks) || empty($websitelinks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("websitelinks")) :
        		view('admin.websitelinks.show',[
				    'title'=>trans('admin.show'),
					'websitelinks'=>$websitelinks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$websitelinks =  WebsiteLink::find($id);
        		return is_null($websitelinks) || empty($websitelinks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("websitelinks")) :
        		view('admin.websitelinks.edit',[
				  'title'=>trans('admin.edit'),
				  'websitelinks'=>$websitelinks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new WebsiteLinksRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(WebsiteLinksRequest $request,$id)
            {
              // Check Record Exists
              $websitelinks =  WebsiteLink::find($id);
              if(is_null($websitelinks) || empty($websitelinks)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("websitelinks"));
              }
              $data = $this->updateFillableColumns(); 
              WebsiteLink::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('websitelinks'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$websitelinks = WebsiteLink::find($id);
		if(is_null($websitelinks) || empty($websitelinks)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("websitelinks"));
		}
               
		it()->delete('websitelink',$id);
		$websitelinks->delete();
		return redirectWithSuccess(aurl("websitelinks"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$websitelinks = WebsiteLink::find($id);
				if(is_null($websitelinks) || empty($websitelinks)){
					return backWithError(trans('admin.undefinedRecord'),aurl("websitelinks"));
				}
                    	
				it()->delete('websitelink',$id);
				$websitelinks->delete();
			}
			return redirectWithSuccess(aurl("websitelinks"),trans('admin.deleted'));
		}else {
			$websitelinks = WebsiteLink::find($data);
			if(is_null($websitelinks) || empty($websitelinks)){
				return backWithError(trans('admin.undefinedRecord'),aurl("websitelinks"));
			}
                    
			it()->delete('websitelink',$data);
			$websitelinks->delete();
			return redirectWithSuccess(aurl("websitelinks"),trans('admin.deleted'));
		}
	}
            

}