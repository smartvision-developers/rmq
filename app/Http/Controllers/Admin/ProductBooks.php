<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\ProductBooksDataTable;
use Carbon\Carbon;
use App\Models\ProductBook;

use App\Http\Controllers\Validations\ProductBooksRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductBooks extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:productbooks_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:productbooks_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:productbooks_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:productbooks_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(ProductBooksDataTable $productbooks)
            {
               return $productbooks->render('admin.productbooks.index',['title'=>trans('admin.productbooks')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.productbooks.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(ProductBooksRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$productbooks = ProductBook::create($data); 

			return successResponseJson([
				"message" => trans("admin.added"),
				"data" => $productbooks,
			]);
			 }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$productbooks =  ProductBook::find($id);
        		return is_null($productbooks) || empty($productbooks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productbooks")) :
        		view('admin.productbooks.show',[
				    'title'=>trans('admin.show'),
					'productbooks'=>$productbooks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$productbooks =  ProductBook::find($id);
        		return is_null($productbooks) || empty($productbooks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productbooks")) :
        		view('admin.productbooks.edit',[
				  'title'=>trans('admin.edit'),
				  'productbooks'=>$productbooks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new ProductBooksRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(ProductBooksRequest $request,$id)
            {
              // Check Record Exists
              $productbooks =  ProductBook::find($id);
              if(is_null($productbooks) || empty($productbooks)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("productbooks"));
              }
              $data = $this->updateFillableColumns(); 
              ProductBook::where('id',$id)->update($data);

              $productbooks = ProductBook::find($id);
              return successResponseJson([
               "message" => trans("admin.updated"),
               "data" => $productbooks,
              ]);
			}

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$productbooks = ProductBook::find($id);
		if(is_null($productbooks) || empty($productbooks)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("productbooks"));
		}
               
		it()->delete('productbook',$id);
		$productbooks->delete();
		return redirectWithSuccess(aurl("productbooks"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$productbooks = ProductBook::find($id);
				if(is_null($productbooks) || empty($productbooks)){
					return backWithError(trans('admin.undefinedRecord'),aurl("productbooks"));
				}
                    	
				it()->delete('productbook',$id);
				$productbooks->delete();
			}
			return redirectWithSuccess(aurl("productbooks"),trans('admin.deleted'));
		}else {
			$productbooks = ProductBook::find($data);
			if(is_null($productbooks) || empty($productbooks)){
				return backWithError(trans('admin.undefinedRecord'),aurl("productbooks"));
			}
                    
			it()->delete('productbook',$data);
			$productbooks->delete();
			return redirectWithSuccess(aurl("productbooks"),trans('admin.deleted'));
		}
	}
            

}