<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\SocialLinksDataTable;
use Carbon\Carbon;
use App\Models\SocialLink;

use App\Http\Controllers\Validations\SocialLinksRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class SocialLinks extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:sociallinks_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:sociallinks_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:sociallinks_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:sociallinks_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(SocialLinksDataTable $sociallinks)
            {
               return $sociallinks->render('admin.sociallinks.index',['title'=>trans('admin.sociallinks')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.sociallinks.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(SocialLinksRequest $request)
            {
                $data = $request->except("_token", "_method");
            	$data['social_image'] = "";
		  		$sociallinks = SocialLink::create($data); 
               if(request()->hasFile('social_image')){
              $sociallinks->social_image = it()->upload('social_image','sociallinks/'.$sociallinks->id);
              $sociallinks->save();
              }
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('sociallinks'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$sociallinks =  SocialLink::find($id);
        		return is_null($sociallinks) || empty($sociallinks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("sociallinks")) :
        		view('admin.sociallinks.show',[
				    'title'=>trans('admin.show'),
					'sociallinks'=>$sociallinks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$sociallinks =  SocialLink::find($id);
        		return is_null($sociallinks) || empty($sociallinks)?
        		backWithError(trans("admin.undefinedRecord"),aurl("sociallinks")) :
        		view('admin.sociallinks.edit',[
				  'title'=>trans('admin.edit'),
				  'sociallinks'=>$sociallinks
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new SocialLinksRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(SocialLinksRequest $request,$id)
            {
              // Check Record Exists
              $sociallinks =  SocialLink::find($id);
              if(is_null($sociallinks) || empty($sociallinks)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("sociallinks"));
              }
              $data = $this->updateFillableColumns(); 
               if(request()->hasFile('social_image')){
              it()->delete($sociallinks->social_image);
              $data['social_image'] = it()->upload('social_image','sociallinks');
               } 
              SocialLink::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('sociallinks'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$sociallinks = SocialLink::find($id);
		if(is_null($sociallinks) || empty($sociallinks)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("sociallinks"));
		}
               		if(!empty($sociallinks->social_image)){
			it()->delete($sociallinks->social_image);		}

		it()->delete('sociallink',$id);
		$sociallinks->delete();
		return redirectWithSuccess(aurl("sociallinks"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$sociallinks = SocialLink::find($id);
				if(is_null($sociallinks) || empty($sociallinks)){
					return backWithError(trans('admin.undefinedRecord'),aurl("sociallinks"));
				}
                    					if(!empty($sociallinks->social_image)){
				  it()->delete($sociallinks->social_image);
				}
				it()->delete('sociallink',$id);
				$sociallinks->delete();
			}
			return redirectWithSuccess(aurl("sociallinks"),trans('admin.deleted'));
		}else {
			$sociallinks = SocialLink::find($data);
			if(is_null($sociallinks) || empty($sociallinks)){
				return backWithError(trans('admin.undefinedRecord'),aurl("sociallinks"));
			}
                    
			if(!empty($sociallinks->social_image)){
			 it()->delete($sociallinks->social_image);
			}			it()->delete('sociallink',$data);
			$sociallinks->delete();
			return redirectWithSuccess(aurl("sociallinks"),trans('admin.deleted'));
		}
	}
            

}