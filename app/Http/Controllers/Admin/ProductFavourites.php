<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\ProductFavouritesDataTable;
use Carbon\Carbon;
use App\Models\ProductFavourite;

use App\Http\Controllers\Validations\ProductFavouritesRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductFavourites extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:productfavourites_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:productfavourites_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:productfavourites_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:productfavourites_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(ProductFavouritesDataTable $productfavourites)
            {
               return $productfavourites->render('admin.productfavourites.index',['title'=>trans('admin.productfavourites')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.productfavourites.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(ProductFavouritesRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$productfavourites = ProductFavourite::create($data); 
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('productfavourites'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$productfavourites =  ProductFavourite::find($id);
        		return is_null($productfavourites) || empty($productfavourites)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productfavourites")) :
        		view('admin.productfavourites.show',[
				    'title'=>trans('admin.show'),
					'productfavourites'=>$productfavourites
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$productfavourites =  ProductFavourite::find($id);
        		return is_null($productfavourites) || empty($productfavourites)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productfavourites")) :
        		view('admin.productfavourites.edit',[
				  'title'=>trans('admin.edit'),
				  'productfavourites'=>$productfavourites
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new ProductFavouritesRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(ProductFavouritesRequest $request,$id)
            {
              // Check Record Exists
              $productfavourites =  ProductFavourite::find($id);
              if(is_null($productfavourites) || empty($productfavourites)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("productfavourites"));
              }
              $data = $this->updateFillableColumns(); 
              ProductFavourite::where('id',$id)->update($data);
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('productfavourites'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$productfavourites = ProductFavourite::find($id);
		if(is_null($productfavourites) || empty($productfavourites)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("productfavourites"));
		}
               
		it()->delete('productfavourite',$id);
		$productfavourites->delete();
		return redirectWithSuccess(aurl("productfavourites"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$productfavourites = ProductFavourite::find($id);
				if(is_null($productfavourites) || empty($productfavourites)){
					return backWithError(trans('admin.undefinedRecord'),aurl("productfavourites"));
				}
                    	
				it()->delete('productfavourite',$id);
				$productfavourites->delete();
			}
			return redirectWithSuccess(aurl("productfavourites"),trans('admin.deleted'));
		}else {
			$productfavourites = ProductFavourite::find($data);
			if(is_null($productfavourites) || empty($productfavourites)){
				return backWithError(trans('admin.undefinedRecord'),aurl("productfavourites"));
			}
                    
			it()->delete('productfavourite',$data);
			$productfavourites->delete();
			return redirectWithSuccess(aurl("productfavourites"),trans('admin.deleted'));
		}
	}
            

}