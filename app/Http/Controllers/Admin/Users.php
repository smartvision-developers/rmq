<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\UsersDataTable;
use Carbon\Carbon;
use App\Models\User;

use App\Http\Controllers\Validations\UsersRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Users extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:users_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:users_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:users_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:users_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(UsersDataTable $users)
            {
               return $users->render('admin.users.index',['title'=>trans('admin.users')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.users.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(UsersRequest $request)
            {
                $data = $request->except("_token", "_method");
            			$users = User::create($data); 
            	  		$users->password=bcrypt($request->password);
							$users->mobile=preg_replace('/^0/i','',request('mobile'));
							$users->code=mt_rand(1111,99999);
							$users->save();
                $redirect = isset($request["add_back"])?"/create":"";
                return redirectWithSuccess(aurl('users'.$redirect), trans('admin.added')); }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$users =  User::with('favourite_id','cart_id')->find($id);
        		$users->setRelation('favourite_id', $users->favourite_id()->paginate(10));
        		$users->setRelation('cart_id', $users->cart_id()->paginate(10));
        		return is_null($users) || empty($users)?
        		backWithError(trans("admin.undefinedRecord"),aurl("users")) :
        		view('admin.users.show',[
				    'title'=>trans('admin.show'),
					'users'=>$users
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$users =  User::find($id);
        		return is_null($users) || empty($users)?
        		backWithError(trans("admin.undefinedRecord"),aurl("users")) :
        		view('admin.users.edit',[
				  'title'=>trans('admin.edit'),
				  'users'=>$users
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new UsersRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(UsersRequest $request,$id)
            {
              // Check Record Exists
              $users =  User::find($id);
              if(is_null($users) || empty($users)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("users"));
              }
              $data = $this->updateFillableColumns(); 
              User::where('id',$id)->update($data);
              $users->mobile=preg_replace('/^0/i','',request('mobile'));
              if ($request->input('password')) {
						$users->password = bcrypt($request->input('password'));
					}
					$users->save();
              $redirect = isset($request["save_back"])?"/".$id."/edit":"";
              return redirectWithSuccess(aurl('users'.$redirect), trans('admin.updated'));
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$users = User::find($id);
		if(is_null($users) || empty($users)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("users"));
		}
               
		it()->delete('user',$id);
		$users->delete();
		return redirectWithSuccess(aurl("users"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$users = User::find($id);
				if(is_null($users) || empty($users)){
					return backWithError(trans('admin.undefinedRecord'),aurl("users"));
				}
                    	
				it()->delete('user',$id);
				$users->delete();
			}
			return redirectWithSuccess(aurl("users"),trans('admin.deleted'));
		}else {
			$users = User::find($data);
			if(is_null($users) || empty($users)){
				return backWithError(trans('admin.undefinedRecord'),aurl("users"));
			}
                    
			it()->delete('user',$data);
			$users->delete();
			return redirectWithSuccess(aurl("users"),trans('admin.deleted'));
		}
	}
            
	public function get_region_id() {
		if (request()->ajax()) {
			if (request("city_id") > 0) {
				$select = request("select") > 0 ? request("select") : "";
				return \Form::select("region_id",\App\Models\Region::where("city_id",request("city_id"))->pluck('region_name','id'), $select, ["class" => "form-control select2", "placeholder" => trans("admin.choose"), "id" => "region_id"]);
			}
		} else {
			return "<select class='form-control'></select>";
		}
	}



}