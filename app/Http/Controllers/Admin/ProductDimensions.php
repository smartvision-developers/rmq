<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\ProductDimensionsDataTable;
use Carbon\Carbon;
use App\Models\ProductDimension;

use App\Http\Controllers\Validations\ProductDimensionsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductDimensions extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:productdimensions_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:productdimensions_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:productdimensions_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:productdimensions_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(ProductDimensionsDataTable $productdimensions)
            {
               return $productdimensions->render('admin.productdimensions.index',['title'=>trans('admin.productdimensions')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.productdimensions.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(ProductDimensionsRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$productdimensions = ProductDimension::create($data); 
            			  		if($request->south == null){
            			  			$productdimensions->south= $productdimensions->north;
            			  			$productdimensions->save();
            			  		}
            			  		if($request->west == null){
            			  			$productdimensions->west= $productdimensions->east;
            			  			$productdimensions->save();
            			  		}
			return successResponseJson([
				"message" => trans("admin.added"),
				"data" => $productdimensions,
			]);
			 }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$productdimensions =  ProductDimension::find($id);
        		return is_null($productdimensions) || empty($productdimensions)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productdimensions")) :
        		view('admin.productdimensions.show',[
				    'title'=>trans('admin.show'),
					'productdimensions'=>$productdimensions
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$productdimensions =  ProductDimension::find($id);
        		return is_null($productdimensions) || empty($productdimensions)?
        		backWithError(trans("admin.undefinedRecord"),aurl("productdimensions")) :
        		view('admin.productdimensions.edit',[
				  'title'=>trans('admin.edit'),
				  'productdimensions'=>$productdimensions
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new ProductDimensionsRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(ProductDimensionsRequest $request,$id)
            {
              // Check Record Exists
              $productdimensions =  ProductDimension::find($id);
              if(is_null($productdimensions) || empty($productdimensions)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("productdimensions"));
              }
              $data = $this->updateFillableColumns(); 
              ProductDimension::where('id',$id)->update($data);
									if($request->south == null){
            			  			$productdimensions->south= $productdimensions->north;
            			  			$productdimensions->save();
            			  		}
            			  		if($request->west == null){
            			  			$productdimensions->west= $productdimensions->east;
            			  			$productdimensions->save();
            			  		}
              $productdimensions = ProductDimension::find($id);
              return successResponseJson([
               "message" => trans("admin.updated"),
               "data" => $productdimensions,
              ]);
			}

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$productdimensions = ProductDimension::find($id);
		if(is_null($productdimensions) || empty($productdimensions)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("productdimensions"));
		}
               
		it()->delete('productdimension',$id);
		$productdimensions->delete();
		return redirectWithSuccess(aurl("productdimensions"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$productdimensions = ProductDimension::find($id);
				if(is_null($productdimensions) || empty($productdimensions)){
					return backWithError(trans('admin.undefinedRecord'),aurl("productdimensions"));
				}
                    	
				it()->delete('productdimension',$id);
				$productdimensions->delete();
			}
			return redirectWithSuccess(aurl("productdimensions"),trans('admin.deleted'));
		}else {
			$productdimensions = ProductDimension::find($data);
			if(is_null($productdimensions) || empty($productdimensions)){
				return backWithError(trans('admin.undefinedRecord'),aurl("productdimensions"));
			}
                    
			it()->delete('productdimension',$data);
			$productdimensions->delete();
			return redirectWithSuccess(aurl("productdimensions"),trans('admin.deleted'));
		}
	}
            

}