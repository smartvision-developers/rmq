<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\DataTables\MostPopularsDataTable;
use Carbon\Carbon;
use App\Models\MostPopular;

use App\Http\Controllers\Validations\MostPopularsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class MostPopulars extends Controller
{

	public function __construct() {

		$this->middleware('AdminRole:mostpopulars_show', [
			'only' => ['index', 'show'],
		]);
		$this->middleware('AdminRole:mostpopulars_add', [
			'only' => ['create', 'store'],
		]);
		$this->middleware('AdminRole:mostpopulars_edit', [
			'only' => ['edit', 'update'],
		]);
		$this->middleware('AdminRole:mostpopulars_delete', [
			'only' => ['destroy', 'multi_delete'],
		]);
	}

	

            /**
             * Baboon Script By [it v 1.6.37]
             * Display a listing of the resource.
             * @return \Illuminate\Http\Response
             */
            public function index(MostPopularsDataTable $mostpopulars)
            {
               return $mostpopulars->render('admin.mostpopulars.index',['title'=>trans('admin.mostpopulars')]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * Show the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function create()
            {
            	
               return view('admin.mostpopulars.create',['title'=>trans('admin.create')]);
            }

            /**
             * Baboon Script By [it v 1.6.37]
             * Store a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response Or Redirect
             */
            public function store(MostPopularsRequest $request)
            {
                $data = $request->except("_token", "_method");
            			  		$mostpopulars = MostPopular::create($data); 

			return successResponseJson([
				"message" => trans("admin.added"),
				"data" => $mostpopulars,
			]);
			 }

            /**
             * Display the specified resource.
             * Baboon Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
        		$mostpopulars =  MostPopular::find($id);
        		return is_null($mostpopulars) || empty($mostpopulars)?
        		backWithError(trans("admin.undefinedRecord"),aurl("mostpopulars")) :
        		view('admin.mostpopulars.show',[
				    'title'=>trans('admin.show'),
					'mostpopulars'=>$mostpopulars
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * edit the form for creating a new resource.
             * @return \Illuminate\Http\Response
             */
            public function edit($id)
            {
        		$mostpopulars =  MostPopular::find($id);
        		return is_null($mostpopulars) || empty($mostpopulars)?
        		backWithError(trans("admin.undefinedRecord"),aurl("mostpopulars")) :
        		view('admin.mostpopulars.edit',[
				  'title'=>trans('admin.edit'),
				  'mostpopulars'=>$mostpopulars
        		]);
            }


            /**
             * Baboon Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @param  \Illuminate\Http\Request  $request
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				$fillableCols = [];
				foreach (array_keys((new MostPopularsRequest)->attributes()) as $fillableUpdate) {
					if (!is_null(request($fillableUpdate))) {
						$fillableCols[$fillableUpdate] = request($fillableUpdate);
					}
				}
				return $fillableCols;
			}

            public function update(MostPopularsRequest $request,$id)
            {
              // Check Record Exists
              $mostpopulars =  MostPopular::find($id);
              if(is_null($mostpopulars) || empty($mostpopulars)){
              	return backWithError(trans("admin.undefinedRecord"),aurl("mostpopulars"));
              }
              $data = $this->updateFillableColumns(); 
              MostPopular::where('id',$id)->update($data);

              $mostpopulars = MostPopular::find($id);
              return successResponseJson([
               "message" => trans("admin.updated"),
               "data" => $mostpopulars,
              ]);
			}

            /**
             * Baboon Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @param  $id
             * @return \Illuminate\Http\Response
             */
	public function destroy($id){
		$mostpopulars = MostPopular::find($id);
		if(is_null($mostpopulars) || empty($mostpopulars)){
			return backWithSuccess(trans('admin.undefinedRecord'),aurl("mostpopulars"));
		}
               
		it()->delete('mostpopular',$id);
		$mostpopulars->delete();
		return redirectWithSuccess(aurl("mostpopulars"),trans('admin.deleted'));
	}


	public function multi_delete(){
		$data = request('selected_data');
		if(is_array($data)){
			foreach($data as $id){
				$mostpopulars = MostPopular::find($id);
				if(is_null($mostpopulars) || empty($mostpopulars)){
					return backWithError(trans('admin.undefinedRecord'),aurl("mostpopulars"));
				}
                    	
				it()->delete('mostpopular',$id);
				$mostpopulars->delete();
			}
			return redirectWithSuccess(aurl("mostpopulars"),trans('admin.deleted'));
		}else {
			$mostpopulars = MostPopular::find($data);
			if(is_null($mostpopulars) || empty($mostpopulars)){
				return backWithError(trans('admin.undefinedRecord'),aurl("mostpopulars"));
			}
                    
			it()->delete('mostpopular',$data);
			$mostpopulars->delete();
			return redirectWithSuccess(aurl("mostpopulars"),trans('admin.deleted'));
		}
	}
            

}