<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Files;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Response;
use Session;
use Validator;

class AboutController extends Controller {
	
	public function index() {
		$about=About::where('status','show')->orderBy('id','DESC')->take(4)->get();
		
		$about_files=Files::where('type_id', 1)->where('type_file','settings')->take(15)->get();
		return view('style.about',compact('about','about_files'));
	}



}
