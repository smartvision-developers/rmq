<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Files;
use Auth;
use App\Models\MostPopular;
use App\Models\Statistic;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Response;
use Session;
use Validator;

class HomeController extends Controller {
	
	public function index() {
		$sliders=Slider::where('status','show')->orderBy('id','DESC')->take(6)->get();

		$about_files=Files::where('type_id', 1)->where('type_file','settings')->take(15)->get();
		$products=Product::where('status','show')->whereRaw('sold < stock')->orderBy('id','DESC')->paginate(6);
		$most_popular=MostPopular::where('status','show')->orderBy('id','DESC')->limit(10)->get();
		$statistics=Statistic::orderBy('id','DESC')->take(4)->get();
		return view('style.home', compact('sliders','about_files','products','most_popular','statistics'));
	}

	public function contact(){
		return view('style.contact');
	}

	public function storeContact(Request $request){
		$rules = [
			'name' => 'required',
			'title' => 'required',
			'email' => 'required|email',
			'message' => 'required',
		];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return response()->json(array(
				'errors' => $validator->errors()->all(),
			));
		} else {
		$data = $request->except("_token", "_method");
		$user_store = Contact::create($data);
		if($user_store){
			return 1; 			
		}
        return 2;
    }
	}

public function search(Request $request){
		 $content=$request->get('content');
        if($request->has('content')){
        $products=Product::where('status','show')->where('product_name','LIKE', '%' .$content. '%' )->orwhere('design_by','LIKE', '%' .$content. '%' )->orwhere('product_description','LIKE', '%' .$content. '%' )->orwhere('product_content','LIKE', '%' .$content. '%' )->paginate(6);  
		return view('style.search', compact('products','content'));

            }
	}

}
