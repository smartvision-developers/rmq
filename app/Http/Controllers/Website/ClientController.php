<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Region;
use App\Models\ProductDimension;
use App\Models\Product;
use App\Models\Cart;
use App\Models\ProductCoupon;
use Mail;
use App\Http\Controllers\ValidationsFront\UserCartsRequest;
use Response;
use Session;
use Validator;
use DB;
use Carbon\Carbon;
class ClientController extends Controller {
	
	public function register() {
		$citys=City::limit(30)->get();
		$regions=Region::limit(30)->get();
		return view('style.register',compact('citys','regions'));
	}
	public function get_ajax_region($id)
    {
                $regions = DB::table("regions")
                        ->where("city_id",$id)
                        ->pluck("region_name","id");
            return json_encode($regions);

    }

	public function store(Request $request) {
		$rules = [
			'first_name' => 'required',
			'last_name' => 'required',
			'mobile' => 'required|numeric|regex:/[0-9]{7}/|digits_between:10,14|unique:users,mobile',
			'password' => 'required|min:6|unique:users,password',
			'email' => 'required|email|unique:users,email',
			'city_id' => 'required|not_in:0',
			'region_id' => 'required',

		];
			$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return response()->json(array(

				'errors' => $validator->errors()->all(),
			));
		} else {
		$data = $request->except("_token", "_method");
		$user_store = User::create($data);
		$user_store->password=bcrypt($request->password);
		$user_store->status='accepted';
		$user_store->code=rand(0, 99999);
		$user_store->country_code='966';
		$user_store->save();
		if (Auth::attempt(['mobile' => $request->mobile, 'password' => $request->password])) 
			{
				$user = auth()->user();
				return 1; 
			}

        return 2;
    }
	}


	public function clientLogin(Request $request){
		$rules = [
			'password' => 'required',
			'email' => 'required|email',
		];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return response()->json(array(

				'errors' => $validator->errors()->all(),
			));
		} else {
			$user=User::where('email',$request->email)->first();
			$rememberme = request('remember_me') == 1 ? true : false;
			if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $rememberme)) 
			{
			    
			    if($user->status == 'accepted') {
					$user = Auth::user();
    				//dd($user);
    				return 1;
				}
				elseif($user->status == 'rejected'){
				    
					auth()->logout();
					return 2;
				}
				elseif($user->status == 'pending'){
					auth()->logout();
					return 2;
				}
				
				
				
			}	
			else
			{
				return Redirect()->back()->withErrorFlashMessage($user->decline_reason); 
        }   }
	}


	public function userLogout() {
		if(Auth::check()){
			auth()->logout();
		}
		return redirect('/');
	}

    public function get_profile()
    {
    	$regions=Region::limit(40)->get();
    	$citys=City::limit(40)->get();

        return view('style.profile', compact('regions','citys'));
    }

 	public function change_password()
    {
        return view('style.change-password');
    }

    public function update_password(Request $request)
    {
    
        $data = User::where('id',Auth::user()->id)->first();
        if(Hash::check($request->password ,$data->password))
        {
        	$this->validate($request, [
    			'new_password' => 'required|required_with:confirm_password|same:confirm_password|min:6',
			], [
				'new_password.required' =>'required',
				'new_password.min' =>'min',
				'new_password.required_with' =>'required_with',
				'new_password.same' =>'same',
			]);
             $data->update([
          'password' => bcrypt($request->new_password),
        ]);
           
          return Redirect()->back()->withFlashMessage(trans('main.edit-done')); 
          auth()->logout();
        }
        else
        {
         return Redirect()->back()->withErrorFlashMessage(trans('main.error'));

        }

    }


    
  public function update_profile(Request $request)
    {
    	$data = User::where('id',Auth::user()->id)->first();
          $rules = [
			'first_name' 	=> 'required',
			'last_name' 	=> 'required',
			'mobile' 		=> 'required|numeric|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,mobile,'.$data->id,
			'email' 		=> 'required|email|unique:users,email,'.$data->id,
			'city_id' 		=> 'required',
			'region_id' 	=> 'required',

		];
			$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return response()->json(array(

				'errors' => $validator->errors()->all(),
			));
		} else {
	
         $data->first_name      = isset($request->first_name) ? $request->first_name : $data->first_name;
         $data->last_name       = isset($request->last_name) ? $request->last_name : $data->last_name;
         $data->mobile      	= isset($request->mobile) ? $request->mobile : $data->mobile;
         $data->email      		= isset($request->email) ? $request->email : $data->email;
         $data->city_id      	= isset($request->city_id) ? $request->city_id : $data->city_id;
         $data->region_id       = isset($request->region_id) ? $request->region_id : $data->region_id;
         $data->update();
         if($data){
			return 1; 
		}else{
			return 2;
    		}
		}
    }

	public function forget_password() {
		return view('style.forget_password');
	}

   	public function check_code(Request $request, $email) {
		$data = User::where('email', $email)->first();
		//  dd($data);
		return view('style.new_password', compact('data'));
	}
		public function get_password(Request $request) {

		$data = $this->validate($request, [
			'email' => 'required|email',
		]);
		$data = User::where('email', $request->email)->first();
		// $data->spare_email= $request->email;
		//$data->save();
		if (!$data) {
			return back()->withErrorFlashMessage(trans('main.email-not-register'));
		}
		$email = $data->email;
		Mail::send('style.reset-password', ['email' => $email, 'code' => $data->code, 'id' => $data->id, 'link' => route('site.checkCode', $email)], function ($message) use ($email) {
			$message->to($email);
			$message->subject('Reset Password Notification');

		});
//dd($defdf);
		session()->put('user_signup', $email);
		return back()->withFlashMessage(trans('main.check-email'));
	}

	public function checking(Request $request) {
		$data = User::where('email', $request->email)->first();
		if ($data->forget_password == 0) {
			$x = [];
			foreach ($request->code as $val) {
				array_push($x, $val);

			}
			$arr = array_reverse($x);
			$user_code = implode("", $arr);
			//dd($user_code);
			if ($data->code == $user_code) {
				$data->forget_password = 1;
				$data->save();
				return back();
			}
			return back()->withFlashMessage(trans('main.check-code'));
		} else {
			$validator = Validator::make($request->all(), [
				'new_password' => 'required|min:6|unique:users,new_password|required_with:confirm_password|same:confirm_password',
				'confirm_password' => 'required|min:6',
			 //]
			 //,
			// 	[
			// 		'new_password.required' => __('translate.password-required'),
			// 		'confirm_password.required' => __('translate.repassword-required'),
			// 		'new_password.min' => __('translate.password-min'),
			// 		'confirm_password.min' => __('translate.repassword-min'),
			 	]);

			$data->forget_password = 0;
			if ($request->confirm_password == $request->new_password) {
				$data->password = bcrypt($request->new_password);
			}
			$data->code = rand(0, 99999);
			$data->save();
			return redirect('/')->withFlashMessage(trans('main.pass-done'));
		}
	}

   public function add_product_cart(Request $request)
    {
        $cart = Cart::where('product_id', $request->product_id)->where('client_id', Auth::user()->id)->first();
        if (!$cart) {
        	if($request->east == null || $request->north == null )
        	{
        		return 3;
        	}
            $product= ProductDimension::where('product_id', $request->product_id)->where('east', $request->east )->where('north', $request->north )->first();
         //dd($product);
            if($product){
               Cart::create([
	            'client_id' => Auth::user()->id,
	            'product_id'  => $request->product_id,
	            'east' => $request->east,
	            'north' => $request->north,
	            'product_name' => $product->product_id()->first()->product_name,
	            'product_price' => $product->product_id()->first()->price,
	            'vat' => setting()->vat,
	            'total_price' =>ceil( $product->product_id()->first()->price + ($product->product_id()->first()->price * setting()->vat )/ 100),
                     ]);
            
            	return 1;
	        }
	        else{
	        	return 2;
	        }
        }
        else
        {
            return 0;
        }
    }

	public function get_checkout(Request $request)
	{
		$citys=City::limit(30)->get();
		$regions=Region::limit(30)->get();
		$design='';
		if($request->design != null){
			$design= $request->design;
			$cart = Cart::where('product_id', $design)->where('client_id', Auth::user()->id)->first();
			return view('style.checkout', compact('design','cart','citys','regions'));
		}

		return back();
	}

	public function pay_product(UserCartsRequest $request){
		$design='';
		if($request->design != null){
			$design= $request->design;
			$cart = Cart::where('product_id', $design)->where('client_id', Auth::user()->id)->first();
		}
		$data = $request->except("_token", "_method","design","purchase_agreement");
		// $rules = [
		// 	'purchase_agreement' => 'accepted',
		// ];

		// $data = $this->validate(request(), $rules, [
		// 	'purchase_agreement' => trans('admin.purchase_agreement'),
		// ]);
	//	dd($data);

		// if (request()->hasFile('logo')) {
		// 	$data['logo'] = it()->upload('logo', 'setting');
		// }
	
		Cart::where('product_id',$design)->where('client_id', Auth::user()->id)->update($data);
		return redirect()->back();
	}

	public function get_ajax_dimension($id, $north)
    {
    	$product=Product::findOrFail($id);
        $product_dimensions = DB::table("product_dimensions")
                        ->where("product_id",$id)->where('north', $north)
                        ->pluck("south","id");
        return json_encode($product_dimensions);
    }

	public function get_ajax_west_dimension($id, $east)
    {
    	$product=Product::findOrFail($id);
        $product_dimensions = DB::table("product_dimensions")
                        ->where("product_id",$id)->where('east', $east)
                        ->pluck("west","id");
        return json_encode($product_dimensions);
    }


    public function apply_coupon(Request $request){
    	$date = Carbon::now();
        $current_date=$date->toDateString();
    	$design='';
		if($request->design != null){
			$design= $request->design;
			$cart = Cart::where('product_id', $design)->where('client_id', Auth::user()->id)->first();
		}
		$coupon=ProductCoupon::where('coupon_code', $request->coupon_code)->where('end_date', '>=', $current_date)->first();
		if($coupon){
			if($coupon->discount_type == 'percent')
			{
				$price_after_coupon= $cart->total_price - ($cart->total_price * $coupon->coupon_discount/ 100);
			}elseif($coupon->discount_type == 'sar')
			{
				$price_after_coupon= $cart->total_price - $coupon->coupon_discount;
			}
		Cart::where('product_id',$design)->where('client_id', Auth::user()->id)->update([
			'coupon_id' => $coupon->id,
			'price_after_coupon' => $price_after_coupon,
		]);
			return redirect()->back()->withFlashMessage(trans('main.coupon-done'));		
		}
		else{
			return redirect()->back()->withErrorFlashMessage(trans('main.coupon-notfound'));
		}
    }
}