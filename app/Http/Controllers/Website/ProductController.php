<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Files;
use App\Models\ProductFavourite;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Response;
use Session;
use Validator;
use DB;
class ProductController extends Controller {
	public function products(Request $request){
		$products=Product::where('status','show')->whereRaw('sold < stock')->orderBy('id','DESC')->paginate(3);
//		dd($products->currentPage());
		return view('style.products',compact('products'));
	}

	public function product($id){
       if (Session::get('id') !== $id) {
            Product::where('id', $id)->increment('view_count');
            Session::put('id', $id);
        }
		$product=Product::with('product_book')->with('product_dimension')->where('id',$id)->first();

		if(!$product){
			return Redirect('/')->withErrorFlashMessage(trans('main.address-not-found'));
		}
		$product_files=Files::where('type_id', $id)->where('type_file','products')->take(15)->get();
		return view('style.product', compact('product','product_files'));
	}


    public function add_product_fav(Request $request)
    {
        $product_fav = ProductFavourite::where('product_id', $request->product_id)->where('user_id', Auth::user()->id)->first();
            if (!$product_fav) {
               ProductFavourite::create([
            'user_id' => Auth::user()->id,
            'product_id'  => $request->product_id,
                     ]);
            
            return 1;
        }
        else
        {
            $product_fav->delete();

            return 0;
        }
    }

    public function get_favs()
    {
    	$favs= ProductFavourite::orderBy('id','DESC')->where('user_id', Auth::user()->id)->paginate(15);

    	return view('style.favorite', compact('favs'));
    }

}