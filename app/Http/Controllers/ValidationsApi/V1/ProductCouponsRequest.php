<?php
namespace App\Http\Controllers\ValidationsApi\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductCouponsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'product_id'=>'required|integer|exists:products,id',
             'coupon_name'=>'',
             'coupon_code'=>'',
             'start_date'=>'',
             'end_date'=>'',
             'coupon_discount'=>'',
             'discount_type'=>'',
		];
	}


	protected function onUpdate() {
		return [
             'product_id'=>'required|integer|exists:products,id',
             'coupon_name'=>'',
             'coupon_code'=>'',
             'start_date'=>'',
             'end_date'=>'',
             'coupon_discount'=>'',
             'discount_type'=>'',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_id'=>trans('admin.product_id'),
             'coupon_name'=>trans('admin.coupon_name'),
             'coupon_code'=>trans('admin.coupon_code'),
             'start_date'=>trans('admin.start_date'),
             'end_date'=>trans('admin.end_date'),
             'coupon_discount'=>trans('admin.coupon_discount'),
             'discount_type'=>trans('admin.discount_type'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}