<?php
namespace App\Http\Controllers\ValidationsApi\V1;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartsRequest extends FormRequest {

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation rules that apply to the request.
	 *
	 * @return array (onCreate,onUpdate,rules) methods
	 */
	protected function onCreate() {
		return [
             'product_name'=>'required|string',
             'product_price'=>'required|numeric',
             'product_id'=>'required|integer|exists:products,id',
             'client_id'=>'required|integer|exists:users,id',
             'client_name'=>'required|string',
             'client_mobile'=>'required|string',
             'client_email'=>'required|email',
             'client_id_card'=>'required|string',
             'client_job'=>'required|string',
             'report_image'=>'required|file|image',
             'city_id'=>'required|integer|exists:cities,id',
             'region_id'=>'required|integer|exists:regions,id',
             'vat'=>'sometimes|nullable|integer',
             'total_price'=>'sometimes|nullable|numeric',
		];
	}


	protected function onUpdate() {
		return [
             'product_name'=>'required|string',
             'product_price'=>'required|numeric',
             'product_id'=>'required|integer|exists:products,id',
             'client_id'=>'required|integer|exists:users,id',
             'client_name'=>'required|string',
             'client_mobile'=>'required|string',
             'client_email'=>'required|email',
             'client_id_card'=>'required|string',
             'client_job'=>'required|string',
             'report_image'=>'required|file|image',
             'city_id'=>'required|integer|exists:cities,id',
             'region_id'=>'required|integer|exists:regions,id',
             'vat'=>'sometimes|nullable|integer',
             'total_price'=>'sometimes|nullable|numeric',
		];
	}

	public function rules() {
		return request()->isMethod('put') || request()->isMethod('patch') ?
		$this->onUpdate() : $this->onCreate();
	}


	/**
	 * Baboon Script By [it v 1.6.37]
	 * Get the validation attributes that apply to the request.
	 *
	 * @return array
	 */
	public function attributes() {
		return [
             'product_name'=>trans('admin.product_name'),
             'product_price'=>trans('admin.product_price'),
             'product_id'=>trans('admin.product_id'),
             'client_id'=>trans('admin.client_id'),
             'client_name'=>trans('admin.client_name'),
             'client_mobile'=>trans('admin.client_mobile'),
             'client_email'=>trans('admin.client_email'),
             'client_id_card'=>trans('admin.client_id_card'),
             'client_job'=>trans('admin.client_job'),
             'report_image'=>trans('admin.report_image'),
             'city_id'=>trans('admin.city_id'),
             'region_id'=>trans('admin.region_id'),
             'vat'=>trans('admin.vat'),
             'total_price'=>trans('admin.total_price'),
		];
	}

	/**
	 * Baboon Script By [it v 1.6.37]
	 * response redirect if fails or failed request
	 *
	 * @return redirect
	 */
	public function response(array $errors) {
		return $this->ajax() || $this->wantsJson() ?
		response([
			'status' => false,
			'StatusCode' => 422,
			'StatusType' => 'Unprocessable',
			'errors' => $errors,
		], 422) :
		back()->withErrors($errors)->withInput(); // Redirect back
	}



}