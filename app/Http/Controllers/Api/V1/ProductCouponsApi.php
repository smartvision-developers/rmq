<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\ProductCoupon;
use Validator;
use App\Http\Controllers\ValidationsApi\V1\ProductCouponsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductCouponsApi extends Controller{
	protected $selectColumns = [
		"id",
		"product_id",
		"coupon_name",
		"coupon_code",
		"start_date",
		"end_date",
		"coupon_discount",
		"discount_type",
	];

            /**
             * Display the specified releationshop.
             * Baboon Api Script By [it v 1.6.37]
             * @return array to assign with index & show methods
             */
            public function arrWith(){
               return ['product_id',];
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Display a listing of the resource. Api
             * @return \Illuminate\Http\Response
             */
            public function index()
            {
            	$ProductCoupon = ProductCoupon::select($this->selectColumns)->with($this->arrWith())->orderBy("id","desc")->paginate(15);
               return successResponseJson(["data"=>$ProductCoupon]);
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Store a newly created resource in storage. Api
             * @return \Illuminate\Http\Response
             */
    public function store(ProductCouponsRequest $request)
    {
    	$data = $request->except("_token");
    	
        $ProductCoupon = ProductCoupon::create($data); 

		  $ProductCoupon = ProductCoupon::with($this->arrWith())->find($ProductCoupon->id,$this->selectColumns);
        return successResponseJson([
            "message"=>trans("admin.added"),
            "data"=>$ProductCoupon
        ]);
    }


            /**
             * Display the specified resource.
             * Baboon Api Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
                $ProductCoupon = ProductCoupon::with($this->arrWith())->find($id,$this->selectColumns);
            	if(is_null($ProductCoupon) || empty($ProductCoupon)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}

                 return successResponseJson([
              "data"=> $ProductCoupon
              ]);  ;
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				       $fillableCols = [];
				       foreach (array_keys((new ProductCouponsRequest)->attributes()) as $fillableUpdate) {
  				        if (!is_null(request($fillableUpdate))) {
						  $fillableCols[$fillableUpdate] = request($fillableUpdate);
						}
				       }
  				     return $fillableCols;
  	     		}

            public function update(ProductCouponsRequest $request,$id)
            {
            	$ProductCoupon = ProductCoupon::find($id);
            	if(is_null($ProductCoupon) || empty($ProductCoupon)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
  			       }

            	$data = $this->updateFillableColumns();
                 
              ProductCoupon::where("id",$id)->update($data);

              $ProductCoupon = ProductCoupon::with($this->arrWith())->find($id,$this->selectColumns);
              return successResponseJson([
               "message"=>trans("admin.updated"),
               "data"=> $ProductCoupon
               ]);
            }

            /**
             * Baboon Api Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function destroy($id)
            {
               $productcoupons = ProductCoupon::find($id);
            	if(is_null($productcoupons) || empty($productcoupons)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}


               it()->delete("productcoupon",$id);

               $productcoupons->delete();
               return successResponseJson([
                "message"=>trans("admin.deleted")
               ]);
            }



 			public function multi_delete()
            {
                $data = request("selected_data");
                if(is_array($data)){
                    foreach($data as $id){
                    $productcoupons = ProductCoupon::find($id);
	            	if(is_null($productcoupons) || empty($productcoupons)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}

                    	it()->delete("productcoupon",$id);
                    	$productcoupons->delete();
                    }
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }else {
                    $productcoupons = ProductCoupon::find($data);
	            	if(is_null($productcoupons) || empty($productcoupons)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}
 
                    	it()->delete("productcoupon",$data);

                    $productcoupons->delete();
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }
            }

            
}