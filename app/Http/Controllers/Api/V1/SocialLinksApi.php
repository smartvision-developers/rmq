<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\SocialLink;
use Validator;
use App\Http\Controllers\ValidationsApi\V1\SocialLinksRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class SocialLinksApi extends Controller{
	protected $selectColumns = [
		"id",
		"social_name",
		"social_url",
		"social_icon",
		"social_image",
		"status",
	];

            /**
             * Display the specified releationshop.
             * Baboon Api Script By [it v 1.6.37]
             * @return array to assign with index & show methods
             */
            public function arrWith(){
               return [];
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Display a listing of the resource. Api
             * @return \Illuminate\Http\Response
             */
            public function index()
            {
            	$SocialLink = SocialLink::select($this->selectColumns)->with($this->arrWith())->orderBy("id","desc")->paginate(15);
               return successResponseJson(["data"=>$SocialLink]);
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Store a newly created resource in storage. Api
             * @return \Illuminate\Http\Response
             */
    public function store(SocialLinksRequest $request)
    {
    	$data = $request->except("_token");
    	
                $data["social_image"] = "";
        $SocialLink = SocialLink::create($data); 
               if(request()->hasFile("social_image")){
              $SocialLink->social_image = it()->upload("social_image","sociallinks/".$SocialLink->id);
              $SocialLink->save();
              }

		  $SocialLink = SocialLink::with($this->arrWith())->find($SocialLink->id,$this->selectColumns);
        return successResponseJson([
            "message"=>trans("admin.added"),
            "data"=>$SocialLink
        ]);
    }


            /**
             * Display the specified resource.
             * Baboon Api Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
                $SocialLink = SocialLink::with($this->arrWith())->find($id,$this->selectColumns);
            	if(is_null($SocialLink) || empty($SocialLink)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}

                 return successResponseJson([
              "data"=> $SocialLink
              ]);  ;
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				       $fillableCols = [];
				       foreach (array_keys((new SocialLinksRequest)->attributes()) as $fillableUpdate) {
  				        if (!is_null(request($fillableUpdate))) {
						  $fillableCols[$fillableUpdate] = request($fillableUpdate);
						}
				       }
  				     return $fillableCols;
  	     		}

            public function update(SocialLinksRequest $request,$id)
            {
            	$SocialLink = SocialLink::find($id);
            	if(is_null($SocialLink) || empty($SocialLink)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
  			       }

            	$data = $this->updateFillableColumns();
                 
               if(request()->hasFile("social_image")){
              it()->delete($SocialLink->social_image);
              $data["social_image"] = it()->upload("social_image","sociallinks/".$SocialLink->id);
               }
              SocialLink::where("id",$id)->update($data);

              $SocialLink = SocialLink::with($this->arrWith())->find($id,$this->selectColumns);
              return successResponseJson([
               "message"=>trans("admin.updated"),
               "data"=> $SocialLink
               ]);
            }

            /**
             * Baboon Api Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function destroy($id)
            {
               $sociallinks = SocialLink::find($id);
            	if(is_null($sociallinks) || empty($sociallinks)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}


              if(!empty($sociallinks->social_image)){
               it()->delete($sociallinks->social_image);
              }
               it()->delete("sociallink",$id);

               $sociallinks->delete();
               return successResponseJson([
                "message"=>trans("admin.deleted")
               ]);
            }



 			public function multi_delete()
            {
                $data = request("selected_data");
                if(is_array($data)){
                    foreach($data as $id){
                    $sociallinks = SocialLink::find($id);
	            	if(is_null($sociallinks) || empty($sociallinks)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}

                    	if(!empty($sociallinks->social_image)){
                    	it()->delete($sociallinks->social_image);
                    	}
                    	it()->delete("sociallink",$id);
                    	$sociallinks->delete();
                    }
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }else {
                    $sociallinks = SocialLink::find($data);
	            	if(is_null($sociallinks) || empty($sociallinks)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}
 
                    	if(!empty($sociallinks->social_image)){
                    	it()->delete($sociallinks->social_image);
                    	}
                    	it()->delete("sociallink",$data);

                    $sociallinks->delete();
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }
            }

            
}