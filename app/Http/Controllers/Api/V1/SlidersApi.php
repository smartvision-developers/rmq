<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Slider;
use Validator;
use App\Http\Controllers\ValidationsApi\V1\SlidersRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class SlidersApi extends Controller{
	protected $selectColumns = [
		"id",
		"slider_title",
		"slider_image",
		"slider_content",
		"status",
	];

            /**
             * Display the specified releationshop.
             * Baboon Api Script By [it v 1.6.37]
             * @return array to assign with index & show methods
             */
            public function arrWith(){
               return [];
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Display a listing of the resource. Api
             * @return \Illuminate\Http\Response
             */
            public function index()
            {
            	$Slider = Slider::select($this->selectColumns)->with($this->arrWith())->orderBy("id","desc")->paginate(15);
               return successResponseJson(["data"=>$Slider]);
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Store a newly created resource in storage. Api
             * @return \Illuminate\Http\Response
             */
    public function store(SlidersRequest $request)
    {
    	$data = $request->except("_token");
    	
                $data["slider_image"] = "";
        $Slider = Slider::create($data); 
               if(request()->hasFile("slider_image")){
              $Slider->slider_image = it()->upload("slider_image","sliders/".$Slider->id);
              $Slider->save();
              }

		  $Slider = Slider::with($this->arrWith())->find($Slider->id,$this->selectColumns);
        return successResponseJson([
            "message"=>trans("admin.added"),
            "data"=>$Slider
        ]);
    }


            /**
             * Display the specified resource.
             * Baboon Api Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
                $Slider = Slider::with($this->arrWith())->find($id,$this->selectColumns);
            	if(is_null($Slider) || empty($Slider)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}

                 return successResponseJson([
              "data"=> $Slider
              ]);  ;
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				       $fillableCols = [];
				       foreach (array_keys((new SlidersRequest)->attributes()) as $fillableUpdate) {
  				        if (!is_null(request($fillableUpdate))) {
						  $fillableCols[$fillableUpdate] = request($fillableUpdate);
						}
				       }
  				     return $fillableCols;
  	     		}

            public function update(SlidersRequest $request,$id)
            {
            	$Slider = Slider::find($id);
            	if(is_null($Slider) || empty($Slider)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
  			       }

            	$data = $this->updateFillableColumns();
                 
               if(request()->hasFile("slider_image")){
              it()->delete($Slider->slider_image);
              $data["slider_image"] = it()->upload("slider_image","sliders/".$Slider->id);
               }
              Slider::where("id",$id)->update($data);

              $Slider = Slider::with($this->arrWith())->find($id,$this->selectColumns);
              return successResponseJson([
               "message"=>trans("admin.updated"),
               "data"=> $Slider
               ]);
            }

            /**
             * Baboon Api Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function destroy($id)
            {
               $sliders = Slider::find($id);
            	if(is_null($sliders) || empty($sliders)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}


              if(!empty($sliders->slider_image)){
               it()->delete($sliders->slider_image);
              }
               it()->delete("slider",$id);

               $sliders->delete();
               return successResponseJson([
                "message"=>trans("admin.deleted")
               ]);
            }



 			public function multi_delete()
            {
                $data = request("selected_data");
                if(is_array($data)){
                    foreach($data as $id){
                    $sliders = Slider::find($id);
	            	if(is_null($sliders) || empty($sliders)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}

                    	if(!empty($sliders->slider_image)){
                    	it()->delete($sliders->slider_image);
                    	}
                    	it()->delete("slider",$id);
                    	$sliders->delete();
                    }
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }else {
                    $sliders = Slider::find($data);
	            	if(is_null($sliders) || empty($sliders)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}
 
                    	if(!empty($sliders->slider_image)){
                    	it()->delete($sliders->slider_image);
                    	}
                    	it()->delete("slider",$data);

                    $sliders->delete();
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }
            }

            
}