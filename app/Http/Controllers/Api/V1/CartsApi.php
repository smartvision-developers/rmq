<?php
namespace App\Http\Controllers\Api\V1;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Cart;
use Validator;
use App\Http\Controllers\ValidationsApi\V1\CartsRequest;
// Auto Controller Maker By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class CartsApi extends Controller{
	protected $selectColumns = [
		"id",
		"product_name",
		"product_price",
		"product_id",
		"client_id",
		"client_name",
		"client_mobile",
		"client_email",
		"client_id_card",
		"client_job",
		"report_image",
		"city_id",
		"region_id",
		"vat",
		"total_price",
	];

            /**
             * Display the specified releationshop.
             * Baboon Api Script By [it v 1.6.37]
             * @return array to assign with index & show methods
             */
            public function arrWith(){
               return ['product_id','client_id','city_id','region_id',];
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Display a listing of the resource. Api
             * @return \Illuminate\Http\Response
             */
            public function index()
            {
            	$Cart = Cart::select($this->selectColumns)->with($this->arrWith())->orderBy("id","desc")->paginate(15);
               return successResponseJson(["data"=>$Cart]);
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * Store a newly created resource in storage. Api
             * @return \Illuminate\Http\Response
             */
    public function store(CartsRequest $request)
    {
    	$data = $request->except("_token");
    	
                $data["report_image"] = "";
        $Cart = Cart::create($data); 
               if(request()->hasFile("report_image")){
              $Cart->report_image = it()->upload("report_image","carts/".$Cart->id);
              $Cart->save();
              }

		  $Cart = Cart::with($this->arrWith())->find($Cart->id,$this->selectColumns);
        return successResponseJson([
            "message"=>trans("admin.added"),
            "data"=>$Cart
        ]);
    }


            /**
             * Display the specified resource.
             * Baboon Api Script By [it v 1.6.37]
             * @param  int  $id
             * @return \Illuminate\Http\Response
             */
            public function show($id)
            {
                $Cart = Cart::with($this->arrWith())->find($id,$this->selectColumns);
            	if(is_null($Cart) || empty($Cart)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}

                 return successResponseJson([
              "data"=> $Cart
              ]);  ;
            }


            /**
             * Baboon Api Script By [it v 1.6.37]
             * update a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function updateFillableColumns() {
				       $fillableCols = [];
				       foreach (array_keys((new CartsRequest)->attributes()) as $fillableUpdate) {
  				        if (!is_null(request($fillableUpdate))) {
						  $fillableCols[$fillableUpdate] = request($fillableUpdate);
						}
				       }
  				     return $fillableCols;
  	     		}

            public function update(CartsRequest $request,$id)
            {
            	$Cart = Cart::find($id);
            	if(is_null($Cart) || empty($Cart)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
  			       }

            	$data = $this->updateFillableColumns();
                 
               if(request()->hasFile("report_image")){
              it()->delete($Cart->report_image);
              $data["report_image"] = it()->upload("report_image","carts/".$Cart->id);
               }
              Cart::where("id",$id)->update($data);

              $Cart = Cart::with($this->arrWith())->find($id,$this->selectColumns);
              return successResponseJson([
               "message"=>trans("admin.updated"),
               "data"=> $Cart
               ]);
            }

            /**
             * Baboon Api Script By [it v 1.6.37]
             * destroy a newly created resource in storage.
             * @return \Illuminate\Http\Response
             */
            public function destroy($id)
            {
               $carts = Cart::find($id);
            	if(is_null($carts) || empty($carts)){
            	 return errorResponseJson([
            	  "message"=>trans("admin.undefinedRecord")
            	 ]);
            	}


              if(!empty($carts->report_image)){
               it()->delete($carts->report_image);
              }
               it()->delete("cart",$id);

               $carts->delete();
               return successResponseJson([
                "message"=>trans("admin.deleted")
               ]);
            }



 			public function multi_delete()
            {
                $data = request("selected_data");
                if(is_array($data)){
                    foreach($data as $id){
                    $carts = Cart::find($id);
	            	if(is_null($carts) || empty($carts)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}

                    	if(!empty($carts->report_image)){
                    	it()->delete($carts->report_image);
                    	}
                    	it()->delete("cart",$id);
                    	$carts->delete();
                    }
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }else {
                    $carts = Cart::find($data);
	            	if(is_null($carts) || empty($carts)){
	            	 return errorResponseJson([
	            	  "message"=>trans("admin.undefinedRecord")
	            	 ]);
	            	}
 
                    	if(!empty($carts->report_image)){
                    	it()->delete($carts->report_image);
                    	}
                    	it()->delete("cart",$data);

                    $carts->delete();
                    return successResponseJson([
                     "message"=>trans("admin.deleted")
                    ]);
                }
            }

            
}