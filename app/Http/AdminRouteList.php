<?php
/*
* To implement in admingroups permissions
* to remove CRUD from Validation remove route name
* CRUD Role permission (create,read,update,delete)
* [it v 1.6.37]
*/
return [
	"productcoupons"=>["create","read","update","delete"],
	"carts"=>["create","read","update","delete"],
	"productdimensions"=>["create","read","update","delete"],
	"sociallinks"=>["create","read","update","delete"],
	"mostpopulars"=>["create","read","update","delete"],
	"sliders"=>["create","read","update","delete"],
	"abouts"=>["create","read","update","delete"],
	"productbooks"=>["create","read","update","delete"],
	"statistics"=>["create","read","update","delete"],
	"tradmarks"=>["create","read","update","delete"],
	"trademarks"=>["create","read","update","delete"],
	"productfavourites"=>["create","read","update","delete"],
	"products"=>["create","read","update","delete"],
	"websitelinks"=>["create","read","update","delete"],
	"contacts"=>["create","read","update","delete"],
	"users"=>["create","read","update","delete"],
	"regions"=>["create","read","update","delete"],
	"citys"=>["create","read","update","delete"],
	"countrys"=>["create","read","update","delete"],
	"admins"=>["create","read","update","delete"],
	"admingroups"=>["create","read","update","delete"],
];