<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class MostPopular extends Model {

protected $table    = 'most_populars';
protected $fillable = [
		'id',
		'admin_id',
        'product_id',

        'status',

		'created_at',
		'updated_at',
	];

	/**
    * product_id relation method
    * @param void
    * @return object data
    */
   public function product_id(){
      return $this->hasOne(\App\Models\Product::class,'id','product_id');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($mostpopular) {
			//$mostpopular->product_id()->delete();
         });
   }
		
}
