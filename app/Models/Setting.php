<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
	protected $table = 'settings';
	protected $fillable = [
		'sitename_ar',
		'sitename_en',
		'sitename_fr',
		'email',
		'mobile',
		'address',
		'logo',
		'icon',
		'system_status',
		'system_message',
		'theme_setting',
		'about_us',
		'video',
		'tax_no',
		'commercial_registration_no','vat',
		'lng',
		'lat',
		'policy',
	];

}
