<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_Coupon extends Model
{
    use HasFactory;


protected $table    = 'productcoupons';
protected $fillable = [
        'id',
        'coupon_id',
        'product_id',
        'created_at',
        'updated_at',
    ];

   public function coupon_id() {
       return $this->hasOne(\App\Models\ProductCoupon::class, 'id', 'coupon_id');
   }
    
     public function product_id() {
       return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
   }
}
