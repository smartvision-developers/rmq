<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductFavourite extends Model {

protected $table    = 'product_favourites';
protected $fillable = [
		'id',
		'admin_id',
        'user_id',

        'product_id',

		'created_at',
		'updated_at',
	];

	/**
    * product_id relation method
    * @param void
    * @return object data
    */
   public function product_id(){
      return $this->hasOne(\App\Models\Product::class,'id','product_id');
   }

	/**
    * user_id relation method
    * @param void
    * @return object data
    */
   public function user_id(){
      return $this->hasOne(\App\Models\User::class,'id','user_id');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($productfavourite) {
			//$productfavourite->product_id()->delete();
			//$productfavourite->product_id()->delete();
         });
   }
		
}
