<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Cart extends Model {

protected $table    = 'carts';
protected $fillable = [
		'id',
		'admin_id',
        'product_name',
        'product_price',
        'product_id',

        'client_id',

        'client_name',
        'client_mobile',
        'client_email',
        'client_id_card',
        'client_job',
        'report_image',
        'city_id',

        'region_id',
        'coupon_id',
        'price_after_coupon',
        'vat',
        'total_price',
        'north',
        'east',
		'created_at',
		'updated_at',
	];

	/**
    * client_id relation method
    * @param void
    * @return object data
    */
   public function client_id(){
      return $this->hasOne(\App\Models\User::class,'id','client_id');
   }

	/**
    * product_id relation method
    * @param void
    * @return object data
    */
   public function product_id(){
      return $this->hasOne(\App\Models\Product::class,'id','product_id');
   }

	/**
    * city_id relation method
    * @param void
    * @return object data
    */
   public function city_id(){
      return $this->hasOne(\App\Models\City::class,'id','city_id');
   }

	/**
    * region_id relation method
    * @param void
    * @return object data
    */
   public function region_id(){
      return $this->hasOne(\App\Models\Region::class,'id','region_id');
   }

    public function coupon_id(){
      return $this->hasOne(\App\Models\ProductCoupon::class,'id','coupon_id');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($cart) {
			//$cart->client_id()->delete();
			//$cart->client_id()->delete();
			//$cart->client_id()->delete();
			//$cart->client_id()->delete();
         });
   }
		
}
