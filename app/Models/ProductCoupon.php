<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class ProductCoupon extends Model {

protected $table    = 'product_coupons';
protected $fillable = [
		'id',
		'admin_id',
        'coupon_name',
        'coupon_code',
        'start_date',
        'end_date',
        'coupon_discount',
        'discount_type',

        'status',

		'created_at',
		'updated_at',
	];

	/**
    * product_id relation method
    * @param void
    * @return object data
    */
   public function coupon_id(){
      return $this->hasMany(\App\Models\Product_Coupon::class,'coupon_id');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($productcoupon) {
			//$productcoupon->product_id()->delete();
         });
   }
		
}
