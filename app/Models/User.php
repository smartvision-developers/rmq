<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class User extends Authenticatable {

protected $table    = 'users';
protected $fillable = [
		'id',
		'admin_id',
        'first_name',
        'last_name',
        'email',
        'mobile',
        'password',
        'city_id',
        'region_id',
        'country_code',
        'status',
        'code',
        'forget_password',
		'created_at',
		'updated_at',
      'decline_reason',
	];

	/**
    * city_id relation method
    * @param void
    * @return object data
    */
   public function city_id(){
      return $this->hasOne(\App\Models\City::class,'id','city_id');
   }

   public function favourite_id(){
      return $this->hasMany(\App\Models\ProductFavourite::class,'user_id');
   }

   public function cart_id(){
      return $this->hasMany(\App\Models\Cart::class,'client_id');
   }
	/**
    * region_id relation method
    * @param void
    * @return object data
    */
   public function region_id(){
      return $this->hasOne(\App\Models\Region::class,'id','region_id');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($user) {
			//$user->city_id()->delete();
			//$user->city_id()->delete();
         });
   }
		
}
