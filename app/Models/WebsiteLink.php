<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class WebsiteLink extends Model {

protected $table    = 'website_links';
protected $fillable = [
		'id',
		'admin_id',
        'link_name',
        'link_url',
        'link_position',

        'sub_link',

        'link_target',

        'link_status',

		'created_at',
		'updated_at',
	];

	/**
    * sub_link relation method
    * @param void
    * @return object data
    */
   public function child(){
      return $this->hasMany(\App\Models\WebsiteLink::class,'sub_link');
   }

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($websitelink) {
			//$websitelink->sub_link()->delete();
         });
   }
		
}
