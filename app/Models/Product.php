<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Auto Models By Baboon Script
// Baboon Maker has been Created And Developed By  [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class Product extends Model {

protected $table    = 'products';
protected $fillable = [
		'id',
		'admin_id',
        'product_name',
        'price',
        'number_floor',
        'number_room',
        'land_area',
        'design_by',
        'product_image',
        'product_description',
        'product_video',
        'product_content',
        'chart_information',
        'design_book',
        'design_file_details',
        'notes',
		'created_at',
		'updated_at',
      'sold',
      'stock',
      'status',
      'design_files',
      'view_count',
	];

 	/**
    * Static Boot method to delete or update or sort Data
    * @param void
    * @return void
    */
   protected static function boot() {
      parent::boot();
      // if you disable constraints should by run this static method to Delete children data
         static::deleting(function($product) {
         });
   }


   public function product_book(){
      return $this->hasMany(\App\Models\ProductBook::class,'product_id');
   }

   public function product_dimension(){
      return $this->hasMany(\App\Models\ProductDimension::class,'product_id');
   }
		
}
