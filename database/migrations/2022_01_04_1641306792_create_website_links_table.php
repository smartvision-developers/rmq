<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// Auto Schema  By Baboon Script
// Baboon Maker has been Created And Developed By [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class CreateWebsiteLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link_name');
            $table->string('link_url');
            $table->enum('link_position',['header','footer']);
            $table->foreignId("sub_link")->constrained("website_links")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->enum('link_target',['self','blank']);
            $table->enum('link_status',['1','0']);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_links');
    }
}