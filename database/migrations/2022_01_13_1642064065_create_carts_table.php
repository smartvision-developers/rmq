<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// Auto Schema  By Baboon Script
// Baboon Maker has been Created And Developed By [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->bigInteger('product_price');
            $table->foreignId("product_id")->constrained("products")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("client_id")->constrained("users")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->string('client_name');
            $table->string('client_mobile');
            $table->string('client_email');
            $table->string('client_id_card');
            $table->string('client_job');
            $table->string('report_image');
            $table->foreignId("city_id")->constrained("cities")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("region_id")->constrained("regions")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->string('vat')->nullable();
            $table->bigInteger('total_price')->nullable();
            $table->string('north');
            $table->string('east');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}