<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// Auto Schema  By Baboon Script
// Baboon Maker has been Created And Developed By [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->string('number_floor');
            $table->string('number_room');
            $table->bigInteger('land_area');
            $table->string('design_by');
            $table->string('product_image');
            $table->longtext('product_description');
            $table->string('product_video')->nullable();
            $table->longtext('product_content');
            $table->longtext('chart_information')->nullable();
            $table->string('design_book')->nullable();
            $table->longtext('design_file_details')->nullable();
            $table->longtext('notes')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}