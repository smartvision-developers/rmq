<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
// Auto Schema  By Baboon Script
// Baboon Maker has been Created And Developed By [it v 1.6.37]
// Copyright Reserved  [it v 1.6.37]
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->bigInteger('mobile');
            $table->string('password');
            $table->foreignId("city_id")->constrained("cities")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->foreignId("region_id")->constrained("regions")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->string('country_code')->nullable();
            $table->enum('status',['pending','accepted','rejected'])->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}