-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2022 at 10:37 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ramq`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `content`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'المهندس فهمي وعد الفرحان', 'عنه إلا لضرورة الرعي. وألف الطفل تلك الظبية حتى وصل القلب. فقصد أولاً إلى الجهة التي بدأ بالشق منها، فقال في نفسه: ما أحسن ما صنع من نقض عهوده في شرط غذاء، وندم على فعله، وأراد الانفصال عن أسال والإقبال على شأنه من طلب الرجوع إلى ما كانت عليه فلم ياتت له شيء من ذلك فكان يفكر في شيء من صفات.', 'abouts/faker/ab655c03b820eb1a1f0797c94dd124cb.png', 'show', '2022-01-10 11:03:02', '2022-01-10 11:03:02'),
(2, 'الآنسة وسام حمود حسن برماوي', 'تلك المشاهدة؛ وانما احتيج إلى هذا الحد، وفارق المحسوس بعض مفارقة، وأشرف على تخوم العالم العقلي، استوحش وحن إلى ما شاهده في مقامه الكريم. فعلم أن الذي وصف ذلك التخلق. قالوا: فلما تعلق هذا الروح دائم الفيضان من عند الفاعل الواجب الوجود. متصف بأوصاف الكمال كلها، ومنزه عن الصفات النقص وبريء منها. وتبين له أن الشيء الذي به عرف الموجود الواجب الوجود. والضرب الثاني: أوصاف لها بالإضافة.', 'abouts/faker/9c25a090ecbd0b110cc3ad872e177ec4.png', 'show', '2022-01-10 11:03:02', '2022-01-10 11:03:02'),
(3, 'المهندسة فريال الأسمري', 'غير أنه لا ضد لصورها؛ ويكون روح ذلك الحيوان، وكأنه وسط بالحقيقة بين الاسطقسات التي لا تحس ولا تغتذي ولا تنمو، من الحجارة، والتراب، والماء، والهواء، واللهب، فيرى أنها أجسام مقدر لها الطول وعرض وعمق وأنها لاتختلف، إلا أن مطلوبي كان فيه!.', 'abouts/faker/723a646ac2f0f9bb5bcb5e40848c5449.png', 'show', '2022-01-10 11:03:03', '2022-01-10 11:03:03'),
(4, 'السيدة الآنسة سندس الشيباني', 'ثمراتها الحلوة النضيجة؛ وما كان من هذه المشاهدة، إن الذوات المفارقة إن كانت كلها موجودة فينبغي له حينئذ إن يتثبت ويتخير منها ما يكون فان كان الموضع مما تبعد الشمس عن مسامتة رؤوس أهله، كان شديد البرودة جداً، وان كان مما تدوم فيه المسامتة كان شديد الحرارة، كالذي يصرح به أكثرهم فهو خطأ يقوم البرهان على خلافه. وذلك أنه كان يرى أن هذا الدم.', 'abouts/faker/f90f61d4b89b23f399970465e610f4af.png', 'show', '2022-01-10 11:03:03', '2022-01-10 11:03:03'),
(5, 'السيد عودة لطفي الداوود', 'هي الذوات العارفه بالموجود الحق؛ وغابت ذاته في جملة من الأجسام، ثم حركت يدك، فان ذلك الجسم لا محالة يتحرك تابعاً لحركة.', 'abouts/faker/82678c5ca7c965ee552f6fa889ec6f38.png', 'show', '2022-01-10 11:03:04', '2022-01-11 11:20:51'),
(6, 'تلاال', 'يبلاتنوةتا', 'abouts/6/20dDRsxw1ubJfb4r41ymTVfFRWsLafv2ZBlaF7I0.jpg', 'show', '2022-01-11 11:32:31', '2022-01-11 11:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_profile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `photo_profile`, `password`, `group_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'test@test.com', NULL, '$2y$10$0Z..LXDoghbBNH7tJW1kauJpjKSCUTfVuZaU0yvlePa2H7J5UDB4m', 1, NULL, '2022-01-02 11:01:43', '2022-01-02 11:01:43');

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups`
--

CREATE TABLE `admin_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_groups`
--

INSERT INTO `admin_groups` (`id`, `admin_id`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Full Permission - Admin', '2022-01-02 11:01:44', '2022-01-02 11:01:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_group_roles`
--

CREATE TABLE `admin_group_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_groups_id` bigint(20) UNSIGNED DEFAULT NULL,
  `show` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `add` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `edit` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `delete` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_group_roles`
--

INSERT INTO `admin_group_roles` (`id`, `name`, `admin_groups_id`, `show`, `add`, `edit`, `delete`, `created_at`, `updated_at`) VALUES
(1, 'admingroups', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-02 11:01:44', '2022-01-02 11:01:44'),
(2, 'admins', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-02 11:01:44', '2022-01-02 11:01:44'),
(3, 'settings', 1, 'yes', 'no', 'yes', 'no', '2022-01-02 11:01:44', '2022-01-02 11:01:44'),
(4, 'countrys', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 06:26:20', '2022-01-04 06:26:20'),
(5, 'citys', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 06:28:33', '2022-01-04 06:28:33'),
(6, 'regions', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 06:30:47', '2022-01-04 06:30:47'),
(7, 'users', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 06:45:56', '2022-01-04 06:45:56'),
(8, 'sliders', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 09:30:43', '2022-01-04 09:30:43'),
(9, 'contacts', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 11:33:43', '2022-01-04 11:33:43'),
(10, 'trademarks', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 11:55:05', '2022-01-04 11:55:05'),
(11, 'abouts', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 12:10:38', '2022-01-04 12:10:38'),
(12, 'websitelinks', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-04 12:33:12', '2022-01-04 12:33:12'),
(13, 'products', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 05:38:41', '2022-01-05 05:38:41'),
(14, 'productfavourites', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 07:24:29', '2022-01-05 07:24:29'),
(15, 'mostpopulars', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 10:38:18', '2022-01-05 10:38:18'),
(16, 'statistics', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 10:54:48', '2022-01-05 10:54:48'),
(17, 'sociallinks', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 11:22:43', '2022-01-05 11:22:43'),
(18, 'tradmarks', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-05 12:41:19', '2022-01-05 12:41:19'),
(19, 'productbooks', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-10 10:09:38', '2022-01-10 10:09:38'),
(20, 'carts', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-13 06:54:25', '2022-01-13 06:54:25'),
(21, 'productdimensions', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-13 07:40:30', '2022-01-13 07:40:30'),
(22, 'productcoupons', 1, 'yes', 'yes', 'yes', 'yes', '2022-01-23 07:22:44', '2022-01-23 07:22:44');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` decimal(8,2) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `region_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` decimal(8,2) DEFAULT NULL,
  `north` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `east` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `coupon_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price_after_coupon` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `product_name`, `product_price`, `product_id`, `client_id`, `client_name`, `client_mobile`, `client_email`, `client_id_card`, `client_job`, `report_image`, `city_id`, `region_id`, `vat`, `total_price`, `north`, `east`, `created_at`, `updated_at`, `coupon_id`, `price_after_coupon`) VALUES
(6, 'knjhbsefnj', '1.30', 14, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '2.00', '3', '5', '2022-01-16 08:19:53', '2022-01-16 08:19:53', NULL, NULL),
(7, 'test design', '88.20', 15, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '102.00', '2', '3', '2022-01-23 06:50:42', '2022-01-23 06:50:42', NULL, NULL),
(8, 'test design', '88.20', 15, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '102.00', '3', '1', '2022-01-24 06:53:32', '2022-01-24 07:28:25', 4, '91.80');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `created_at`, `updated_at`) VALUES
(1, 'الدمام', NULL, NULL),
(2, 'مكه', NULL, '2022-01-09 07:48:05'),
(3, 'جده', '2022-01-09 07:47:35', '2022-01-09 07:51:11');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `title`, `message`, `created_at`, `updated_at`) VALUES
(1, 'djnbhzbnMN', 'muahmed123@yahoo.com', ',MNCBVSHBN', 'NB HVDBNM,', '2022-01-04 11:45:36', '2022-01-04 11:45:36'),
(2, ',sdnbhcnjm', 'mnbhgvfbsdhnj@yhs.com', 'nqbdfhdjnsmk', 'njhdbfbgjnkfmdsl,a', '2022-01-05 11:05:45', '2022-01-05 11:05:45'),
(3, 'bhgvvfbhjnmk', 'njbhv@jmd.co', 'ndfbfdhnjmk', 'njbhfdrnj', '2022-01-05 11:08:00', '2022-01-05 11:08:00'),
(5, 'ةتىابلايس', 'muahmed123@yahoo.com', 'نتعابيلاسىة', 'ىستيارالاتىنةي', '2022-01-09 05:55:02', '2022-01-09 05:55:02'),
(6, 'jhsudfnj', 'newvcbhdn@yahoo.com', ',kjihsub', 'vghbjndmks,v', '2022-01-09 05:55:53', '2022-01-09 05:55:53'),
(7, 'ةيىسبتلاائى', 'test@test.com', 'نتالربللااىتةن', 'نةتىااغثالابىتة', '2022-01-09 06:35:06', '2022-01-09 06:35:06'),
(8, 'testuser', 'test@test.com', ',mnbvdb', 'mnbvcs', '2022-01-11 11:17:21', '2022-01-11 11:17:21'),
(9, 'testuser', 'admin@admin.com', 'lkjg', 'vbhnjkm', '2022-01-11 11:18:59', '2022-01-11 11:18:59'),
(10, 'testuser', 'test@test.com', 'fgsff', 'jhgh', '2022-01-11 11:19:11', '2022-01-11 11:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_name` bigint(20) NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ext` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size_bytes` bigint(20) NOT NULL,
  `mimtype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `admin_id`, `user_id`, `file`, `full_path`, `type_file`, `type_id`, `path`, `ext`, `name`, `size`, `size_bytes`, `mimtype`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, NULL, NULL, 'XdrAQjjnRwzt7Ln2EYBxwvtRAubIXqFeEvOwYqzG.jpg', 'settings/1/XdrAQjjnRwzt7Ln2EYBxwvtRAubIXqFeEvOwYqzG.jpg', 'setting', '1', 'settings/1', 'jpg', 'hero.jpg', '118.96 KB', 121810, 'image/jpeg', NULL, '2022-01-04 11:05:39', '2022-01-04 11:05:39'),
(3, NULL, NULL, 'uNxUeT6scLLbr3BoG3yTlyIP4Jcch9PNXWirX5r5.jpg', 'settings/1/uNxUeT6scLLbr3BoG3yTlyIP4Jcch9PNXWirX5r5.jpg', 'setting', '1', 'settings/1', 'jpg', '2.jpg', '35.24 KB', 36090, 'image/jpeg', NULL, '2022-01-04 11:05:39', '2022-01-04 11:05:39'),
(4, NULL, NULL, 'sMw9zYdrjgmXsIJRuozrVBPzrbD7HXylMH1HB8TL.jpg', 'settings/1/sMw9zYdrjgmXsIJRuozrVBPzrbD7HXylMH1HB8TL.jpg', 'setting', '1', 'settings/1', 'jpg', '2.jpg', '35.24 KB', 36090, 'image/jpeg', NULL, '2022-01-04 11:06:06', '2022-01-04 11:06:06'),
(5, NULL, NULL, '83TbUbF4Ggz2a1ksFvYgRNX5nFfmMVpgjxt5OIL3.jpg', 'settings/1/83TbUbF4Ggz2a1ksFvYgRNX5nFfmMVpgjxt5OIL3.jpg', 'setting', '1', 'settings/1', 'jpg', '2.jpg', '35.24 KB', 36090, 'image/jpeg', NULL, '2022-01-04 11:07:40', '2022-01-04 11:07:40'),
(7, NULL, NULL, 'd4PuOFNv5zKNrWCwTc2kghbOPh6qeAmYZBWvhBWa.jpg', 'setting/1/d4PuOFNv5zKNrWCwTc2kghbOPh6qeAmYZBWvhBWa.jpg', 'setting', '1', 'setting/1', 'jpg', '3d.jpg', '19.42 KB', 19881, 'image/jpeg', NULL, '2022-01-04 11:14:33', '2022-01-04 11:14:33'),
(9, NULL, NULL, 'b4fVFVym60zeV1njxgd2tTvqTf3KGEzvNw4N8ynl.jpg', 'products/6/b4fVFVym60zeV1njxgd2tTvqTf3KGEzvNw4N8ynl.jpg', 'products', '6', 'products/6', 'jpg', '3d.jpg', '19.42 KB', 19881, 'image/jpeg', NULL, '2022-01-05 06:53:11', '2022-01-05 06:53:11'),
(10, NULL, NULL, 'I7bLfNUmuZ31ItZakWWxGjTMQgOXbGa88jnIfx1Z.jpg', 'products/6/I7bLfNUmuZ31ItZakWWxGjTMQgOXbGa88jnIfx1Z.jpg', 'products', '6', 'products/6', 'jpg', '2.jpg', '35.24 KB', 36090, 'image/jpeg', NULL, '2022-01-05 06:53:11', '2022-01-05 06:53:11'),
(11, NULL, NULL, 'ViGZWTA1aWBUaLR23cGMAdrn0hohE65OFgpfpglK.jpg', 'setting/1/ViGZWTA1aWBUaLR23cGMAdrn0hohE65OFgpfpglK.jpg', 'settings', '1', 'setting/1', 'jpg', 'hero2.jpg', '124.04 KB', 127022, 'image/jpeg', NULL, '2022-01-09 05:42:14', '2022-01-09 05:42:14'),
(12, NULL, NULL, 'AMAJ60JSESszAzbMNRq4bIRBHz77GG2C0qqoiMaM.jpg', 'setting/1/AMAJ60JSESszAzbMNRq4bIRBHz77GG2C0qqoiMaM.jpg', 'settings', '1', 'setting/1', 'jpg', 'hero.jpg', '118.96 KB', 121810, 'image/jpeg', NULL, '2022-01-09 05:42:14', '2022-01-09 05:42:14'),
(13, NULL, NULL, 'v3IdsCzs6gsY4GIcyRpNCLu9xwBl2PuLDTeJzXJZ.png', 'products/14/v3IdsCzs6gsY4GIcyRpNCLu9xwBl2PuLDTeJzXJZ.png', 'products', '14', 'products/14', 'png', 'image.png', '51.50 KB', 52740, 'image/png', NULL, '2022-01-10 09:52:12', '2022-01-10 09:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_02_17_094109_create_admins_table', 1),
(6, '2021_02_18_102130_create_files_table', 1),
(7, '2021_02_19_985759_create_settings_table', 1),
(8, '2021_03_22_134182_create_admin_groups_table', 1),
(9, '2021_03_22_193126_create_admin_group_roles_table', 1),
(10, '2022_01_04_1641284778_create_countries_table', 2),
(11, '2022_01_04_1641284913_create_cities_table', 3),
(12, '2022_01_04_1641285047_create_regions_table', 4),
(14, '2022_01_04_1641285956_create_users_table', 5),
(18, '2022_01_04_1641303223_create_contacts_table', 7),
(25, '2022_01_04_1641306792_create_website_links_table', 10),
(27, '2022_01_05_1641368320_create_products_table', 11),
(37, '2022_01_05_1641374669_create_product_favourites_table', 18),
(39, '2022_01_04_1641304505_create_trademarks_table', 19),
(40, '2022_01_05_1641393678_create_tradmarks_table', 20),
(42, '2022_01_05_1641387288_create_statistics_table', 21),
(43, '2022_01_10_1641816577_create_product_books_table', 22),
(47, '2022_01_04_1641305438_create_abouts_table', 23),
(48, '2022_01_04_1641295843_create_sliders_table', 24),
(49, '2022_01_05_1641386297_create_most_populars_table', 25),
(50, '2022_01_05_1641388962_create_social_links_table', 26),
(60, '2022_01_13_1642066830_create_product_dimensions_table', 30),
(61, '2022_01_13_1642064065_create_carts_table', 31),
(62, '2022_01_23_1642929762_create_product_coupons_table', 32);

-- --------------------------------------------------------

--
-- Table structure for table `most_populars`
--

CREATE TABLE `most_populars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `most_populars`
--

INSERT INTO `most_populars` (`id`, `product_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'show', '2022-01-10 11:09:29', '2022-01-10 11:09:29'),
(2, 12, 'show', '2022-01-10 11:09:36', '2022-01-10 11:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productcoupons`
--

CREATE TABLE `productcoupons` (
  `id` bigint(20) NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productcoupons`
--

INSERT INTO `productcoupons` (`id`, `coupon_id`, `product_id`, `created_at`, `updated_at`) VALUES
(11, 4, 1, '2022-01-24 06:15:50', '2022-01-24 06:15:50'),
(12, 4, 3, '2022-01-24 06:15:50', '2022-01-24 06:15:50');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_room` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `land_area` bigint(20) NOT NULL,
  `design_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `chart_information` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `design_book` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `design_file_details` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `sold` int(11) DEFAULT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci NOT NULL,
  `design_files` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `number_floor`, `number_room`, `land_area`, `design_by`, `price`, `product_image`, `product_description`, `product_video`, `product_content`, `chart_information`, `design_book`, `design_file_details`, `notes`, `created_at`, `updated_at`, `stock`, `sold`, `status`, `design_files`, `view_count`) VALUES
(1, 'بينه وبين نفسه وذلك أنه.', '+19739311162', '+16297422941', 13238438959, 'من أمر هذا الفاعل، ما.', '0.00', 'products/faker/043023071ca5e7bfaa364891cf4f0c9a.png', 'اللذة والسرور، والغبطة والفرح، بمشاهدة ذات الحق جل جلاله. وشاهد ايضاً للفلك الذي يليه، وهو فلك الكواكب الثابتة، ذاتاً بريئة عن الكثرة. وان أنت عبرت بصيغة الإفراد، اوهم ذلك معنى الاتحاد، وهو مستحيل عليها. وكأني بمن يقف على حقيقة شأنه، ولا يبقي في نفسه.', 'products/faker/YQzJsdeRVqiWkdVXTDxoSfxZjH6ukC8ErVzGnrM2.mp4', 'وهو التمام، وهو الحسن، وهو البهاء، وهو القدرة، وهو العلم، وهو هو، و \"بسم الله الرحمن الرحيم\" يعملون ظاهراً من الحياة في عالم الحس حتى يقف على هذا الكلام، أن يقبلو عذري فيما تسائلت في تبينه وتسامحت في تثبيته، فلم أفعل ذلك إلا لأني تسمنت شواهق يزل الطرف عن مرآها. وأردت تقريب الكلام فيها على سبيل المحاكة. فلما بصر بها رأى منظراً هاله، وخلقاً لم يعهده قبل، فوقف يتعجب.', 'تدقيقك حتى انك قد انخلعت عن غريزة العقلاء، واطرحت حكم معقول، فان من أحكام العقل إن الشيء آما واحد واما كثير، فليتئد في غلوائه، وليكف من غرب لسانه وليتهم نفسه، وليعتبر بالعالم المحسوس الخسيس الذي هو من صفات الأجسام،.', 'products/faker/fd23bf91af4bc9222687ae54b0685adf.png', 'به، وكيف بقاء هذا البخار المدة التي تكون بين العبادات إليه. فنظر أولاً إلى أجناس ما به من بين سائر أنواع الحيوان بجزئه الخسيس الذي هو من جوهر هذا الوجود لا يخلو من تلك المضرة. واما التشبه الثالث، فتحصل به المشاهدة الصرفة، والاستغراق المحض الذي لا سبب لتكون الحرارة إلا الحركة أو ملاقاة الأجسام الحارة والإضاءة؛ وتبين فيها أيضاً حيوان، كما يتكون في.', 'على هذا الكلام، أن يقبلو عذري فيما تسائلت في تبينه وتسامحت في تثبيته، فلم أفعل ذلك إلا نبوأً ونفاراً، مع أنهم كانوا محبين للخير، راغبين في الحق، إلا انهم لنقص فطرتهم كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه.', '2022-01-05 05:48:38', '2022-01-05 05:48:38', 0, NULL, 'show', NULL, 0),
(2, 'استحال عنده به وجود جسم.', '+16892653035', '+13046160413', 15204482759, 'يستضيء به بعض الأعمال.', '0.00', 'products/faker/d00f7b196248b4e070a7b703d2e96273.png', 'في نفسه جنس الحيوان كله واحداً بهذا النوع من ضروب التشبه حتى بلغ في ذلك ولم يترجح عنده أحد الاعتقادين على الآخر. وذلك أنه كان يرى انه إذا خلي وما تقتضيه صورته، ظهر منه برد محسوس، وطلب النزول إلى اسفل. وصار يطلب الصعود إلى فوق. فزال عنه بالجملة الوصفان اللذان كانا أبداً يصدران عن.', 'products/faker/q4WTknaERCoxHPcv5BwGFZGLwzjlgJ9B9C8RwAVv.mp4', 'جميع صيد البر والبحر، حتى مهر في ذلك. وزادت محبته للنار، إذ تأتي له بها من كل جهة، فنظر هل يرى فيه آفة ظاهرة؟ فلم ير فيه شيئاً! فشد على يده، فتبين له أن يفرض لنفسه فيها حدوداً لا يتعداها، ومقادير لا يتجاوزها، وبأن له الفرض يجب أن يكون قوة سارية في جسم خارج عنه. فهي إذا لشيء بريء عن الأجسام، وغير موصوف بشيء من الحواس، ولا يتخيل، ولا يتوصل إلى معرفته بآلة سواه، بل يتوصل إليه به؛ فهو العارف والمعروف، والمعرفة؛ وهو العالم، والمعلوم، والعلم؛ لا يتباين في شيء سواه، ولا يشترك به احداً ويستعين على ذلك أن تكون القوة التي تحرك ليست في الحقيقة لها، وانما هي لفاعل يفعل بها الأفعال المنسوبة إليها؛ وهذا المعنى الذي يحرك أحدهما الأخر علواً.', 'ما بها من الخصب والمرافق والهواء المعتدل، وان الانفراد بها يتأتى لملتمسه، فأجمع إن يرتحل إليها ويعتزل الناس بها بقية عمره. فجمع ما كان يراه من اتفاق فعله في أنه يتغذى وينمو. ثم كان يرجع إلى.', 'products/faker/a274c085ad5da406a4a4f5dfcff90d67.png', 'بالإضافة إلى ما انتهى إليه نظره أولاً، وأن ذلك بمنزلة نور الشمس بحاله لم ينقص عند حضور ذلك الجسم ولم يزد عند مغيبه. ومتى حدث جسم يصلح لقبول ذلك النور، قبله، فإذا عدم الجسم عدم ذلك القبول، ولك يكن له معنى، عنده هذا الظن بما قد بان له من العالم الروحاني، اذ هي صور لا تدرك إلا جسماً من الأجسام، ولو كان ذلك أطول لبقائه إلا انه على كل حال قصير المدة. واتخذ من الصياصي البقر الوحشية شبه الاسنة، وركبها في القصب القوي، وفي عصي الزان وغيرها، واستعان في ذلك ليلها ونهارها إلى حين مماتها وانقضاء مدتها. ولم ير.', 'التي لم تصل بعد حد كمالها. والشرط عليه من الأولى والثانية وكان دوامه أطول. وما زال يتصفح حركة القمر، فيراها آخذه من المغرب إلى المشرق وحركات الكواكب السيارة كذلك، حتى تبين له أن الأفعال الصادرة عنها، ليست في الحقيقة لها، وانما هي لفاعل يفعل بها الأفعال.', '2022-01-05 05:48:39', '2022-01-05 05:48:39', 0, NULL, 'show', NULL, 0),
(3, 'وانقضاء مدتها. ولم ير.', '+13513286804', '+14133262895', 16098227330, 'على الآخر. وذلك أنه كان.', '0.00', 'products/faker/9be84a2ec675730d856a82fe37971ab5.png', 'واقتدى به أسال حتى قرب من المحيط كان أقل ضوءاً حتى ينتهي إلى الظلمة عند محيط الدائرة الذي ما أضاء موقعه من الأرض فانهم قالوا إن بطناً من أرض تلك الجزيرة تخمرت فيه طينه على مر السنين والأعوام، حتى امتزج فيها الحار بالبارد، والرطب باليابس، امتزاج تكافؤ وتعادل في القوى. وكانت هذه الطينة المتخمرة كبيرة جداً وكان.', NULL, 'فيها، كلها أمور غير مفارقة للأجسام، ولا قوام لها إلا ثبات بثبات المرآة، فإذا فسدت المرآة صح فساد الصورة واضمحلت هي؛ فأقول لك: ما لأسرع ما نسيت العهد، وحلت عن الربط، ألم نقدم إليك إن مجال العبارة هنا ضيق، وان الألفاظ على كل حال توهم غير الحقيقة وذلك الذي توهمته إنما أوقعك فيه، إن جعلت المثال والممثل به على شكله، وتكون لحماً صلباً، وصار عليه غلاف صفيق يحفظه وسمي العضو كله قلباً واحتاج لما يتبع الحرارة من التحليل وافناء الرطوبات.', 'للنبات، مثل الصفاة والسبخة ونحوهما. فان تعذر عليه وجود مثل هذه الثمرات ذات الطعم الغاذي، كالتفاح والكمثرى والأجاص ونحوها، كان له من فاعل ليس بجسم، وإذا لم يكن جسماً فليس إلى إدراكه لشيء من الأعضاء. فبحث عن الجانب الآخر من الصدر، فوجد فيه الحجاب المستبطن للأضلاع، ووجد الرئة كمثل ما وجد من هذه الثلاثة قد يقال له قلب ولكن لا سبيل لخطور ذلك الآمر على حقيقته لاعرضوا عن هذه البواطل، وأقبلو على الحق، واستغنوا عن هذا كله، فليسد عنه سمعه من لا يعرف سوى المحسوسات وكلياتها،.', 'products/faker/d6b280c028fb941fd89dd8468ab5bddb.png', 'بها فيه: إن ألزم نفسه من الشروط لتناول الغذاء، ولم يدر اصل ذلك الشيء ما هو؟ وكيف هو؟ وما الذي أوجب بكاءه وتضرعه؛ فزاد في الدنو منه.', 'تولد من الأرض كثيراً، وأن الذي يستضيء من الشمس أجزاءاً أكثر، وما قرب من أو كاد وعبدا الله في تلك الذات الشريفة، التي أدرك بها ذلك الموجود الواجب الوجود. والضرب الثاني: أوصاف لها بالإضافة إلى ما التشبه بجوهره مادة قريبة منه، يجتذبها إلى نفسه. والنمو: هو الحركة في الأقطار الثلاثة، على نسبة محفوظة في الطول والعرض والعمق، وهو منزه عنها. ولما كانت المادة في كل شخص منها واحداً بهذا النوع من التلويح والإشارة إلى ما اتفقت فيه. وكان في غدوهما ورواحهما قد ألفهما ربرب يسرح ويبيت معهما حيث مبيتهما. فما زال يتتبع صفات الكمال كلها، فيراها على شكل الكرة، وقوى ذلك في اعتقاده، ما رآه من رجوع الشمس والقمر وسائر الكواكب، فرأها كلها تطلع من جهة المشرق، وتغرب من جهة المشرق،.', '2022-01-05 05:48:41', '2022-01-05 05:48:41', 0, NULL, 'show', NULL, 0),
(4, 'عنهم وتلطفا في العود.', '+14588466328', '+19254733220', 14586677588, 'وأن صفات الثبوت يشترط.', '0.00', 'products/SsVToPZqilq2aZ6h3vHmrIXZaZo9Oh7GrIpifNM4.jpg', 'وجللته بنفسها وبريش كان هناك؛ مما ملئ به التابوت أولاً في وقت من الأوقات، فبان له بذلك أن ذاته ليست هذه المتجسمة التي يدركها البصر أتم وأحسن من التي يدركها بحواسه، ويحيط.', 'products/faker/DIIfFCT6gOKAlUSegQRW6ngPtJ7YEBwgGSTbyIpn.mp4', '<p>أن جميع الأعضاء محتاجة إليه، وأن الواجب بحسب ذلك أن تكون ذاته بريئة عن الأجسام لا تفسد، فتبين له إن الأجسام التي في عالم الكون والفساد، المنزهة عن الحوادث النقص والاستحالة والتغيير. وأما أشرف جزأيه، فهو الشيء الذي اتحد به عند النبات والحيوان، مع مشاركة الجملة المتقدمة في تلك الجزيرة حتى أتاهما اليقين. هذا - أيدنا الله وأياك بروح منه - ما كان من نبأ حي بن يقظان فلم يدر ما هو، لانه لم يره على صورة شيء من الضوء والحرارة، آم لا؟ فعمد إلى بعد الوحوش واستوثق منه كتافاً وشقه على الصفة التي شق بها الظبية حتى كان لا يوجد في كتاب ولا يسمع في معتاد خطاب، وهو من العلم المكنون الذي لا التفات فيه بوجه من الوجوه، تعطل الجسد كله، وصار.</p>', '<p>اوهم ذلك معنى الكثرة فيها، وهي بريئة عن المادة، لا يجب إن يقال لها واحدة. وراى لذاته ولتلك الذوات التي في عالم الكون والفساد، منها ما لم يعقها عائق عن النزول: ومتى تحركت إلى جهة العلو مثل الدخان واللهيب والهواء، إذا حصل تحت الماء واما أن.</p>', 'products/faker/10a0b4082cb671abd5dc588cfc96e424.png', '<p>بقاءً سرمدياً، بحسب استعداده لكل واحد من هذه الأجناس إذا عدمت آيها تيسر له، بالقدر الذي يتبين له بعد هذا. فأما إن كانت لم تدرك قط بالفعل، فهي ما دامت بالقوة تشتاق إلى الإدراك.</p>', '<p>جميع الموجودات؛ فمنها ما لا يظهر أثره فيه، وهي أنواع النبات على اختلافها. فيرى كل نوع من أنواع الحيوان، وكفى به شرفاً أن يكون له طول وعرض وعمق على أي قدر كان، ولا يمكن أن يتحرك إلى جهة الشمس، وتحرك عروقه إلى الغذاء، بسبب شيء واحد وتارةً كثيرة كثرة لا تنحصر ولا تدخل تحت حد، ثم ينظر فيه بنظر آخر، فيراه واحداً. وبقي في ذلك كله عنه جهده واطعمه وسقاه. ومتى وقع بصره على ماء يسيل إلى سقي نبات أو حيوان وقد عاقه عن ممره ذلك عائق، من حجر سقط فيه، آو جرف انهار عليه، ازال ذلك كله يريد إن يريحه الله عز وجل، ووصفه ذلك الحق تعالى وجل بأوصافه الحسنى، ووصف له شأنه كله وكيف ترقى بالمعرفة، حتى انتهى إلى هذه المعرفة، فالأنقص إدراكاً أحرى أن لا يجعل لها حظاً من.</p>', '2022-01-05 05:48:42', '2022-01-05 10:49:28', 0, NULL, 'show', NULL, 0),
(5, 'إن تعدم ذات الواحد الحق.', '+14584382509', '+14152990334', 16298219239, 'له أنها معنى على حياله؛.', '0.00', 'products/faker/025042023472315a2c12ea255f5670c2.png', 'منها كل ما أدركته، كانت حينئذ بمنزلة المرأة المنعكسة على نفسها المحرقة لسوها وهذا لا يكون إلا للأنبياء صلوات الله عليهم، وأرادوا تقليد السفهاء والأغبياء أن يظنوا أن تلك الآراء هي الأسرار المضنون بها على غير أهلها، فيزيد بذلك أنسه وتنبسط نفسه لما كان أعطاه الله من القوة والبسطة في العلم والجسم - فالتزمه وقبض عليه؛ ولم يمكنه من البراح. فلما نظر إليه أسال وهو مكتس بجلود الحيوان ذوات.', 'products/faker/tPegQnHuqStKLCodvf1QHAVyK3FGg44DRpFxvh6y.mp4', 'إذا لم يجد لها كفواً. وكان له قريب يسمى يقظان فتزوجها سراً على وجه الأرض صلباً، فلا يمكن أن يكون قبل ذلك - في مدة تصريفه للبدن - قد تعرف بهذا الموجود الواجب الوجود؛ ولا اتصل به؛ ولا سمع عنه؛ فهذا إذا فارق البدن لا يشتاق إلى المبصرات.', 'لا تدرك إلا جسماً من الأجسام، ثم حركت يدك، فان ذلك الجسم زال نوره، وبقي نور الشمس بحاله لم ينقص عند حضور ذلك الجسم لا محالة متناه، فإذن كل قوة في جسم، فانها لا محالة جسمان ولكل واحد منهما مركبة من معنى زائد على الجسمية لانهما لو كانا للجسم من حيث لا يشعر، فرأى أن يقيم عليه ولا يتعرض لسواه، حتى يلحقه ضعف يقطع به بعض الأعمال التي يجب عليه من حيث هو الذات بعينها. وكذلك جميع الذوات المفارقة إن كانت لم تتأخر بالزمان عنها، بل كان يتطوف بأكناف تلك الجزيرة اعدل بقاع الأرض التي على خط الاستواء الذي وصفناه أولاً، كانت.', 'products/faker/71232cafcc0250d9524a8a0c7e3b4e62.png', 'فإذن هو الذات بعينها. وكذلك جميع الذوات ولم ير لنفسه شيئاً من الذوات التي شاهدها قبله ولا هي سواها. ولهذه سبعون ألف وجه، لقلنا انها بعضها. ولولا إن هذه الذات حدثت.', 'عن حركة يدك، تأخراً بالذات؛ وان كانت لجسم يؤول إلى الفساد كالحيوان الناطق، فسدت هي واضمحلت وتلاشت، حسبما مثلث به في اليم. فصادف ذلك جري الماء بقوة المد، فاحتمله من ليلته إلى ساحل البحر، وقلبها يحترق صبابةً به، وخوفاً عليه، ثم إنها حملت منه ووضعت طفلاً. فلما خافت أن يفتضح أمرها وينكشف سرها، وضعته في تابوت أحكمت زمه بعد أن لم تكن،.', '2022-01-05 05:48:43', '2022-01-05 05:48:43', 0, NULL, 'show', NULL, 0),
(6, 'مسكن أطلال - دورين', '2', '5', 200, 'مسكن أطلال - دورين', '0.00', 'products/6/0Ebpfs8SA4vjUWzA60AMmMMKdveJsgb9ODpbE4PL.jpg', 'مسكن أطلال يتميز بالمرونة العالية في عدد الوحدات والتمدد مستقبلا ، كما يمكن عمله على دور أودورين أو ثلاثة أدوار كوحدات منفصلة . بالإضافة لإمكانية عمل وحدتين ( وحدة في الدور الأرضي والوحدة الثانية دور للعائلة مع نصف دور للضيافة في السطح ) مع إمكانية عمل ثلاثة غرف مستقبلا .لتصبح وحدة ثالثة', 'products/7395i3gIkLTs5oEvkhad4Y14Lyq0iH22h3Aa1jZf.mp4', '<h4>عن التصميم</h4>\r\n\r\n<p>مسكن أطلال يتميز بالمرونة العالية في عدد الوحدات والتمدد مستقبلا ، كما يمكن عمله على دور أو دورين أو ثلاثة أدوار كوحدات منفصلة . بالإضافة لإمكانية عمل وحدتين ( وحدة في الدور الأرضي والوحدة الثانية دور للعائلة مع نصف دور للضيافة في السطح ) مع إمكانية عمل ثلاثة غرف مستقبلا لتصبح وحدة ثالثة . تم مراعاة توفير متنفس لكل وحدة سواء عبر الأفنية والشرفات المتدرجة على الواجهة البسيطة ، حيث تستمد قوتها عبر التكوين المتدرج.</p>\r\n\r\n<h4>تفاصيل المخطط الهندسي</h4>\r\n\r\n<h5>الدور الأرضي</h5>\r\n\r\n<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p>\r\n\r\n<h5>الدول الأول</h5>\r\n\r\n<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p>', NULL, 'products/OCrlaksvGjiwUpHYMyxnsU5ztKfWa2YKYqdmMkzy.svg', '<ul>\r\n	<li>nhbesgvb</li>\r\n	<li>fjshgfsfcvgbdhnjs</li>\r\n	<li>jhsfeghfjnes</li>\r\n</ul>', NULL, '2022-01-05 05:54:19', '2022-01-05 06:57:04', 0, NULL, 'show', NULL, 0),
(7, 'بينه وبين نفسه وذلك أنه.', '+19739311162', '+16297422941', 13238438959, 'من أمر هذا الفاعل، ما.', '0.00', 'products/faker/043023071ca5e7bfaa364891cf4f0c9a.png', 'اللذة والسرور، والغبطة والفرح، بمشاهدة ذات الحق جل جلاله. وشاهد ايضاً للفلك الذي يليه، وهو فلك الكواكب الثابتة، ذاتاً بريئة عن الكثرة. وان أنت عبرت بصيغة الإفراد، اوهم ذلك معنى الاتحاد، وهو مستحيل عليها. وكأني بمن يقف على حقيقة شأنه، ولا يبقي في نفسه.', 'products/faker/YQzJsdeRVqiWkdVXTDxoSfxZjH6ukC8ErVzGnrM2.mp4', 'وهو التمام، وهو الحسن، وهو البهاء، وهو القدرة، وهو العلم، وهو هو، و \"بسم الله الرحمن الرحيم\" يعملون ظاهراً من الحياة في عالم الحس حتى يقف على هذا الكلام، أن يقبلو عذري فيما تسائلت في تبينه وتسامحت في تثبيته، فلم أفعل ذلك إلا لأني تسمنت شواهق يزل الطرف عن مرآها. وأردت تقريب الكلام فيها على سبيل المحاكة. فلما بصر بها رأى منظراً هاله، وخلقاً لم يعهده قبل، فوقف يتعجب.', 'تدقيقك حتى انك قد انخلعت عن غريزة العقلاء، واطرحت حكم معقول، فان من أحكام العقل إن الشيء آما واحد واما كثير، فليتئد في غلوائه، وليكف من غرب لسانه وليتهم نفسه، وليعتبر بالعالم المحسوس الخسيس الذي هو من صفات الأجسام،.', 'products/faker/fd23bf91af4bc9222687ae54b0685adf.png', 'به، وكيف بقاء هذا البخار المدة التي تكون بين العبادات إليه. فنظر أولاً إلى أجناس ما به من بين سائر أنواع الحيوان بجزئه الخسيس الذي هو من جوهر هذا الوجود لا يخلو من تلك المضرة. واما التشبه الثالث، فتحصل به المشاهدة الصرفة، والاستغراق المحض الذي لا سبب لتكون الحرارة إلا الحركة أو ملاقاة الأجسام الحارة والإضاءة؛ وتبين فيها أيضاً حيوان، كما يتكون في.', 'على هذا الكلام، أن يقبلو عذري فيما تسائلت في تبينه وتسامحت في تثبيته، فلم أفعل ذلك إلا نبوأً ونفاراً، مع أنهم كانوا محبين للخير، راغبين في الحق، إلا انهم لنقص فطرتهم كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه لجهة تحقيقه، ولا يلتمسونه من بابه، بل كانوا لا يطلبون الحق من طريقة ولا يأخذونه.', '2022-01-05 05:48:38', '2022-01-05 05:48:38', 0, NULL, 'show', NULL, 0),
(8, 'استحال عنده به وجود جسم.', '+16892653035', '+13046160413', 15204482759, 'يستضيء به بعض الأعمال.', '0.00', 'products/faker/d00f7b196248b4e070a7b703d2e96273.png', 'في نفسه جنس الحيوان كله واحداً بهذا النوع من ضروب التشبه حتى بلغ في ذلك ولم يترجح عنده أحد الاعتقادين على الآخر. وذلك أنه كان يرى انه إذا خلي وما تقتضيه صورته، ظهر منه برد محسوس، وطلب النزول إلى اسفل. وصار يطلب الصعود إلى فوق. فزال عنه بالجملة الوصفان اللذان كانا أبداً يصدران عن.', 'products/faker/q4WTknaERCoxHPcv5BwGFZGLwzjlgJ9B9C8RwAVv.mp4', 'جميع صيد البر والبحر، حتى مهر في ذلك. وزادت محبته للنار، إذ تأتي له بها من كل جهة، فنظر هل يرى فيه آفة ظاهرة؟ فلم ير فيه شيئاً! فشد على يده، فتبين له أن يفرض لنفسه فيها حدوداً لا يتعداها، ومقادير لا يتجاوزها، وبأن له الفرض يجب أن يكون قوة سارية في جسم خارج عنه. فهي إذا لشيء بريء عن الأجسام، وغير موصوف بشيء من الحواس، ولا يتخيل، ولا يتوصل إلى معرفته بآلة سواه، بل يتوصل إليه به؛ فهو العارف والمعروف، والمعرفة؛ وهو العالم، والمعلوم، والعلم؛ لا يتباين في شيء سواه، ولا يشترك به احداً ويستعين على ذلك أن تكون القوة التي تحرك ليست في الحقيقة لها، وانما هي لفاعل يفعل بها الأفعال المنسوبة إليها؛ وهذا المعنى الذي يحرك أحدهما الأخر علواً.', 'ما بها من الخصب والمرافق والهواء المعتدل، وان الانفراد بها يتأتى لملتمسه، فأجمع إن يرتحل إليها ويعتزل الناس بها بقية عمره. فجمع ما كان يراه من اتفاق فعله في أنه يتغذى وينمو. ثم كان يرجع إلى.', 'products/faker/a274c085ad5da406a4a4f5dfcff90d67.png', 'بالإضافة إلى ما انتهى إليه نظره أولاً، وأن ذلك بمنزلة نور الشمس بحاله لم ينقص عند حضور ذلك الجسم ولم يزد عند مغيبه. ومتى حدث جسم يصلح لقبول ذلك النور، قبله، فإذا عدم الجسم عدم ذلك القبول، ولك يكن له معنى، عنده هذا الظن بما قد بان له من العالم الروحاني، اذ هي صور لا تدرك إلا جسماً من الأجسام، ولو كان ذلك أطول لبقائه إلا انه على كل حال قصير المدة. واتخذ من الصياصي البقر الوحشية شبه الاسنة، وركبها في القصب القوي، وفي عصي الزان وغيرها، واستعان في ذلك ليلها ونهارها إلى حين مماتها وانقضاء مدتها. ولم ير.', 'التي لم تصل بعد حد كمالها. والشرط عليه من الأولى والثانية وكان دوامه أطول. وما زال يتصفح حركة القمر، فيراها آخذه من المغرب إلى المشرق وحركات الكواكب السيارة كذلك، حتى تبين له أن الأفعال الصادرة عنها، ليست في الحقيقة لها، وانما هي لفاعل يفعل بها الأفعال.', '2022-01-05 05:48:39', '2022-01-05 05:48:39', 0, NULL, 'show', NULL, 0),
(9, 'وانقضاء مدتها. ولم ير.', '+13513286804', '+14133262895', 16098227330, 'على الآخر. وذلك أنه كان.', '0.00', 'products/faker/9be84a2ec675730d856a82fe37971ab5.png', 'واقتدى به أسال حتى قرب من المحيط كان أقل ضوءاً حتى ينتهي إلى الظلمة عند محيط الدائرة الذي ما أضاء موقعه من الأرض فانهم قالوا إن بطناً من أرض تلك الجزيرة تخمرت فيه طينه على مر السنين والأعوام، حتى امتزج فيها الحار بالبارد، والرطب باليابس، امتزاج تكافؤ وتعادل في القوى. وكانت هذه الطينة المتخمرة كبيرة جداً وكان.', 'products/faker/qCyjWynNjccfcgDFR3lBdQJJSZNWVwRNrW7UtBa8.mp4', 'فيها، كلها أمور غير مفارقة للأجسام، ولا قوام لها إلا ثبات بثبات المرآة، فإذا فسدت المرآة صح فساد الصورة واضمحلت هي؛ فأقول لك: ما لأسرع ما نسيت العهد، وحلت عن الربط، ألم نقدم إليك إن مجال العبارة هنا ضيق، وان الألفاظ على كل حال توهم غير الحقيقة وذلك الذي توهمته إنما أوقعك فيه، إن جعلت المثال والممثل به على شكله، وتكون لحماً صلباً، وصار عليه غلاف صفيق يحفظه وسمي العضو كله قلباً واحتاج لما يتبع الحرارة من التحليل وافناء الرطوبات.', 'للنبات، مثل الصفاة والسبخة ونحوهما. فان تعذر عليه وجود مثل هذه الثمرات ذات الطعم الغاذي، كالتفاح والكمثرى والأجاص ونحوها، كان له من فاعل ليس بجسم، وإذا لم يكن جسماً فليس إلى إدراكه لشيء من الأعضاء. فبحث عن الجانب الآخر من الصدر، فوجد فيه الحجاب المستبطن للأضلاع، ووجد الرئة كمثل ما وجد من هذه الثلاثة قد يقال له قلب ولكن لا سبيل لخطور ذلك الآمر على حقيقته لاعرضوا عن هذه البواطل، وأقبلو على الحق، واستغنوا عن هذا كله، فليسد عنه سمعه من لا يعرف سوى المحسوسات وكلياتها،.', 'products/faker/d6b280c028fb941fd89dd8468ab5bddb.png', 'بها فيه: إن ألزم نفسه من الشروط لتناول الغذاء، ولم يدر اصل ذلك الشيء ما هو؟ وكيف هو؟ وما الذي أوجب بكاءه وتضرعه؛ فزاد في الدنو منه.', 'تولد من الأرض كثيراً، وأن الذي يستضيء من الشمس أجزاءاً أكثر، وما قرب من أو كاد وعبدا الله في تلك الذات الشريفة، التي أدرك بها ذلك الموجود الواجب الوجود. والضرب الثاني: أوصاف لها بالإضافة إلى ما التشبه بجوهره مادة قريبة منه، يجتذبها إلى نفسه. والنمو: هو الحركة في الأقطار الثلاثة، على نسبة محفوظة في الطول والعرض والعمق، وهو منزه عنها. ولما كانت المادة في كل شخص منها واحداً بهذا النوع من التلويح والإشارة إلى ما اتفقت فيه. وكان في غدوهما ورواحهما قد ألفهما ربرب يسرح ويبيت معهما حيث مبيتهما. فما زال يتتبع صفات الكمال كلها، فيراها على شكل الكرة، وقوى ذلك في اعتقاده، ما رآه من رجوع الشمس والقمر وسائر الكواكب، فرأها كلها تطلع من جهة المشرق، وتغرب من جهة المشرق،.', '2022-01-05 05:48:41', '2022-01-05 05:48:41', 0, NULL, 'show', NULL, 0),
(10, 'عنهم وتلطفا في العود.', '+14588466328', '+19254733220', 14586677588, 'وأن صفات الثبوت يشترط.', '0.00', 'products/hvi51C8Bgx4Z77Jj5DKzPEZeasB6NfMbcVw3sCWW.jpg', 'وجللته بنفسها وبريش كان هناك؛ مما ملئ به التابوت أولاً في وقت من الأوقات، فبان له بذلك أن ذاته ليست هذه المتجسمة التي يدركها البصر أتم وأحسن من التي يدركها بحواسه، ويحيط.', 'products/O4Zi1glBLqQI5Wf8mpfNtfehnm4UVjDqfBSTPneC.mp4', '<p>أن جميع الأعضاء محتاجة إليه، وأن الواجب بحسب ذلك أن تكون ذاته بريئة عن الأجسام لا تفسد، فتبين له إن الأجسام التي في عالم الكون والفساد، المنزهة عن الحوادث النقص والاستحالة والتغيير. وأما أشرف جزأيه، فهو الشيء الذي اتحد به عند النبات والحيوان، مع مشاركة الجملة المتقدمة في تلك الجزيرة حتى أتاهما اليقين. هذا - أيدنا الله وأياك بروح منه - ما كان من نبأ حي بن يقظان فلم يدر ما هو، لانه لم يره على صورة شيء من الضوء والحرارة، آم لا؟ فعمد إلى بعد الوحوش واستوثق منه كتافاً وشقه على الصفة التي شق بها الظبية حتى كان لا يوجد في كتاب ولا يسمع في معتاد خطاب، وهو من العلم المكنون الذي لا التفات فيه بوجه من الوجوه، تعطل الجسد كله، وصار.</p>', '<p>اوهم ذلك معنى الكثرة فيها، وهي بريئة عن المادة، لا يجب إن يقال لها واحدة. وراى لذاته ولتلك الذوات التي في عالم الكون والفساد، منها ما لم يعقها عائق عن النزول: ومتى تحركت إلى جهة العلو مثل الدخان واللهيب والهواء، إذا حصل تحت الماء واما أن.</p>', 'products/faker/10a0b4082cb671abd5dc588cfc96e424.png', '<p>بقاءً سرمدياً، بحسب استعداده لكل واحد من هذه الأجناس إذا عدمت آيها تيسر له، بالقدر الذي يتبين له بعد هذا. فأما إن كانت لم تدرك قط بالفعل، فهي ما دامت بالقوة تشتاق إلى الإدراك.</p>', '<ul>\r\n	<li>fnhbdsb</li>\r\n	<li>fdsjhhzdjk\r\n	<ul>\r\n		<li>fmndsbhj</li>\r\n		<li>&nbsp;</li>\r\n	</ul>\r\n	</li>\r\n</ul>', '2022-01-05 05:48:42', '2022-01-24 07:35:36', 0, NULL, 'show', NULL, 1),
(11, 'إن تعدم ذات الواحد الحق.', '+14584382509', '+14152990334', 16298219239, 'له أنها معنى على حياله؛.', '0.00', 'products/faker/025042023472315a2c12ea255f5670c2.png', 'منها كل ما أدركته، كانت حينئذ بمنزلة المرأة المنعكسة على نفسها المحرقة لسوها وهذا لا يكون إلا للأنبياء صلوات الله عليهم، وأرادوا تقليد السفهاء والأغبياء أن يظنوا أن تلك الآراء هي الأسرار المضنون بها على غير أهلها، فيزيد بذلك أنسه وتنبسط نفسه لما كان أعطاه الله من القوة والبسطة في العلم والجسم - فالتزمه وقبض عليه؛ ولم يمكنه من البراح. فلما نظر إليه أسال وهو مكتس بجلود الحيوان ذوات.', 'products/faker/tPegQnHuqStKLCodvf1QHAVyK3FGg44DRpFxvh6y.mp4', 'إذا لم يجد لها كفواً. وكان له قريب يسمى يقظان فتزوجها سراً على وجه الأرض صلباً، فلا يمكن أن يكون قبل ذلك - في مدة تصريفه للبدن - قد تعرف بهذا الموجود الواجب الوجود؛ ولا اتصل به؛ ولا سمع عنه؛ فهذا إذا فارق البدن لا يشتاق إلى المبصرات.', 'لا تدرك إلا جسماً من الأجسام، ثم حركت يدك، فان ذلك الجسم زال نوره، وبقي نور الشمس بحاله لم ينقص عند حضور ذلك الجسم لا محالة متناه، فإذن كل قوة في جسم، فانها لا محالة جسمان ولكل واحد منهما مركبة من معنى زائد على الجسمية لانهما لو كانا للجسم من حيث لا يشعر، فرأى أن يقيم عليه ولا يتعرض لسواه، حتى يلحقه ضعف يقطع به بعض الأعمال التي يجب عليه من حيث هو الذات بعينها. وكذلك جميع الذوات المفارقة إن كانت لم تتأخر بالزمان عنها، بل كان يتطوف بأكناف تلك الجزيرة اعدل بقاع الأرض التي على خط الاستواء الذي وصفناه أولاً، كانت.', 'products/faker/71232cafcc0250d9524a8a0c7e3b4e62.png', NULL, 'عن حركة يدك، تأخراً بالذات؛ وان كانت لجسم يؤول إلى الفساد كالحيوان الناطق، فسدت هي واضمحلت وتلاشت، حسبما مثلث به في اليم. فصادف ذلك جري الماء بقوة المد، فاحتمله من ليلته إلى ساحل البحر، وقلبها يحترق صبابةً به، وخوفاً عليه، ثم إنها حملت منه ووضعت طفلاً. فلما خافت أن يفتضح أمرها وينكشف سرها، وضعته في تابوت أحكمت زمه بعد أن لم تكن،.', '2022-01-05 05:48:43', '2022-01-05 05:48:43', 0, NULL, 'show', NULL, 0),
(12, 'مسكن أطلال - دورين', '2', '5', 200, 'مسكن أطلال - دورين', '100000.00', 'products/6/0Ebpfs8SA4vjUWzA60AMmMMKdveJsgb9ODpbE4PL.jpg', 'مسكن أطلال يتميز بالمرونة العالية في عدد الوحدات والتمدد مستقبلا ، كما يمكن عمله على دور أودورين أو ثلاثة أدوار كوحدات منفصلة . بالإضافة لإمكانية عمل وحدتين ( وحدة في الدور الأرضي والوحدة الثانية دور للعائلة مع نصف دور للضيافة في السطح ) مع إمكانية عمل ثلاثة غرف مستقبلا .لتصبح وحدة ثالثة', '', '<h4>عن التصميم</h4>\r\n\r\n<p>مسكن أطلال يتميز بالمرونة العالية في عدد الوحدات والتمدد مستقبلا ، كما يمكن عمله على دور أو دورين أو ثلاثة أدوار كوحدات منفصلة . بالإضافة لإمكانية عمل وحدتين ( وحدة في الدور الأرضي والوحدة الثانية دور للعائلة مع نصف دور للضيافة في السطح ) مع إمكانية عمل ثلاثة غرف مستقبلا لتصبح وحدة ثالثة . تم مراعاة توفير متنفس لكل وحدة سواء عبر الأفنية والشرفات المتدرجة على الواجهة البسيطة ، حيث تستمد قوتها عبر التكوين المتدرج.</p>\r\n\r\n<h4>تفاصيل المخطط الهندسي</h4>\r\n\r\n<h5>الدور الأرضي</h5>\r\n\r\n<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p>\r\n\r\n<h5>الدول الأول</h5>\r\n\r\n<p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p>', NULL, '', NULL, NULL, '2022-01-05 05:54:19', '2022-01-10 09:57:28', 0, NULL, 'show', NULL, 0),
(14, 'knjhbsefnj', '22', '22', 2344, 'ebfhgsbhnj', '1.30', 'products/14/6fT6EvZiaQzt4u0kAyCL5MavAVoQifBj9LM7IsWR.png', 'jnhdgxzhbnjkjhg', '', '<p>lmkznjfbhan</p>', NULL, '', NULL, '<p><input checked=\"checked\" name=\"mmm\" type=\"radio\" value=\"md\" /></p>', '2022-01-10 09:52:46', '2022-01-10 09:52:46', 0, NULL, 'show', NULL, 0),
(15, 'test design', '282', '828', 92383, 'khdsnm', '88.20', 'products/15/IFtHbFYBE2sFIzRhVDMR9OVb8DjkJOKYADtDGunm.png', 'SNDVBVFHUJIKJKJHg', 'products/15/P2vJOi7YtcOYrHX28FhjykHp1E3KNW51MteU37FX.mp4', '<p>mvfgyxvhbjnkmsdjnhg</p>', '<p>sgvfedvhbdeusajemkcsj</p>', 'products/zNIbhAt2FU7pJkAU8MZ9fK1CgvgrjIDJPNWNJYJx.pdf', NULL, NULL, '2022-01-11 05:46:51', '2022-01-24 05:43:15', 2, 0, 'show', NULL, 3),
(17, 'hgzfcghj', '2828', '828', 27363, '227bdnakdnj', '21.22', 'products/17/Jr5vFYmHwr08rzPz4kTUxs787hTaUQYVXYtNoqmx.png', 'kdjhgvbhnjmk', '', '<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>\r\n\r\n<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>\r\n\r\n<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>\r\n\r\n<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>', '<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>\r\n\r\n<pre>\r\n<code>-&gt;whereRaw(&#39;seller_id = buyer_id&#39;)</code></pre>', '', NULL, NULL, '2022-01-23 06:12:46', '2022-01-23 06:25:07', 222, 2, 'show', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_books`
--

CREATE TABLE `product_books` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_detail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_books`
--

INSERT INTO `product_books` (`id`, `product_id`, `product_detail`, `created_at`, `updated_at`) VALUES
(1, 12, 'رسوم الواجهة الامامية', '2022-01-10 10:10:39', '2022-01-10 10:10:39'),
(2, 12, 'رسوم الكهرباء', '2022-01-10 10:10:58', '2022-01-10 10:10:58'),
(3, 1, 'test datya', '2022-01-11 12:29:13', '2022-01-11 12:29:13'),
(4, 15, 'رسوم الواجهة الامامية', '2022-01-13 12:19:58', '2022-01-13 12:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `product_coupons`
--

CREATE TABLE `product_coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `coupon_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_type` enum('percent','sar') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_coupons`
--

INSERT INTO `product_coupons` (`id`, `coupon_name`, `coupon_code`, `start_date`, `end_date`, `coupon_discount`, `discount_type`, `status`, `created_at`, `updated_at`) VALUES
(4, 'kmjdnhzvbcn', 'kjhgfdwww', '2022-02-05', '2022-02-22', '10', 'percent', 'show', '2022-01-23 08:14:32', '2022-01-24 06:15:49');

-- --------------------------------------------------------

--
-- Table structure for table `product_dimensions`
--

CREATE TABLE `product_dimensions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `north` bigint(20) NOT NULL,
  `east` bigint(20) NOT NULL,
  `south` bigint(20) DEFAULT NULL,
  `west` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_dimensions`
--

INSERT INTO `product_dimensions` (`id`, `product_id`, `north`, `east`, `south`, `west`, `created_at`, `updated_at`) VALUES
(4, 15, 5, 3, 5, 222, '2022-01-13 10:28:05', '2022-01-13 10:28:05'),
(5, 15, 3, 1, 3, 11, '2022-01-13 10:28:42', '2022-01-13 10:28:42'),
(6, 15, 2, 3, 2, 3, '2022-01-13 10:29:09', '2022-01-13 10:29:10'),
(7, 14, 5, 5, 5, 5, '2022-01-16 08:19:14', '2022-01-16 08:19:14'),
(8, 14, 3, 5, 5, 5, '2022-01-16 08:19:29', '2022-01-16 08:19:29');

-- --------------------------------------------------------

--
-- Table structure for table `product_favourites`
--

CREATE TABLE `product_favourites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_favourites`
--

INSERT INTO `product_favourites` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(13, 19, 12, '2022-01-11 12:37:16', '2022-01-11 12:37:16'),
(14, 19, 10, '2022-01-11 12:37:17', '2022-01-11 12:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `region_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `city_id`, `region_name`, `created_at`, `updated_at`) VALUES
(6, 2, 'مكه1', '2022-01-11 12:16:08', '2022-01-11 12:16:08'),
(7, 2, 'مكه2', '2022-01-11 12:16:14', '2022-01-11 12:16:14'),
(8, 1, 'دمام1', '2022-01-11 12:16:24', '2022-01-11 12:16:24'),
(9, 1, 'دمام2', '2022-01-11 12:16:35', '2022-01-11 12:16:35');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sitename_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename_fr` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system_status` enum('open','close') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `system_message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_setting` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `commercial_registration_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT 29.37858600,
  `lng` decimal(10,8) DEFAULT 47.99034100,
  `policy` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `sitename_ar`, `sitename_en`, `sitename_fr`, `email`, `address`, `mobile`, `logo`, `icon`, `about_us`, `video`, `system_status`, `system_message`, `theme_setting`, `created_at`, `updated_at`, `commercial_registration_no`, `tax_no`, `vat`, `lat`, `lng`, `policy`) VALUES
(1, 'منصه رمق', 'منصه رمق', 'منصه رمق', 'contact@company.com', 'المملكة العربية السعودية . الرياض . حى الورورد. شارع النزهة . بناية رقم 15', '1-3524-3356', 'setting/T5TE2cA9lmUyRgAd6QMzTAIP6mSVugh5sK99PxOl.png', 'setting/iTVxvjzEQCrTM1oePQXA1ppZVzwRAlN9DtjnDh70.png', 'النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.', 'setting/qUXj4Thf5UbkdWarUD9nb3ysJ0dfRbbskapdS9vB.mp4', 'open', NULL, '{\"brand_color\":\"navbar-dark\",\"sidebar_class\":\"sidebar-dark-navy\",\"main_header\":\"\",\"navbar\":\"navbar-dark navbar-dark\"}', '2022-01-04 06:49:12', '2022-01-16 06:05:45', '12345678912345', '123456789122333', '15', '24.78980000', '46.66025000', 'يقر المشتري بأن المنتج الذي يشتريه عبر المنصة هو للاستخدام الشخصي وليس التجاري ولمرة واحدة فقط ، وذلك على أرضه المرفق بياناتها في عملية الشراء ، ويتحمل كافة التبعات القانونية في حال انتهاك الحقوق أو تسريب المخططات بشكل مقصود إلى الآخرين بقصد الاستفادة منها في البناء أو التطوير . كما يتعهد بمراجعة المخططات ومواءمتها مع الاستشاري والمقاول والتأكد من القواعد الإنشائية المناسبة بناء على تقرير التربة .وتوصية الاستشاري المشرف على المشروع');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_title`, `slider_image`, `slider_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'يلحقه ضعف يقطع به بعض.', 'sliders/faker/b2083f860f6ec7b23321dc4b956863d2.png', 'جسماً، لحتاج إلى محدث ثالث، والثالث إلى رابع، ويتسلسل ذلك إلى فساد جسمه، فيكون ذلك اعتراضاً على فاعله أشد من الأول، إذ هو تصرف في الأمور المحسوسة، والأمور المحسوسة.', 'show', '2022-01-10 11:06:53', '2022-01-10 11:06:53'),
(2, 'لا تفاوت فيه ولا قصور،.', 'sliders/faker/d7a6e500a80a10f06be7fa97b6bc2d31.png', 'الفساد جداً مثل الذهب والياقوت، وأن الأجسام البسيطة صرفة، ولذلك هي بعيدة عن الفساد، والصور لا تتعاقب عليها. وتبين له أن حركتها لا تكون إلا بالاتصال. ولا.', 'show', '2022-01-10 11:06:54', '2022-01-10 11:06:54'),
(3, 'اختلاف أنواعها، والنبات.', 'sliders/faker/e0112235fb71f3baa3801bb946b8c52c.png', 'إن جعلت المثال والممثل به على حكم واحد من الوجهين لحياته الجسمانية. واما من البقول التي لم تكن إلا من الحجارة والقصب، فاستجدها ثانية واستحدها وتلطف في خرق الحجاب حتى انخرق له، فأفضى إلى الرئة فظن أنها مطلوبه، فما زال يتخذ غيره ويخصف بعضه ببعض طاقات مضاعفة، وربما كان ذلك المحدث الثاني أيضاً جسماً، لحتاج إلى محدث ثالث، والثالث إلى رابع، ويتسلسل ذلك إلى فساد.', 'show', '2022-01-10 11:06:54', '2022-01-10 11:06:54'),
(4, 'وان ذلك الشيء ما هو؟.', 'sliders/faker/71c692d0bfd3b7a44358098afa733804.png', 'قبولهم. وتصفح طبقات الناس بعد ذلك، فرأى كل حزب بما لديهم فرحون، قد اتخذوا ألههم هواهم، ومعبودهم شهواتهم، وتهالكوا في جميع أصناف الحيوان، كيف \"بسم الله الرحمن الرحيم\" لمن الملك اليوم لله الواحد القهار صدق الله العظيم ففهم كلامه وسمع ندائه ولم يمنعه عن فهمه كونه لا يعرف سوى المحسوسات وكلياتها، وليرجع إلى فريقه الذين \"بسم الله الرحمن الرحيم\" لا يغرب عنه مثقال ذرة في.', 'show', '2022-01-10 11:06:55', '2022-01-10 11:06:55'),
(5, 'للشمس، ورأى لهذه الذات.', 'sliders/lbE7pFiHxJ6pOearV5KBppDeBsJTe8wksEqGKmiD.png', 'ثمتأمل في جميع أصناف الحيوان، كيف \"بسم الله الرحمن الرحيم\" فان تقتلوهم ولكن الله قتلهم؛ وما رميت إذا رميت، ولكن الله قتلهم؛ وما رميت إذا رميت، ولكن الله قتلهم؛ وما رميت إذا رميت، ولكن الله رمى! صدق الله العظيم ففهم كلامه وسمع ندائه ولم يمنعه عن فهمه كونه لا يعرف سوى المحسوسات وكلياتها، وليرجع إلى.', 'hide', '2022-01-10 11:06:55', '2022-01-13 06:17:50'),
(6, 'هذا النص هو مثال', 'sliders/ODfCLWXhuLGAfNCnLYR3kJSobWPgpOT69MhQfp6v.png', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\r\nإذا كنت تحتاج إلى عدد أكبر من الفقرات', 'show', '2022-01-11 10:25:59', '2022-01-13 06:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `social_links`
--

CREATE TABLE `social_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `social_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('show','hide') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_links`
--

INSERT INTO `social_links` (`id`, `social_name`, `social_url`, `social_icon`, `social_image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'facebook', 'https://facebook.com', 'la la-facebook', 'sociallinks/1/ufB55BLFpeu1yaJG2idLHl7QaQ2B1yVJmgiAv6tr.png', 'show', '2022-01-10 12:40:26', '2022-01-10 12:41:22'),
(2, 'twitter', 'https://twitter.com', 'la la-twitter', 'sociallinks/2/g7kr4Qc82N5uLOSqIqF4P2JZM0IRksa5cVbDP1lG.png', 'show', '2022-01-10 12:41:53', '2022-01-10 12:43:05'),
(3, 'instagram', 'https://instagram.com', 'la la-instagram', 'sociallinks/3/XODJCpXwlNTkERoKNbosRfRBRdLMtCGU72m6bJEr.png', 'show', '2022-01-10 12:42:28', '2022-01-10 12:42:28'),
(4, 'youtube', 'https://youtube.com', 'la la-youtube', 'sociallinks/4/FRY74wGkvwVzVoSw4IYZp0263m9vBlZ7V5pWeWsL.png', 'show', '2022-01-10 12:42:58', '2022-01-11 11:44:37');

-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE `statistics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `statistic_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `statistic_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statistics`
--

INSERT INTO `statistics` (`id`, `statistic_title`, `statistic_number`, `created_at`, `updated_at`) VALUES
(1, 'هذا النظر على ان حقيقة.', '23', '2022-01-09 06:31:46', '2022-01-09 06:33:44'),
(2, 'عدد المهندسين', '50', '2022-01-09 06:31:46', '2022-01-09 06:33:05'),
(3, 'عدد التصميمات المباعه', '190', '2022-01-09 06:31:46', '2022-01-09 06:33:29'),
(4, 'عدد العملاء', '200', '2022-01-09 06:31:46', '2022-01-09 06:32:53'),
(5, 'عدد التصميمات', '21', '2022-01-09 06:31:47', '2022-01-09 06:32:33');

-- --------------------------------------------------------

--
-- Table structure for table `tradmarks`
--

CREATE TABLE `tradmarks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tradmarks`
--

INSERT INTO `tradmarks` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'السيد هانيا عبد الغني المقبل', 'tradmarks/faker/74439275d3d8488940c55f74faa71e52.png', '2022-01-05 12:41:20', '2022-01-05 12:41:20'),
(2, 'السيدة بنان الراجحي', 'tradmarks/faker/b47744ef22954d70dfe1e11c40545f67.png', '2022-01-05 12:41:21', '2022-01-05 12:41:21'),
(3, 'الآنسة السيدة دنى السعيد', 'tradmarks/faker/c9d34884ee13679dcfdbea7e0f6dea9c.png', '2022-01-05 12:41:22', '2022-01-05 12:41:22'),
(4, 'المهندس سالم الشيباني', 'tradmarks/faker/16fdc7e6f18ec1cc2d45488821f3773b.png', '2022-01-05 12:41:22', '2022-01-05 12:41:22'),
(5, 'الدكتور مختار اوسم الأسمري', 'tradmarks/faker/769215872b656309b5e299ef6be5f5fe.png', '2022-01-05 12:41:23', '2022-01-09 05:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` int(11) DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `region_id` bigint(20) UNSIGNED NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','accepted','rejected') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `decline_reason` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `forget_password` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `code`, `first_name`, `last_name`, `email`, `mobile`, `password`, `city_id`, `region_id`, `country_code`, `status`, `decline_reason`, `created_at`, `updated_at`, `forget_password`) VALUES
(18, 57551, 'mai', 'tarek', 'maitarekttt@gmail.com', 192837337, '$2y$10$TeiwfhEQXfQzQoYdhXJH2OxKRGcfPjStZO5q/z9sXzQ3SvdmvO0Bq', 2, 7, '+023', NULL, NULL, '2022-01-11 12:17:20', '2022-01-11 12:17:20', 0),
(19, NULL, 'test123', 'user2', 'test123@test.com', 9276367282, '$2y$10$LToXUDhOn8YX6UcWwcBbaOhy7Ol85kZla2/NNLBLVyK1Fiie42BKG', 1, 8, '+1', 'accepted', NULL, '2022-01-11 12:37:08', '2022-01-11 12:45:01', 0),
(20, 9885, 'testuser', 'testuser', 'test@test.com', 9223372036854775807, '$2y$10$BJGYgJdLr1OxfXTCulQt5efpPWL714hukkZlhf4Hh/Q4NDCBQX1OG', 2, 6, '966', 'accepted', NULL, '2022-01-23 05:41:17', '2022-01-23 05:41:18', 0),
(21, 38545, 'testuser1', 'testuser', 'testuser1@test.com', 922926367222, '$2y$10$u5Jp10yaiX6r2oSPr8lKYuVf2TXH0DZGIZTBc7dkZM.SXZ0pmSQcG', 2, 6, '966', 'accepted', NULL, '2022-01-23 05:43:14', '2022-01-23 05:43:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `website_links`
--

CREATE TABLE `website_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_position` enum('header','footer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_link` bigint(20) UNSIGNED DEFAULT NULL,
  `link_target` enum('self','blank') COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `website_links`
--

INSERT INTO `website_links` (`id`, `link_name`, `link_url`, `link_position`, `sub_link`, `link_target`, `link_status`, `created_at`, `updated_at`) VALUES
(1, 'من نحن', 'http://localhost/rmq/public/about-us', 'header', NULL, 'self', '1', '2022-01-04 12:39:13', '2022-01-05 12:11:09'),
(2, 'متجر التصاميم', 'http://localhost/rmq/public/products', 'header', NULL, 'self', '1', '2022-01-05 11:38:24', '2022-01-05 11:38:24'),
(3, 'من نحن', 'http://localhost/rmq/public/about-us', 'footer', NULL, 'self', '1', '2022-01-05 11:38:49', '2022-01-05 12:11:34'),
(4, 'تواصل معنا', 'http://localhost/rmq/public/contact', 'header', NULL, 'self', '1', '2022-01-05 11:39:16', '2022-01-05 11:39:16'),
(5, 'تواصل معنا', 'http://localhost/rmq/public/contact', 'footer', NULL, 'self', '1', '2022-01-05 11:39:31', '2022-01-11 11:37:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_groups`
--
ALTER TABLE `admin_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_groups_admin_id_foreign` (`admin_id`);

--
-- Indexes for table `admin_group_roles`
--
ALTER TABLE `admin_group_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_group_roles_admin_groups_id_foreign` (`admin_groups_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_product_id_foreign` (`product_id`),
  ADD KEY `carts_client_id_foreign` (`client_id`),
  ADD KEY `carts_city_id_foreign` (`city_id`),
  ADD KEY `carts_region_id_foreign` (`region_id`),
  ADD KEY `coupon_id` (`coupon_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_admin_id_foreign` (`admin_id`),
  ADD KEY `files_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `most_populars`
--
ALTER TABLE `most_populars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `most_populars_product_id_foreign` (`product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `productcoupons`
--
ALTER TABLE `productcoupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `coupon_id` (`coupon_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_books`
--
ALTER TABLE `product_books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_books_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_coupons`
--
ALTER TABLE `product_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_dimensions`
--
ALTER TABLE `product_dimensions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_dimensions_product_id_foreign` (`product_id`),
  ADD KEY `north` (`north`);

--
-- Indexes for table `product_favourites`
--
ALTER TABLE `product_favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_favourites_user_id_foreign` (`user_id`),
  ADD KEY `product_favourites_product_id_foreign` (`product_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regions_city_id_foreign` (`city_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_links`
--
ALTER TABLE `social_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tradmarks`
--
ALTER TABLE `tradmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_city_id_foreign` (`city_id`),
  ADD KEY `users_region_id_foreign` (`region_id`);

--
-- Indexes for table `website_links`
--
ALTER TABLE `website_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_links_sub_link_foreign` (`sub_link`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_groups`
--
ALTER TABLE `admin_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_group_roles`
--
ALTER TABLE `admin_group_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `most_populars`
--
ALTER TABLE `most_populars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productcoupons`
--
ALTER TABLE `productcoupons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_books`
--
ALTER TABLE `product_books`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_coupons`
--
ALTER TABLE `product_coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_dimensions`
--
ALTER TABLE `product_dimensions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_favourites`
--
ALTER TABLE `product_favourites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `social_links`
--
ALTER TABLE `social_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tradmarks`
--
ALTER TABLE `tradmarks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `website_links`
--
ALTER TABLE `website_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_groups`
--
ALTER TABLE `admin_groups`
  ADD CONSTRAINT `admin_groups_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `admin_group_roles`
--
ALTER TABLE `admin_group_roles`
  ADD CONSTRAINT `admin_group_roles_admin_groups_id_foreign` FOREIGN KEY (`admin_groups_id`) REFERENCES `admin_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `files_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `most_populars`
--
ALTER TABLE `most_populars`
  ADD CONSTRAINT `most_populars_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productcoupons`
--
ALTER TABLE `productcoupons`
  ADD CONSTRAINT `coupon_id` FOREIGN KEY (`coupon_id`) REFERENCES `product_coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_books`
--
ALTER TABLE `product_books`
  ADD CONSTRAINT `product_books_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_dimensions`
--
ALTER TABLE `product_dimensions`
  ADD CONSTRAINT `product_dimensions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_favourites`
--
ALTER TABLE `product_favourites`
  ADD CONSTRAINT `product_favourites_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_favourites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `regions`
--
ALTER TABLE `regions`
  ADD CONSTRAINT `regions_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `website_links`
--
ALTER TABLE `website_links`
  ADD CONSTRAINT `website_links_sub_link_foreign` FOREIGN KEY (`sub_link`) REFERENCES `website_links` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
